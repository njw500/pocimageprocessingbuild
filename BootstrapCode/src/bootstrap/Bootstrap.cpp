
//#include "stdafx.h"

#include "..\..\..\Utilities\Utilities\UDPSend.h"

#include <conio.h>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#ifdef USE_PLOTTER
#include <opencv2/plot.hpp>
#endif

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "..\parameters\BootstrapSysParams.h"
#include "BootstrapGui.h"
#include "..\parameters\DebugFlags.h"  
#include "..\tracking\Tracking2D.h"

#include "..\..\..\Utilities\Utilities\BaslerCamera.h"
#include "..\..\..\Utilities\Utilities\DisplayIm.h"
#include "..\..\..\Utilities\Utilities\Histogram.h"


int main(int argc, char* argv[])
{

	// The exit code of the sample application.
	int exitCode = 0;
	int UseCamera = 1;	// Cant actually be turned off at the moment
	int FrameCount = 0;

	bool CamOK = false;// Status flag for camera
	bool CamOK2 = false;// 2nd camera

	bool UseFile = false; // Enable/disable toggle for file processing
	bool FileLoaded = false; // Status flag to know of file has been read in
	bool SetUpMode = true;

	cUDP UDP;
	UDP.Init(27127,false);// Pick a different port for Bootstrap


#ifdef EMULATEFROMFILE
	UseFile = true;
	SetUpMode = false;// Dont launch into the GUI display mode
#endif
	BootstrapSysParams Params;

	SetUpMode = true;
	
	Tracker2D tracker2D;

	tracker2D.LoVelocityLimit = Params.LoVelocityLimit.val;
	tracker2D.HiVelocityLimit = Params.HiVelocityLimit.val;
	
//	Params.cParams.CameraSerial = "";

	cv::Mat avg;	// Average

	try
	{
		LARGE_INTEGER Frequency, start, end;

		char string[80];

		int NumFrames = Params.NumberOfImagesToGrab;  // use to set the number of frames from file, or in a burst

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;

		BaslerCamera  camera;// Primary Bootstrap Camera
		BaslerCamera  Sortercamera;

#ifdef ENABLECAMERA
		//Params.cParams.AutoStart = true;
		//Params.cParams.CameraSerial = "21744985";// Debug
		CamOK = camera.Init(Params.cParams.CameraSerial, Params.cParams); // Get first camera available

		// DEBUG
		if (!CamOK)
		{
			cout << "Cannot connect to camera " << endl;

			if (!UseFile)
			{
				// No camera and not using file. Exit now !
				cout << "No camera, shutting down " << endl;
				cerr << endl << "Press Enter to exit." << endl;
				while (cin.get() != '\n');
				return(-1);
			}
		}
		else
		{
			camera.SetExposure(100);
			camera.SetFrameRate(-1);// Max
			camera.SetGain(8);
			double FrameRate = camera.GetRealFrameRate();

			if (FrameRate > 2000)
			{
				camera.SetFrameRate(2000);// Max
			}
	
			Sleep(200);
			FrameRate = camera.GetRealFrameRate();
			cout << FrameRate << endl;
			// Try the 2nd camera
			//Params.cParams.CameraSerial = "21814112";// Debug
			//CamOK2 = Sortercamera.Init(Params.cParams2.CameraSerial, Params.cParams2); // Get first camera available
		/*	Sortercamera.SetFrameRate(FrameRate);// Max

			if (!CamOK2)
			{
				cout << "Cannot connect to 2nd camera " << endl;

			}*/
		}

#else
		CamOK = false;
#endif

		// Declare an integer variable to count the number of grabbed images

		int grabbedImages = 0;

		vector<float> transitTimeList;
		vector<float> BlockByBlocktTime;

		//vector<double > timeList;

#ifdef EMULATEFROMFILE

		VideoCapture vidCap;
		int NumFramesInFile;
		Mat Frames[Params.NumberOfImagesToGrab];

		// Create an OpenCV image.
		Mat openCvImage;
		char filename[200];
		// Open stored video file
		sprintf(filename, "%s", Params.VideoFileName.c_str());

		// Open the input avi file
		vidCap.open(filename);
		NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);

		// Allocate and read in the frames
		// pre-load to avoid this read giving misleading load times

		NumFrames = Params.NumberOfImagesToGrab;

		if (NumFramesInFile < NumFrames)
		{
			cout << "Too few frames" << endl;
			NumFrames = NumFramesInFile;
		}

		if (NumFramesInFile < 10)
		{
			cout << "No Data in file ! " << endl << "giving up " << endl;
			cout << endl << "Press Enter to exit." << endl;
			while (cin.get() != '\n');
			return(-1);
		}

		cout << "Reading frames" << endl;

		for (int i = 0; i < NumFrames; ++i)
		{
			vidCap.read(Frames[i]);  // Fake a frame from the stored file
			if (Frames[i].empty())
			{
				cout << "ERROR! blank frame grabbed\n";
				break;
			}
		}

		cout << "Frames read into memory" << endl;
		FileLoaded = true;
#endif // Read in from file


#ifdef recordVideo
		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;
		// Define the video file name.
		std::string videoFileName = "c:\\temp\\openCvVideo.avi";
		// Define the video frame size.
		cv::Size CamFrameSize = Size(Params.BootstrapFrame_ROI_Width, Params.BootstrapFrame_ROI_Height + 100);
		// Set the codec type and the frame rate. 
		cvVideoCreator.open(videoFileName, CV_FOURCC('D', 'I', 'V', 'X'), 20, CamFrameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
#endif

		// Other openVCImages
		//Mat filtered_frame 
		Mat mask, frame;

		// Make a rotation matrix to align the current generate video
		cv::Point2f pc(camera.GetCurWidth() / 2., camera.GetCurHeight() / 2.);
		cv::Mat r = cv::getRotationMatrix2D(pc, Params.FrameRotate, 1.0);// Build Rotation matrix for specified rotation

		// Start main grab and process loop
		bool StopLoop = false;

		// Just to be sure
#ifndef ENABLECAMERA
		CamOK = false;
#endif
		bool GotValidFrame = false;
		QueryPerformanceCounter(&start);

		if (!UseFile &&  !CamOK)
		{
			cerr << "NO CAMERA OR DATA FILE" << endl << "giving up " << endl;
			cerr << endl << "Press Enter to exit." << endl;
			while (cin.get() != '\n');
			return(-1);
		}


		std::vector<cv::Mat> cFrames(Params.NumGrab);
		std::vector<float> DeltaT;// Need to use this for precision 

		Rect OutRect;

		int BurstFrameIndex;
		bool BurstComplete = false;
		bool NewBurst = true;
		bool SingleStep = false;

		SetUpMode = false;

		std::vector<cv::Rect> roiList;
		roiList.push_back(Params.BS_roiVelRegion1);
		roiList.push_back(Params.BS_roiVelRegion2);
		roiList.push_back(Params.BS_roiVelRegion3);
		roiList.push_back(Params.BS_roiVelRegion4);
		
		Mat avg;

#ifdef WITHGUI
		BootstrapGui bootstrapGui;
		bootstrapGui.BootstrapGuiInit(&tracker2D,&Params,&camera);
#endif

		// Main capture, process (display) loop
		while (!StopLoop)
		{
			// Wait for an image and then retrieve it. 
			GotValidFrame = false;// Set to false for safety

			// Launch into display mode first
			if (SetUpMode && CamOK)
			{
				camera.DisplayLoop(OutRect);// Loop displaying frame and allowing the ROI to be adjusted
				SetUpMode = false;
			}

			// Bootstrap works in a burst and process mode
			if ((CamOK) && (NewBurst))
			{
				camera.Burst(cFrames, DeltaT, Params.NumGrab);
				NumFrames = DeltaT.size();

				BurstFrameIndex = 0;
				if (NumFrames > 10)
				{
					BurstComplete = true;
					NewBurst = false;
					tracker2D.initialised = false;
				}
				else
					cout << "Failed to grab" << endl;	
			}
			else
			{
				// Otherwise the camera is just disconnected and we are in video mode
			}

			Mat getframe;
#ifdef EMULATEFROMFILE
			// SWap in the sorted video frame

			if (UseFile)
			{
				// Swap from stored video frames
				getframe = Frames[FrameCount];
				getframe.copyTo(frame);
				if ((frame.rows > 10) && (frame.cols > 10))
					GotValidFrame = true;
				else
					GotValidFrame = false;
			}
#else
			// Not emulating but actually using video data
			getframe = cFrames[FrameCount];
			getframe.copyTo(frame);
			displayIm("Copied frame", frame);
			GotValidFrame = true;
#endif
			// Process loop
			if (GotValidFrame)
			{
				// We have a frame worth processing

				//Rotate if we need
				if (Params.DoFrameRotate) // Avoid this affine rotate if possible
					cv::warpAffine(frame, frame, r, frame.size());

				cv::Mat cframe;
				Mat ffcrop;
				Mat frameDisp;
				Mat phaseFrame;
				cv::Mat background;

#ifdef EMULATEFROMFILE

				if (!Params.VideoIsPreCropped)
					cframe = frame(Rect(Params.BootstrapFrame_ROI_Left, Params.BootstrapFrame_ROI_Top,
					Params.BootstrapFrame_ROI_Width, Params.BootstrapFrame_ROI_Height));
				else  // DONT crop as the video is already the active ROI
					cframe = frame;
#else
				cframe = frame;// copy
#endif
				if (Params.DisplayMode[WCropped])
					displayIm("CROPPED FRAME", cframe);

				//imwrite("Cropped Frame.jpg", cframe);

				// Initialize Averaget
				if (FrameCount == 0)
				{
					avg = Mat(cframe.rows, cframe.cols, CV_32F);
					cframe.convertTo(avg, CV_32F);
				}
				else
				{
					cframe.convertTo(ffcrop, CV_32F);// Promote current crop to float
					accumulateWeighted(ffcrop, avg, Params.AverageDecay);
				}
				
				convertScaleAbs(avg, background);

				cv::Mat Subtracted;
				// Calculate this again
				subtract(background, cframe, Subtracted);

				// Flip subtracted into image
				cframe = Subtracted;

#ifdef EMULATEFROMFILE
				cvtColor(cframe, cframe, COLOR_BGR2GRAY);
#endif
				cframe.convertTo(cframe, CV_32F);// Promote current crop to float
				frameDisp = cframe.clone();
				phaseFrame = cframe.clone();
				frame.convertTo(frame, CV_32F);
				//displayIm("trackerFrame2", phaseFrame);

				cHistogram timeHist;
				timeHist.UseFloat = true;// Allow lots of precision bins

				// Create the regions of interest.

				// Initialise the tracker if required
				if (tracker2D.initialised == false)
				{
					// Initialise the tracker
					std::vector<double>velList;
					tracker2D.init(roiList, phaseFrame);
				}
				// If already initialised, update the tracker and (optionally) plot
				else
				{
					tracker2D.update(phaseFrame);

				//	if (Params.DisplayMode[WTrackerFilter])
					//{		
						Mat trackReg = frameDisp.clone();
						// Make display image with tracking blue circle
						tracker2D.updatePlot(&frame, Params.Bs_StartRow.val, Params.Bs_EndRow.val);
					//}

					if ((tracker2D.medianVelocityList.size() > 0) && (tracker2D.ValidVelocities))
					{
						double TransitTime = tracker2D.calculateFramesBetweenPoints(Params.Bs_StartRow.val, Params.Bs_EndRow.val);

						if (TransitTime > 0)// Valid Data
						{
							transitTimeList.push_back(TransitTime);
						}
						//timeList.push_back((float)FrameCount);
						if (transitTimeList.size() > 20)
						{
							float rmx, rmn, rstd;

							timeHist.GetHistogram(transitTimeList, transitTimeList.size(),
								rmx, rmn, rstd, cv::String("Transit Time Histogram"));

							if ((Params.DisplayMode[WHist]) && (transitTimeList.size() > 30))
							{
								stringstream str1;
								str1 << "mn: " << rmn;
								stringstream str2;
								str2 << "sd: " << rstd;
								timeHist.Annotate(str1.str(), str2.str(), "");
								timeHist.Show();
							}// Display HIST

							// Send time in frames as string to UDP
							char Cstr[200];
							sprintf(Cstr, "%0.2f\n", rmn);// Mean Time
							UDP.SendString(Cstr);



#ifdef USE_PLOTTER
							Ptr<plot::Plot2d> newPlot, newPlot2;
							newPlot = plot::createPlot2d(timeList, transitTimeListD);
							newPlot->setMaxY(150);
							newPlot->setMinY(0);
							Mat plot_result;
							newPlot->render(plot_result);
							imshow("Transit Time vs Time", plot_result);
#endif		
						}// Got enough points for a histogram
					}// Got a velocity list

				}// Tracking

				//imwrite("BlueCircle.jpg", frameDisp);

				//if (Params.DisplayMode[WTrackerFilter])
				displayIm("Main Output", frame);// Display the blue-circle image
				//waitKey(1);


				// Saving output to files for inspection

				/*	std::ostringstream s1;
					// Create image name files with ascending grabbed image numbers.
					s1 << "imageNew_" << FrameCount << ".jpg";
					std::string imageName(s1.str());
					// Save an OpenCV image.
					imwrite(imageName, frameDisp);
					*/

				// Set recordVideo to '1' to record AVI video file.
#ifdef recordVideo

				cvVideoCreator.write(frameDisp);
				grabbedImages++;
				//cv::accumulate(openCvImage, avgImg);

#endif
			}

			FrameCount = FrameCount + 1;
			cout << "FC: " << FrameCount << endl;
			if (FrameCount > NumFrames - 2)
			{
				FrameCount = 2;// avoid the special conditions
				cout << "Rewind" << endl;
				NewBurst = true;
				tracker2D.reset();
				transitTimeList.erase(transitTimeList.begin(), transitTimeList.end());
				//StopLoop = true;// Use for one pass timing
			}




			if (SingleStep)
				waitKey(0);// wait for key press

			if (_kbhit())
			{
				cout << "ok" << endl;
				int key = _getch();

				switch (key)
				{
				case 'q':
					StopLoop = true;
					break;

				case '#':
					SingleStep = !SingleStep;
					break;

					/*			case '[':
								{
								float gain;
								uint16_t Exp, Rate;
								camera.GetGainExpRate(gain, Exp, Rate);

								cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
								gain = gain + 0.5f;
								camera.SetGain(gain);
								}
								break;

								case ']':
								{
								float gain;
								uint16_t Exp, Rate;
								camera.GetGainExpRate(gain, Exp, Rate);
								cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;

								gain = gain - 0.5f;
								if (gain < 0.0)
								gain = 0.0;
								camera.SetGain(gain);
								}
								break;

								case 'a':
								{
								bool flag = true;
								if (Params.DisplayMode[WGrab])
								flag = false;
								Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
								Params.DisplayMode[WHough] = flag;
								Params.DisplayMode[WBack] = flag;
								Params.DisplayMode[WCropped] = flag;
								Params.DisplayMode[WEdges] = flag;
								Params.DisplayMode[WHist] = true;
								Params.DisplayMode[WOutput] = flag;
								Params.DisplayMode[WDisplayEd] = flag;
								}
								break;

								case 'j':
								{

								camera.RoiVert(1, -1);
								Rect Cur = camera.GetROI();
								cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
								}
								break;
								case 'k':
								{
								camera.RoiVert(1, +1);
								Rect Cur = camera.GetROI();
								cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
								}
								break;

								case 'o':
								{
								Params.Bs_EndRow = Params.Bs_EndRow - 8;
								Params.BS_roiVelRegion1 = cv::Rect(Params.Bs_EndRow + 470 + 170, 1, 492, 20);
								Params.BS_roiVelRegion2 = cv::Rect(Params.Bs_EndRow, 1, 470, 20);
								std::vector<cv::Rect> roiList;
								roiList.push_back(Params.BS_roiVelRegion1);
								roiList.push_back(Params.BS_roiVelRegion2);
								//roiList.push_back(Params.BS_roiVelRegion3);
								//roiList.push_back(Params.BS_roiVelRegion4);
								// Initialise the tracker if required
								// Initialise the tracker
								tracker2D.updateRoi(roiList);
								}
								break;


								case 'p':
								{
								Params.Bs_EndRow = Params.Bs_EndRow + 8;
								Params.BS_roiVelRegion1 = cv::Rect(Params.Bs_EndRow + 470 + 170, 1, 492, 20);
								Params.BS_roiVelRegion2 = cv::Rect(Params.Bs_EndRow, 1, 470, 20);
								std::vector<cv::Rect> roiList;
								roiList.push_back(Params.BS_roiVelRegion1);
								roiList.push_back(Params.BS_roiVelRegion2);
								//roiList.push_back(Params.BS_roiVelRegion3);
								//roiList.push_back(Params.BS_roiVelRegion4);
								// Initialise the tracker if required
								// Initialise the tracker
								tracker2D.updateRoi(roiList);
								}
								break;

								case 'v':
								{
								if (UseFile)
								UseFile = false;
								else
								UseFile = true;
								FrameCount = 0;
								//					getDropletSize.ResetVectors();
								}
								break;
								*/
								}// Keypress CASE
								
					}// kbhit
			}// StopLoop (end of main process loop)
								
				
		QueryPerformanceCounter(&end);

		{
			// Display the elapsed time
			double freq, diff;
			diff = (double)(end.QuadPart - start.QuadPart);
			freq = (double)Frequency.QuadPart;
			diff = diff / freq;
			cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;

			printf("%d frames in %g sec, rate %g\n", grabbedImages, diff, ((double)Params.NumberOfImagesToGrab) / diff);

			cout << "Num frames" << _i64toa_s(grabbedImages, &string[0], 30, 10) << endl;
		}

		// Release the video file on leaving.
#ifdef recordVideo

		cvVideoCreator.release();
#endif
	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
		exitCode = 1;
		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
	}

	// Comment the following two lines to disable waiting on exit.
	cerr << endl << "Press Enter to exit." << endl;
	while (cin.get() != '\n');

	return exitCode;
}
