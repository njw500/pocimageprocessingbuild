#pragma once
#ifndef BootstrapGUI_C
#define BootstrapGUI_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\..\Utilities\Utilities\CamParams.h"
#include "..\parameters\BootstrapSysParams.h"
#include "..\tracking\Tracking2D.h"
#include "..\..\Utilities\Utilities\BaslerCamera.h"


// Namespace for using cout.
using namespace std;


/* This class adds simple controls using the openCV gui interface. It makes use
of trackbars to establish callbacks that act on the system parameters as
well as a click and drag interface for changing the cameras ROI.*/
class BootstrapGui
{
public:
	BootstrapGui();
	~BootstrapGui();
	void BootstrapGui::BootstrapGuiInit(Tracker2D *tracker, BootstrapSysParams *BootstrapSysParams_in, BaslerCamera *camera_in);
	void onMouseClickInternal(int ev, int x, int y);

private:
	//CellDetector	*cellDetector;
	BootstrapSysParams *bootstrapSysParams;
	Tracker2D *tracker;
	BaslerCamera *camera;
	void updateParams();
	//static void BootstrapGui::onTrackbar(int, void*);
	static void BootstrapGui::onTrackbar(int newValue, void* object)
	{
		if (newValue < 1)
			newValue = 1;
		BootstrapGui* bootstrapGui = (BootstrapGui*)(object);
		bootstrapGui->updateParams();
		//
	}
	static void BootstrapGui::onBootstrapMouseClick(int ev, int x, int y, int, void* obj)
	{
		BootstrapGui* sg = static_cast<BootstrapGui*>(obj);
		if (sg)
			sg->onMouseClickInternal(ev, x, y);

	}

	//friend void onBootstrapMouseClick(int ev, int x, int y, void* obj);


	void createTrackbars();
	void createMouseCallbacks();
	bool mouseActive; // Whether the button is down
	int lastX; // Last x position when button is down
	int lastY; // Last y position when button is down
	int gain;
};



#endif