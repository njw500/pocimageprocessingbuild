//#include "stdafx.h"

#include "..\parameters\BootstrapSysParams.h"
#include "BootstrapGui.h"

// For Debug Macros
//#include "VideoOutput.h"

using namespace cv;

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC
BootstrapGui::BootstrapGui()
{
	mouseActive = false;
}

BootstrapGui::~BootstrapGui()
{
}

void BootstrapGui::BootstrapGuiInit(Tracker2D *tracker_in, BootstrapSysParams *bootstrapSysParams_in, BaslerCamera *camera_in)
{
	tracker = tracker_in;
	bootstrapSysParams = bootstrapSysParams_in;
	camera = camera_in;

	// Now draw the image
	namedWindow("Main Output", WINDOW_AUTOSIZE);

	createTrackbars();
#ifdef ENABLECAMERA
	createMouseCallbacks();
#endif
	//onTrackbar(bootstrapSysParams->bootstrapRoiY1, 0);
}
void BootstrapGui::updateParams()
{
	
	// Reset all the parameters with some bounds checking
//	bootstrapSysParams->Bs_EndSlowZone.setValue(bootstrapSysParams->Bs_EndSlowZone.val, bootstrapSysParams->bootstrapRoiY1.getMin(), bootstrapSysParams->bootstrapRoiY2.val - 2);
	//bootstrapSysParams->Bs_FastZoneEnd.setValue(bootstrapSysParams->Bs_FastZoneEnd.val, max(1, bootstrapSysParams->bootstrapRoiY1.val + 2), bootstrapSysParams->bootstrapRoiY2.getMax());
	bootstrapSysParams->Bs_MidSlowZone.setValue(bootstrapSysParams->Bs_MidSlowZone.val);
	bootstrapSysParams->Bs_EndRow.setValue(bootstrapSysParams->Bs_EndRow.val);
	bootstrapSysParams->Bs_StartRow.setValue(bootstrapSysParams->Bs_StartRow.val);
	bootstrapSysParams->Bs_EndSlowZone.setValue(bootstrapSysParams->Bs_EndSlowZone.val);
	bootstrapSysParams->Bs_FastZoneEnd.setValue(bootstrapSysParams->Bs_FastZoneEnd.val);
	bootstrapSysParams->updateRoi();
	std::vector<cv::Rect> roiList;
	roiList.push_back(bootstrapSysParams->BS_roiVelRegion1);
	roiList.push_back(bootstrapSysParams->BS_roiVelRegion2);
	roiList.push_back(bootstrapSysParams->BS_roiVelRegion3);
	roiList.push_back(bootstrapSysParams->BS_roiVelRegion4);
	tracker->updateRoi(roiList); 
	/*bootstrapSysParams->bootstrapCheckDropRowTop.setValue(bootstrapSysParams->bootstrapCheckDropRowTop.val, bootstrapSysParams->bootstrapCheckDropRowTop.getMin(), bootstrapSysParams->bootstrapCheckDropRowBottom.val - 2);
	bootstrapSysParams->bootstrapCheckDropRowBottom.setValue(bootstrapSysParams->bootstrapCheckDropRowBottom.val, max(1, bootstrapSysParams->bootstrapCheckDropRowTop.val + 2), bootstrapSysParams->bootstrapCheckDropRowBottom.getMax());*/
/*
	setTrackbarPos("DropRowTop", "Main Output", bootstrapSysParams->bootstrapCheckDropRowTop.val);
	setTrackbarPos("DropRowBot", "Main Output", boo
tstrapSysParams->bootstrapCheckDropRowBottom.val);*/

}

void BootstrapGui::createTrackbars()
{
	cv::createTrackbar("Right Row", "Main Output", &(bootstrapSysParams->Bs_StartRow.val), bootstrapSysParams->Bs_StartRow.getMax(), &BootstrapGui::onTrackbar, this);
	cv::createTrackbar("Left Row", "Main Output",   &(bootstrapSysParams->Bs_EndRow.val), bootstrapSysParams->Bs_EndRow.getMax(), &BootstrapGui::onTrackbar, this);
	cv::createTrackbar("FastZone", "Main Output", &(bootstrapSysParams->Bs_FastZoneEnd.val), bootstrapSysParams->Bs_FastZoneEnd.getMax(), &BootstrapGui::onTrackbar, this);
	cv::createTrackbar("MidZone", "Main Output", &(bootstrapSysParams->Bs_MidSlowZone.val), bootstrapSysParams->Bs_MidSlowZone.getMax(), &BootstrapGui::onTrackbar, this);
	cv::createTrackbar("EndZone", "Main Output", &(bootstrapSysParams->Bs_EndSlowZone.val), bootstrapSysParams->Bs_EndSlowZone.getMax(), &BootstrapGui::onTrackbar, this);
	//cv::createTrackbar("FastZoneEnd", "Main Output", &(bootstrapSysParams->Bs_StartRow.val), bootstrapSysParams->Bs_StartRow.getMax(), &BootstrapGui::onTrackbar, this);



	//	cv::createTrackbar("Roi Y2", "Main Output", &(bootstrapSysParams->bootstrapRoiY2.val), bootstrapSysParams->bootstrapRoiY1.getMax(), &BootstrapGui::onTrackbar, this);*/

}

void BootstrapGui::createMouseCallbacks()
{
	setMouseCallback("Main Output", onBootstrapMouseClick, this);
}

void BootstrapGui::onMouseClickInternal(int ev, int x, int y)
{

	if (ev == EVENT_MOUSEMOVE)
	{
		if (mouseActive)
		{
			int moveX = lastX - x;
			int moveY = lastY - y;
			int sign;
			if (moveX > 0)
				sign = 1;
			else
				sign = -1;

			camera->RoiHoriz(abs(moveX), sign);
			if (moveY > 0)
				sign = 1;
			else
				sign = -1;
			camera->RoiVert(abs(moveY), sign);
		}
	}
	if (ev == EVENT_LBUTTONDOWN)
	{
		lastX = x;
		lastY = y;
		mouseActive = true;
	}

	if (ev == EVENT_LBUTTONUP)
	{
		mouseActive = false;
	}
}