//#include "stdafx.h"
#include "BootstrapSysParams.h"
#include "DebugFlags.h"

// For Debug Macros

// Namespace for using cout
using namespace std;

void BootstrapSysParams::updateRoi()
{
	BS_roiVelRegion1 = cv::Rect(Bs_EndRow.val, 1, (Bs_FastZoneEnd.val - Bs_EndRow.val), 20);
	// Just to right of diluter
	BS_roiVelRegion2 = cv::Rect(Bs_EndSlowZone.val, 1, (Bs_MidSlowZone.val - Bs_EndSlowZone.val), 20);
	BS_roiVelRegion3 = cv::Rect(Bs_MidSlowZone.val, 1, (Bs_StartRow.val - Bs_MidSlowZone.val), 20);
	// Dubious diluter zone
	BS_roiVelRegion4 = cv::Rect(Bs_FastZoneEnd.val - 20, 1, ((Bs_EndSlowZone.val - Bs_FastZoneEnd.val) + 40), 20);
}

BootstrapSysParams::BootstrapSysParams()
{

	/* SET CAMERA PARAMETERS */
	FrameROIr.x = 1;
	FrameROIr.y = 664-10;
	FrameROIr.width = 2600;// 2600;// 1800;
	FrameROIr.height = 25;
	NumGrab = 500;

	cParams.SetRect(FrameROIr);
	cParams.BlockMode = true;
	cParams.AutoStart = true;
	cParams.CameraSerial = "21982883";// Boot

	cParams2.BlockMode = false;
	cParams2.AutoStart = false;
	cParams2.CameraSerial = "21997816";// Sort
	
	//cParams2.SetRect(cv::Rect( SET IT HERE ))

	InteractiveMode = false; // Start in single frame display and cursor mode

	//CameraSerial = "21799755";// Local office
	//CameraSerial = "21982883";// Generator
	// 21997816;  // SORTER
	// 22025914;  // DISP
	// 21982883;  // BOOT

	DisplayMode[WGrab] = true; // Display this window   WOutput
	DisplayMode[WHough] = true;
	DisplayMode[WBack] = false;
	DisplayMode[WCropped] = true;
	DisplayMode[WEdges] = false;
	DisplayMode[WHist] = true;
	DisplayMode[WOutput] = true;
	DisplayMode[WDisplayEd] = true;
	DisplayMode[WTrackerFilter] = true;

	//VideoFileName = "C:/Temp/161129_bootstrap_newDrops_A7000_B1000_V6Close_600FPS.avi";
	//VideoFileName = "c:/Temp/161129_bootstrap_newDrops_A7000_B1000_600FPS.avi";
	//VideoFileName = "V:/Projects/Pascal/Bootstrap/Blocked/gain8-6dB.avi";// gain_0dB.avi";
	//VideoFileName = "V:/Projects/Pascal/Bootstrap/Blocked/Startup.avi";
	VideoFileName = "c:/Temp/gain8-6dB.avi";

//#ifdef ENABLECAMERA
	/* SETUP ALGORITHM PARAMETERS */
	Bs_StartRow.setValue(	2530,	1500, FrameROIr.width);
	Bs_EndRow.setValue(		730,	300, FrameROIr.width);	// The end row (on the left of the image) representing the final point that the journey time should be calculated
	Bs_FastZoneEnd.setValue(1150,	600, FrameROIr.width);
	Bs_EndSlowZone.setValue(1300,	800, FrameROIr.width);
	Bs_MidSlowZone.setValue(2000,	1000,FrameROIr.width);	

	updateRoi();
	// Setup the velocity regions for which the velocity will be assessed
	// Fast section

//#endif

#ifdef EMULATEFROMFILE
	// This block wins if both #define flags are active here

	VideoIsPreCropped = true;
	// If the above flag is true then the ROI below is ignored
	/* Use for mangled files ONLY
	*/

#endif



}

// Do nothing in Destructor
BootstrapSysParams::~BootstrapSysParams()
{
}



// A helper function to check (and correct) the bounds of key function parameters
bool BootstrapSysParams::checkBounds()
{
	// Placeholder: needs implementing for Bootstrap
	bool retVal = true;
	/*retVal = retVal && checkVarBounds(&, 2, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&, 2, cParams.FrameROIr.height - 2);
	retVal = retVal && checkVarBounds(&sorterRoiY1, 2, sorterRoiY2);
	retVal = retVal && checkVarBounds(&sorterCellPadding, 1, 50);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelCell, 1, 255);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelDrop, 1, 255);
	retVal = retVal && checkVarBounds(&sorterCellPadding, 1, 50);
	retVal = retVal && checkVarBounds(&sorterOpenSize, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&sorterErodeSize, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&(cParams.gain), (int)0, (int)12);
	*/
	// Need to add some bounds checking for exposure and gain. 
	return retVal;

}

bool BootstrapSysParams::checkVarBounds(int *var, int mn, int mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

bool BootstrapSysParams::checkVarBounds(double *var, double mn, double mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

