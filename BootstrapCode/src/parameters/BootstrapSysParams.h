
#pragma once

#ifndef BOOTSTRAPSYSPARAMS_C
#define BOOTSTRAPSYSPARAMS_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\..\..\Utilities\Utilities\CamParams.h"
#include "..\..\..\Utilities\Utilities\Params.h"


// Namespace for using cout.
using namespace std;

#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WDisplayEd 7
#define WTrackerFilter 8

class BootstrapSysParams
{
public:
	BootstrapSysParams();
	void BootstrapSysParams::updateRoi();
	~BootstrapSysParams();

	// Define all the parameters that are tuned and tuneable in the algorithm
	static const uint NumberOfImagesToGrab = 600;
	int NumGrab;

	int roi1;

	CamParams cParams;
	CamParams cParams2;// 2nd camera parameter block

	const bool DoFrameRotate = false;
	bool VideoIsPreCropped = false;
	const float FrameRotate = 0.0;

	int Frame_ROI_Left, Frame_ROI_Top, Frame_ROI_Width, Frame_ROI_Height;
	cv::Rect FrameROIr;

	int BootstrapFrame_ROI_Left, BootstrapFrame_ROI_Top, BootstrapFrame_ROI_Width, BootstrapFrame_ROI_Height;
	
	cv::Rect	BS_roiVelRegion2, BS_roiVelRegion1;
	cv::Rect	BS_roiVelRegion4, BS_roiVelRegion3;

	const float Zone_Detection_Radius = 0.4f;	
	const float AverageDecay = 0.1f;

	const double Drop_minDist = 30;
	const double Drop_minRadius = 20;
	const double Drop_maxRadius = 45;
	const int	   MinDropsPerFrame = 4;

	ParamDouble LoVelocityLimit;// 2.0;
	ParamDouble HiVelocityLimit;// = 300;

	bool InteractiveMode;
	bool DisplayMode[8];
	cv::String VideoFileName;

	ParamInt Bs_StartRow, Bs_EndRow;
	ParamInt BS_roi_y1; // = 8
	ParamInt BS_roi_y2;// = 9;
	ParamInt Bs_FastZoneEnd;// = 1150;// 1080
	ParamInt Bs_EndSlowZone; //= 1300;// 1380
	ParamInt Bs_MidSlowZone;// = 2000;
	// RIGHT
	//
	
	
private:
	int ok;
	bool BootstrapSysParams::checkVarBounds(double *var, double mn, double mx);
	bool BootstrapSysParams::checkVarBounds(int *var, int mn, int mx);
	bool BootstrapSysParams::checkBounds();

};

#endif