#include "stdafx.h"
#include "BaslerCamera.h"

#include "WriteVector.h"// DEBUG ONLY

#include <conio.h> // KBhit etc
#include "DisplayIm.h"
// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
// Namespace for using OpenCV objects.
using namespace cv; 

// For Debug Macros


// Namespace to setup USB3 Basler Ace parameters
using namespace Basler_UsbCameraParams;

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using cout
using namespace std;  


// Development Macro

//Enumeration used for distinguishing different events.
enum MyEvents
{
	eFrameBurstStartEvent = 100,
	eFrameBurstStartOvertriggerEvent = 200,
	eMyExposureEndEvent = 300
	// More events can be added here if required...
};

// Event Handler for Camera Events
class CCameraEventCallback : public CBaslerUsbCameraEventHandler
{

private:

	// Pointer to the first frame received flag
	bool* mIsFirstFramePtr;

public:

	// Register the First Frame Recv Flag
	void RegReestCounterPtr(bool* firstFrameRecvFlagPtr){
		mIsFirstFramePtr = firstFrameRecvFlagPtr;
	}


	// Only very short processing tasks should be performed by this method. Otherwise, the event notification will block the
	// processing of images.
	virtual void OnCameraEvent(CBaslerUsbInstantCamera& camera, intptr_t userProvidedId, GenApi::INode* /* pNode */)
	{
		
		switch (userProvidedId)
		{

		case eFrameBurstStartOvertriggerEvent:
		case eFrameBurstStartEvent:

			// We reset whenever a signal on the Frame Burst Start is received (overTrigger or not)

			// Print
			//cout << "Frame Burst Start Trigger Received - Flag Cleared" << endl;

			// Clear the flag for the first frame received when we get a new Frame Burst Start Trigger
			*(this->mIsFirstFramePtr) = true;

			break;

		case eMyExposureEndEvent: // Exposure End event

			// No longer used

			break;

		}
	}
};


// Do nothing in Constructor
BaslerCamera::BaslerCamera()
{
	CameraOK = -1; // NOT OK
	Camera_Default_Frame_Rate		= 1200;	 // Clamp to 100Hz
	Camera_Default_Exposure_Time	= 60;    // In uSeconds

	FastMode			 = true;// Try it on init
	EventSelectorAllowed = true;
	ChunkAllowed		 = true;

	LimitBandwidth=true;// It isnt safe to unlimit for big cameras on workstations 
	const int BandWidthLimit=300000000;
	
}

// Do nothing in Destructor
BaslerCamera::~BaslerCamera()
{
	cout << "Basler shutting down " << endl;
}


//bool BaslerCamera::Init(bool CallbacksEnabled, string camerSerialNumber, int parameterSet)
bool BaslerCamera::Init(const string camerSerialNumber, CamParams Params)
{
	bool OK = false;

	// Start with 0 frames
	this->mTotalNumberFrames = 0;
	
	// STARTUP PYLON 
	PylonInitialize();

	// CONNECT CAMERA 
	AutoStart = Params.AutoStart;

	// Connect up to the Camera with the specified serial number
	CDeviceInfo deviceInfo;

	// Convert our parsed string into a pylon string
	bool NoSerial = camerSerialNumber.empty();

	String_t* camerSerialNumberPylon = new String_t(camerSerialNumber.c_str());
	deviceInfo.SetSerialNumber(*camerSerialNumberPylon);

	// Explicitly tell Pylon it is a USB Camera to enumerate faster
	deviceInfo.SetDeviceClass(BaslerUsbDeviceClass);

	// Connect up to the camera 
	try
	{
		if (NoSerial) // Grab the first camera
			mPylonCameraPtr = new CBaslerUsbInstantCamera(CTlFactory::GetInstance().CreateFirstDevice());
		else
			mPylonCameraPtr = new CBaslerUsbInstantCamera(CTlFactory::GetInstance().CreateDevice(deviceInfo));
	}
	catch(RuntimeException e)
	{
		// Show user error message
		int msgboxID = MessageBoxA(NULL, ("Couldn't connect to camera with Serial Number: " + camerSerialNumber).c_str(), "Camera Error", MB_ICONEXCLAMATION);
		return(false);
	}
	

	// Turn Camera Event processing - need to do this when camera is closed
	mPylonCameraPtr->GrabCameraEvents = true;

	// Open the camera
	(mPylonCameraPtr)->Open();

	// Set all the key settings and store those we might need

	BaslerCamera::GetSettings();// Get the current key parameters

	BaslerCamera::SetSettings();

	// Last grab and conversions are false by default
	mLastGrabSuccessful			= false;
	mLastConversionSuccessful	= false;

	// Initialise memory for the Last Grabbed Image Pointer
	CGrabResultPtr* tempPtr = new CGrabResultPtr();

	mLastGrabbedResultPtr = *tempPtr;

	// Print
	cout << "Successfully connected to Camera: " << this->CamSerial << ", starting grabbing" << endl;

	// Print out the grabbing priority
	// TODO: enum this....
	// Basler documentation recommed a value of 24 or higher for the Grabbing thread priority
	mPylonCameraPtr->GrabLoopThreadPriorityOverride = true;
	mPylonCameraPtr->GrabLoopThreadPriority =15; // Modified from 24 to 15. Throws an error on the Mini-Pcs when this is run, work out why later on....
	cout << "Grabbing Thread Priority: " << mPylonCameraPtr->GrabLoopThreadPriority() << endl;
	// Basler doucmentation recommend a value of 25 or higher for the Instant Camera's internal grabbing engine priorty
	cout << "Grabbing Engine Priority: " << mPylonCameraPtr->InternalGrabEngineThreadPriority() << endl;

	SetROI(Params.FrameROIr);
	SetExposure(Params.exp);
	SetGain(Params.gain.val);

	// Print our settings
	if (VerboseStart)
		BaslerCamera::PrintSettings();

	if (AutoStart)
		this->Run();

	CameraOK = true;

	return(true);
}// Init function


void BaslerCamera::Run()
{
	// Start grabbing images using the specified grab strategy
	// GrabStrategy_LatestImages : Grab the last
	// GrabStrategy_LatestImageOnly: Grab the last, if its there or wait
	// GrabStrategy_OneByOne: Queue
	
	// if we are in block mode then we want a queue, otherwise we want the last available

	mPylonCameraPtr->StartGrabbing(GrabStrategy_LatestImages);
	//mPylonCameraPtr->StartGrabbing(CAMERA_GRAB_STRATEGY);

	//mPylonCameraPtr->StartGrabbing(CAMERA_GRAB_STRATEGY);
}

void BaslerCamera::Stop()
{
	mPylonCameraPtr->StopGrabbing();
}

// Grab the Next Image
void BaslerCamera::GrabNextImg(void)
{

	// Grab the next image using the set Grabbing Strategy and specified timeout
	try{
		mPylonCameraPtr->RetrieveResult(CAMERA_GRAB_TIMEOUT_MSEC, mLastGrabbedResultPtr, TimeoutHandling_ThrowException);

		// Image grabbed successfully?
		if (mLastGrabbedResultPtr->GrabSucceeded())
		{
			// If the grab succeeded then set flag
			mLastGrabSuccessful = true;
		}
		else
		{
			// If grab failed then flag is false
			mLastGrabSuccessful = false;
			cout << "Error: " << mLastGrabbedResultPtr->GetErrorCode() << " " << mLastGrabbedResultPtr->GetErrorDescription() << endl;
		}
	}catch (TimeoutException e)
	{

#ifdef CAMERA_PRINT_FRAME_DROPPED

		// Lock
		mLastGrabSuccessfulLock.lock();

		// If grab timed out then flag is false
		mLastGrabSuccessful = false;
		cout << "Grab Timed Out" << endl;

		// Unlock
		mLastGrabSuccessfulLock.unlock();

#endif // CAMERA_PRINT_FRAME_DROPPED

		}
}

// Get the grabbed image and convert into an OpenCV image
bool BaslerCamera::GetMatImage(cv::Mat& frame)
{
	bool OK = false;
	int err=-99;
	try
	{
		// Only perform a conversion if the last grab was successful
		if (mLastGrabSuccessful)
		{
			// Create a pylon ImageFormatConverter object.
			//CImageFormatConverter formatConverter;
			
			// Create a PylonImage that will be used to create OpenCV images later.
			//CPylonImage pylonImage;

			// Specify the output pixel format.
			mFormatConverter.OutputPixelFormat = PixelType_Mono8;

			// Convert the grabbed buffer to a pylon image.
			int err = mLastGrabbedResultPtr->GetErrorCode();

			if ((mLastGrabbedResultPtr->GetBuffer() == NULL) || (mLastGrabbedResultPtr->GetHeight() < 3) ||
				(mLastGrabbedResultPtr->GetWidth() < 3))
				OK = false;// Fail
			else
			{
				mFormatConverter.Convert(mPylonImage, mLastGrabbedResultPtr);
				//formatConverter.Convert(pylonImage, ptrGrabResult);

				// Create an OpenCV image from a pylon image.
				frame = cv::Mat(mLastGrabbedResultPtr->GetHeight(), mLastGrabbedResultPtr->GetWidth(),
					CV_8UC1, (uint8_t *)mPylonImage.GetBuffer());

				if ((frame.rows > 3) && (frame.cols > 3))
					OK = true;
			}
		}
	}

	catch (InvalidArgumentException e)
	{
		// Get an exception sometimes, not sure why
		cout << "Conversion Exception: " << e.what() << endl;
		cout << "Error code " << err << endl;
		cout << "Error: " << mLastGrabbedResultPtr->GetErrorCode() << " " << mLastGrabbedResultPtr->GetErrorDescription() << endl;
		return(false);
	}
	return(OK);
}


// Display loop for ROI adjust
bool BaslerCamera::DisplayLoop(cv::Rect &CurROIm)
{
	bool StopLoop = false;
	bool StopAfterLoop = false;
	int X, Y;

	X = GetCurWidth()/2;
	Y = GetCurHeight()/2;

	if (!CameraOK)
	{
		cout << "Camera is NOT OK. Cannot Display" << endl;
		return(false);
	}

	if (!mPylonCameraPtr->IsGrabbing())
	{
		this->Run();
		Sleep(50);
		StopAfterLoop = true;
	}

	Mat camframe,frame;

	while (!StopLoop)
	{

		GrabNextImg();

		if (GrabSuccesful())
		{
			bool ok = GetMatImage(camframe);
			if (ok)
			{
				frame = camframe.clone();
				cvtColor(frame, frame, COLOR_GRAY2BGR);
				line(frame, Point(X, 1), Point(X, frame.cols),
					Scalar(255, 255, 255), 1, 8);
				line(frame, Point(1,Y), Point(frame.rows,Y),
					Scalar(255, 255, 255), 1, 8);

				// Add Lines to the 
				displayIm("ROI ADJUST MODE (e to return to process)", frame);
			}
		}


		if (_kbhit())
		{
			cout << "ok" << endl;
			int key = _getch();
			switch (key)
			{
				case 'e':
					StopLoop = true;
				break;
			
				case '[':
				{
					float gain;
					uint16_t Exp, Rate;
					GetGainExpRate(gain, Exp, Rate);

					cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
					gain = gain + 0.5f;
					SetGain(gain);
				}
				break;

				case']':
				{
					float gain;
					uint16_t Exp, Rate;
					GetGainExpRate(gain, Exp, Rate);
					cout << "Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
					gain = gain - 0.5f;
					if (gain < 0.0)
						gain = 0.0;
					SetGain(gain);
				}
				break;

				case 'j':
				{
					RoiVert(1, -1);
					Rect Cur = GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;

				case 'k':
				{
					RoiVert(1, +1);
					Rect Cur = GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;

				case 'h':
				{
					RoiHoriz(1, -1);
					Rect Cur = GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;

				case 'l':
				{
					RoiHoriz(1, +1);
					Rect Cur = GetROI();
					cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
				}
				break;

				case ',':
				{
					X = X - 1;
					if (X < 0)
						X = 0;
				}
				break;
				
				case '.':
				{
					X = X + 1;
					if (X >= frame.rows)
						X = frame.rows-1;
				}
				break;
				case ';':
				{
					Y = Y - 1;
					if (Y < 0)
						Y = 0;
				}
				break;

			}

		}
	}

	CurROIm = GetROI();

	return(true);
}



// Convert the Image
/*
void BaslerCamera::ConvImg(void* DstPtr)
{
	
	try{

		// Only perform a conversion if the last grab was successful
		if (mLastGrabSuccessful)
		{
			// Convert the current image to the output image
			//formatConverter.Convert(pylonImage, ptrGrabResult);
			//mRawToImageConverterPtr->Convert(DstPtr, (OLED_WINDOW_X_RES*OLED_WINDOW_Y_RES*4), mLastGrabbedResultPtr);
			//mRawToImageConverterPtr->Convert(DstPtr,  mLastGrabbedResultPtr);

			mLastConversionSuccessful = true;

		}else{

			mLastConversionSuccessful = false;
		}

	}
	catch (InvalidArgumentException e){

		// Get an exception sometimes, not sure why
		//cout << "Conversion Exception: " << e.what() << endl;

	}
	

}*/


//// Getter function which returns a pointer to the most recently converted image
//void* BaslerCamera::GetLastestImg(){
//
//	// Only perform a conversion if the last grab was successful
//	if (mLastGrabSuccessful){
//
//		// Use the buffer which is not being used for conversion
//		if (!mCurrentImageConvOutputIsEven){
//
//			// Even Buffer - want the opposite one not being used
//			return mCurrConvImageEvenPtr->GetBuffer();
//
//		}
//		else{
//
//			// Odd Buffer - want the opposite one not being used
//			return mCurrConvImageOddPtr->GetBuffer();
//
//		}
//
//	}
//	else{
//
//		return nullptr;
//
//	}
//
//}

//// Switch the converted Image Out Buffers around
//void BaslerCamera::PingPongConvOutBuffer(){
//
//	if (mCurrentImageConvOutputIsEven){
//		mCurrentImageConvOutputIsEven = false;
//	}else{
//		mCurrentImageConvOutputIsEven = true;
//	}
//
//}

// Getter function which returns whether or not the most recently converted image is valid
bool BaslerCamera::IsLastestImgValid(){

	return mLastConversionSuccessful;

}

// Getter function which returns which number image in the sequence the last acquired image is
int BaslerCamera::GetCurrImageSequenceNumer(bool CheckingLeftCameraFlag)
{

	// Default return value
	int returnMe = 0;
	int mParameterSet = 0;// This should be removed and the 2nd block of code removed

	//// Helmet or Swift System?  <DEFAULT TO FIRST BLOCK>
	if (mParameterSet == 0)
	{

		// Increment for the Left Camera - do nothing for the right one
		if (CheckingLeftCameraFlag){

		mTotalNumberFrames++;
		}

		// Helmet System just returns the modulo 4 of the absolute counter
		returnMe = (mTotalNumberFrames % 4);

	}else if (mParameterSet = 1)
	{

		// Swift System reads the chunk value from the camera
		try{

			// The chunk data is attached to the grab result and can be accessed anywhere.
			// Generic parameter access:
			// This shows the access via the chunk data node map. This method is available for all grab result types.
			GenApi::CIntegerPtr chunkCounter(mLastGrabbedResultPtr->GetChunkDataNodeMap().GetNode("ChunkCounterValue"));

			// Which ImageNumber are we up to?
			int currCounter = mLastGrabbedResultPtr->GetImageNumber();

			// Is this the first frame since a start trigger pulse?
			if ((this->mIsFirstFrame)){

				// Yes, update the counter offset
				mFirstFrameCounterOffset = currCounter;

				// Clear the flag
				this->mIsFirstFrame = false;

			}

			// Correct for the offset and return
			returnMe = currCounter - mFirstFrameCounterOffset;

			//// Is this the first frame in a sequence?
			//if (!(this->mFirstFrameRecv)){

			//	// If not then this is the first frame
			//	// Do nothing to the returned value read but set the flag
			//	this->mFirstFrameRecv = true;

			//	// Print
			//	cout << "Counter Read: " << returnMe << " Flag Set" <<endl;
			//}
			//else{

			//	// If not then we have already received the first flag
			//	// Add one to it
			//	// This is because there is a latency of one read from the raw counter value (eg: raw camera read is 0, 0, 1, 2 and we want 0, 1, 2, 3)
			//	returnMe++;

			//	// Print
			//	cout << "Counter Read: " << returnMe << " Flag already Set" << endl;

			//}

		}
		catch(RuntimeException e)
		{

			// Do nothing - simply return 0 if we can't read the value
			// Will drop this sequence then resync

		}

	}

	return (returnMe);

}

// Put camera into Burst mode
bool BaslerCamera::Burst(std::vector<cv::Mat> &frames, std::vector<float> &DeltaT,const int NumFrames)
{
	if (mPylonCameraPtr->IsGrabbing())
	{
		cout << "Stopped current grab" << endl;
		mPylonCameraPtr->StopGrabbing();
	}
	
	//frames.erase(frames.begin(), frames.end());
	DeltaT.erase(DeltaT.begin(), DeltaT.end());

	mPylonCameraPtr->MaxNumBuffer = NumFrames;

	mPylonCameraPtr->StartGrabbing(NumFrames, GrabStrategy_OneByOne);

	mFormatConverter.OutputPixelFormat = PixelType_Mono8;

	CGrabResultPtr ptrGrabResult;
	int count = 0;
	int loop_count = 0;
	bool err;
	
	LARGE_INTEGER Frequency, start, end;
	vector<INT64> TimeStamp;

	TimeStamp.reserve(NumFrames);
	DeltaT.reserve(NumFrames);

	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&start);

	while (mPylonCameraPtr->IsGrabbing())
	{
		err=mPylonCameraPtr->RetrieveResult(1, ptrGrabResult, TimeoutHandling_Return);

		Mat current;
		if (err && ptrGrabResult->GrabSucceeded())
		{
			//cout << "FrameNum: " << ptrGrabResult->GetFrameNumber() << endl;

			mFormatConverter.Convert(mPylonImage, ptrGrabResult);

			// Create an OpenCV image from a pylon image.
			TimeStamp.push_back(ptrGrabResult->GetTimeStamp());
			current=cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(),
				CV_8UC1, (uint8_t *)mPylonImage.GetBuffer());
			frames[count] = current.clone();// Make copy before buffer is released . Otherwise the frame memory may randomly be released later
			count = count + 1;
			//cout << "Ready buffers: " << mPylonCameraPtr->NumReadyBuffers.GetValue() 	<< endl;
			//cout << "Output Queue Size " << mPylonCameraPtr->OutputQueueSize.GetValue() << endl;
		}
		loop_count++;
		
	}

	QueryPerformanceCounter(&end);

	double freq, diff;
	diff = (double)(end.QuadPart - start.QuadPart);
	freq = (double)Frequency.QuadPart;
	diff = diff / freq;

	double R = GetRealFrameRate();

	cout << endl << "Burst completed in " << diff << "sec, Rate: " << (NumFrames /diff) << " Set rate: " << R << endl << endl;


	//WriteVector(TimeStamp, TimeStamp.size() , "TimeStamp.csv");
	// Convert INT64 timestamp to time difference value
	DeltaT.push_back(0.0f);
	for (int i = 1; i < TimeStamp.size(); ++i)
		DeltaT.push_back((float)(TimeStamp[i] - TimeStamp[0]) / 1.0e6f);// Convert to ms
	WriteVector(DeltaT, TimeStamp.size(), "TimeOffset.csv");

	return(true);
}

void BaslerCamera::GetSettings()
{
	// Retreive and store the parameters we want

	this->CamSerial = mPylonCameraPtr->GetDeviceInfo().GetSerialNumber();
	this->CamModel  = mPylonCameraPtr->GetDeviceInfo().GetModelName();

	this->CamWidth  = mPylonCameraPtr->Width();
	this->CamHeight = mPylonCameraPtr->Height();

	this->Xoffset=mPylonCameraPtr->OffsetX();
	this->Yoffset= mPylonCameraPtr->OffsetY();

	this->CamGain = mPylonCameraPtr->Gain();
	this->CamExposureTime = mPylonCameraPtr->ExposureTime();
	this->Current_Frame_Rate = mPylonCameraPtr->AcquisitionFrameRate();

	this->PixelFormat = LOOK_UP_PIXEL_FORMAT(mPylonCameraPtr->PixelFormat());

	string str = BOOL_TO_STRING(mPylonCameraPtr->DeviceLinkThroughputLimitMode());

	this->BandWidthLimit = (int)mPylonCameraPtr->DeviceLinkThroughputLimit();// BandWidthLimit 210 654 945
	this->CurBandWidth = (int)mPylonCameraPtr->DeviceLinkCurrentThroughput();

	//int Temp = mPylonCameraPtr->DeviceTemperature();

	WidthInc = mPylonCameraPtr->Width.GetInc();
	HeightInc = mPylonCameraPtr->Height.GetInc();

	xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();


	RealFrameRate=mPylonCameraPtr->ResultingFrameRate();

}

double BaslerCamera::GetRealFrameRate()
{
	return(mPylonCameraPtr->ResultingFrameRate());
}

// Just print out the current state
// NO HIDDEN SETS
void BaslerCamera::PrintSettings(){

	// Print the model name and serial number of the camera.
	// Expect GetSettings to be called first.... but to be sure
	GetSettings();

	cout << endl<< "--------------- DEVICE -------------" << endl << endl;
	
	cout << "Device Serial Number:   " << CamSerial << endl;
	cout << "Device Model:           " << CamModel << endl;

	// Print out all Key Parameters

	cout << endl<< "----------- ACQUISITION ------------" << endl << endl;
	
	cout << "Shutter Mode:           " << LOOK_UP_SHUTTER_MODE_SEL(mPylonCameraPtr->ShutterMode()) << endl;
	cout << "Exposure Auto:          " << LOOK_UP_EXPOSURE_AUTO_SEL(mPylonCameraPtr->ExposureAuto()) << endl;
	cout << "Exposure Mode:          " << LOOK_UP_EXPOSURE_MODE_SEL(mPylonCameraPtr->ExposureMode()) << endl;
	cout << "Exposure Time:          " << CamExposureTime << endl;

	if (this->FastMode)
		cout << "Sensor Readout Mode:    " << LOOK_UP_SENSOR_READOUT_MODE_SEL(mPylonCameraPtr->SensorReadoutMode()) << endl;
	else
		cout << "Sensor: No fast mode" << endl;

	cout << "Sensor Readout Time:    " << mPylonCameraPtr->SensorReadoutTime() << endl;
	cout << "Sensor Frame Rate:      " << mPylonCameraPtr->ResultingFrameRate() << endl;

	cout << "Constant FrameRate:     " << BOOL_TO_STRING(mPylonCameraPtr->AcquisitionFrameRateEnable()) << endl;

	cout << "Acquisition Frame Rate: " << this->Current_Frame_Rate<< endl;
	

	cout << "Acquisition Burst Count:" << mPylonCameraPtr->AcquisitionBurstFrameCount() << endl;
	cout << "Ext Trigger Enabled:    " << BOOL_TO_STRING(mPylonCameraPtr->TriggerMode()) << endl;
	cout << "Ext Trigger Type:       " << LOOK_UP_TRIGGER_TYPE_SEL(mPylonCameraPtr->TriggerSelector()) << endl;
	cout << "Ext Trigger Source:     " << LOOK_UP_TRIGGER_SOURCE_SEL(mPylonCameraPtr->TriggerSource()) << endl;
	cout << "Ext Trigger Delay:      " << mPylonCameraPtr->TriggerDelay() << endl;
	cout << "Ext Trigger Activation: " << LOOK_UP_TRIGGER_ACT_SEL(mPylonCameraPtr->TriggerActivation()) << endl;

	cout << "Max Num Buffer " << mPylonCameraPtr->MaxNumBuffer.GetValue() << endl;
	cout << "Output Queue Size " << mPylonCameraPtr->OutputQueueSize.GetValue() << endl;
	
	cout << "Limit USB Throughput:   " << BOOL_TO_STRING(mPylonCameraPtr->DeviceLinkThroughputLimitMode()) << endl;

	if (this->EventSelectorAllowed)
	{
		mPylonCameraPtr->EventSelector.SetValue(EventSelector_ExposureEnd);

		cout << "Exposure End Events On: " << BOOL_TO_STRING(mPylonCameraPtr->EventNotification()) << endl;
	}
	else
		cout << "CAMERA DOES NOT ALLOW EVENT SELELECTOR AS CODED" << endl;

	
	cout << endl<< "-------------- GAIN ----------------" << endl << endl;

	cout << "Gain Auto Adjust:       " << LOOK_UP_GAIN_AUTO_SEL(mPylonCameraPtr->GainAuto()) << endl;
	cout << "Auto Brightness Target: " << mPylonCameraPtr->AutoTargetBrightness() << endl;
	cout << "Current Gain:           " << mPylonCameraPtr->Gain() << endl;

	//cout << "Balance White Auto:     " << LOOK_UP_WHITE_BALANCE_SEL(mPylonCameraPtr->BalanceWhiteAuto()) << endl;
	//cout << "Light Source Preset:    " << LOOK_UP_LIGHT_SOURCE_SEL(mPylonCameraPtr->LightSourcePreset()) << endl;

	cout << endl << "---------- IMAGE FORMAT ------------" << endl << endl;
	
	cout << "Image Width:            " << mPylonCameraPtr->Width() << endl;
	cout << "Image Height:           " << mPylonCameraPtr->Height() << endl;
	cout << "X Offset:               " << mPylonCameraPtr->OffsetX() << endl;
	cout << "Y Offset:               " << mPylonCameraPtr->OffsetY() << endl;

	cout << "Pixel Format:           " << PixelFormat << endl;

	if (this->ChunkAllowed)
	{
		cout << "Chunk Mode Enabled:     " << BOOL_TO_STRING(mPylonCameraPtr->ChunkModeActive()) << endl;
		cout << "Chunk Data Included:    " << BOOL_TO_STRING(mPylonCameraPtr->ChunkEnable()) << endl;
		cout << "Chunk Data Type:        " << LOOK_UP_CHUNK_DATA_SEL(mPylonCameraPtr->ChunkSelector()) << endl;
	}
	else
		cout << "CAMERA DOES NOT ALLOW CHUNK MODE AS CODED" << endl;


	cout << "------------------------------------" << endl;
	cout << "---------------- IO ----------------" << endl;
	cout << "------------------------------------" << endl;
	mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE1);
	cout << "Line1 I/O:              " << LOOK_UP_GPIO_MODE(mPylonCameraPtr->LineMode()) << endl;
	cout << "Line1 Inverter:         " << LOOK_UP_LINE_INVERTED(mPylonCameraPtr->LineInverter()) << endl;
	mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE2);
	cout << "Line2 I/O:              " << LOOK_UP_GPIO_MODE(mPylonCameraPtr->LineMode()) << endl;
	cout << "Line2 Inverter:         " << LOOK_UP_LINE_INVERTED(mPylonCameraPtr->LineInverter()) << endl;
	cout << "Line2 Source:           " << LOOK_UP_LINE_SOURCE(mPylonCameraPtr->LineSource()) << endl;
	mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE3);
	cout << "Line3 I/O:              " << LOOK_UP_GPIO_MODE(mPylonCameraPtr->LineMode()) << endl;
	cout << "Line3 Inverter:         " << LOOK_UP_LINE_INVERTED(mPylonCameraPtr->LineInverter()) << endl;
	//cout << "Line3 Source:           " << LOOK_UP_LINE_SOURCE(mPylonCameraPtr->LineSource()) << endl;
	mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE4);
	cout << "Line4 I/O:              " << LOOK_UP_GPIO_MODE(mPylonCameraPtr->LineMode()) << endl;
	cout << "Line4 Inverter:         " << LOOK_UP_LINE_INVERTED(mPylonCameraPtr->LineInverter()) << endl;
	cout << "Line4 Source:           " << LOOK_UP_LINE_SOURCE(mPylonCameraPtr->LineSource()) << endl;

}



// Set Settings based on which parameter set we have passed
void BaslerCamera::SetSettings()
{

		////////// ACQUISITION ///////////

		// Should be setting
		//camera.MaxNumBuffer = 5000;

		mPylonCameraPtr->ShutterMode.SetValue(ShutterMode_Global);

		mPylonCameraPtr->ExposureAuto.SetValue(ExposureAuto_Off);
		
		mPylonCameraPtr->ExposureMode.SetValue(ExposureMode_Timed);

		// Always safe call
		SetExposure(Camera_Default_Exposure_Time);
		SetFrameRate(Camera_Default_Frame_Rate);

		int err;
		try
		{
			err = mPylonCameraPtr->SensorReadoutMode.GetAccessMode();

			mPylonCameraPtr->SensorReadoutMode.SetValue(SensorReadoutMode_Fast);
			this->FastMode = true;
		}
		catch (GenICam::GenericException &e)
		{
			cout << "No Fast Mode: Mode: " << err << endl;
			this->FastMode = false;
		}
	
		mPylonCameraPtr->AcquisitionFrameRateEnable.SetValue(true);
		
		mPylonCameraPtr->AcquisitionBurstFrameCount.SetValue(CAMERA_BURST_NUM_FRAMES);
		mPylonCameraPtr->TriggerSelector.SetValue(TriggerSelector_FrameStart);
		mPylonCameraPtr->TriggerSource.SetValue(TriggerSource_Software);
		mPylonCameraPtr->TriggerDelay.SetValue(CAMERA_TRIGGER_DELAY);
		mPylonCameraPtr->TriggerActivation.SetValue(TriggerActivation_RisingEdge);
		mPylonCameraPtr->TriggerMode.SetValue(TriggerMode_Off); // Needs to be after the calls above or will reset

		if (this->LimitBandwidth)
		{
			mPylonCameraPtr->DeviceLinkThroughputLimitMode.SetValue(DeviceLinkThroughputLimitMode_On);
			mPylonCameraPtr->DeviceLinkThroughputLimit.SetValue(BandWidthLimit);
		}
		else
			mPylonCameraPtr->DeviceLinkThroughputLimitMode.SetValue(DeviceLinkThroughputLimitMode_Off);


	try
	{
		mPylonCameraPtr->EventSelector.SetValue(EventSelector_ExposureEnd);
		mPylonCameraPtr->EventNotification.SetValue(EventNotification_On);
	}
	catch (GenICam::GenericException &e)
	{
		cout << "No Event selector" << endl;
		this->EventSelectorAllowed = false;
	}
		
	///////////// GAIN ///////////////

	try
	{
		mPylonCameraPtr->GainAuto.SetValue(GainAuto_Continuous);
	}
	catch (GenICam::GenericException &e)
	{
		cout << "No Gain Auto selector" << endl;
		//this->FastMode = false;
	}

	try
	{
		mPylonCameraPtr->AutoTargetBrightness.SetValue(CAMERA_TARGET_BRIGHTNESS);
	}
	catch (GenICam::GenericException &e)
	{
		cout << "No AutoTargetBrightness" << endl;
		//this->FastMode = false;
	}

		// Mono camera. Need to skip the next two lines
		//mPylonCameraPtr->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Continuous);
		//mPylonCameraPtr->LightSourcePreset.SetValue(LightSourcePreset_Daylight5000K);

		//////////////////////////////////
		///////// IMAGE FORMAT ///////////
		//////////////////////////////////

		int64_t Width = mPylonCameraPtr->WidthMax();
		int64_t Height = mPylonCameraPtr->HeightMax();
		int64_t mWidth = mPylonCameraPtr->SensorWidth();
		int64_t mHeight = mPylonCameraPtr->SensorHeight();

		mPylonCameraPtr->OffsetX.SetValue(0);
		mPylonCameraPtr->OffsetY.SetValue(0);
		mPylonCameraPtr->Width.SetValue(mPylonCameraPtr->SensorWidth());
		mPylonCameraPtr->Height.SetValue(mPylonCameraPtr->SensorHeight());

		try
		{
			int ok = mPylonCameraPtr->DecimationHorizontal.GetAccessMode();
			if ((ok == GenApi::NI) || (ok == GenApi::NA))
			{
				cout << "Not available" << endl;
			}
			mPylonCameraPtr->DecimationHorizontal.SetValue(1);
		}
		catch (GenICam::GenericException &e)
		{
			cout << "No Decimate control" << endl;
		}
		mPylonCameraPtr->BinningHorizontal.SetValue(1);
		mPylonCameraPtr->BinningVertical.SetValue(1);

		mPylonCameraPtr->PixelFormat.SetValue(PixelFormat_Mono8); // PixelFormat_BayerBG8);

		try
		{
			mPylonCameraPtr->ChunkModeActive.SetValue(true);
			mPylonCameraPtr->ChunkEnable.SetValue(true);
			mPylonCameraPtr->ChunkSelector.SetValue(ChunkSelector_CounterValue);
		}
		catch (GenICam::GenericException &e)
		{
			cout << "No Event selector" << endl;
			this->ChunkAllowed = false;
		}
		
		mPylonCameraPtr->CounterReset();   // Reset the Counter Value to 0

		////////////// IO ////////////////
	
		// Line 1 - Opto Input Only
		mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE1);
		mPylonCameraPtr->LineMode.SetValue(LineMode_Input);
		mPylonCameraPtr->LineInverter.SetValue(true);

		// Line 2 - Opto Output Only
		mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE2);
		mPylonCameraPtr->LineMode.SetValue(LineMode_Output);
		mPylonCameraPtr->LineInverter.SetValue(true);
		mPylonCameraPtr->LineSource.SetValue(LineSource_UserOutput1);

		// Line 3 - Direct GPIO
		mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE3);
		mPylonCameraPtr->LineMode.SetValue(LineMode_Output);
		mPylonCameraPtr->LineInverter.SetValue(true);
		mPylonCameraPtr->LineSource.SetValue(LineSource_ExposureActive);

		// Line 4 - Direct GPIO
		mPylonCameraPtr->LineSelector.SetIntValue(GPIO_LINE4);
		mPylonCameraPtr->LineMode.SetValue(LineMode_Output);
		mPylonCameraPtr->LineInverter.SetValue(true);
		mPylonCameraPtr->LineSource.SetValue(LineSource_UserOutput3);
	

}


void BaslerCamera::SetGain(const float value)
{
	float req_gain = value;
	mPylonCameraPtr->GainAuto.SetValue(GainAuto_Off);


	float mx = mPylonCameraPtr->Gain.GetMax();
	float mn = mPylonCameraPtr->Gain.GetMin();

	if (req_gain > mx)
		req_gain = mx - 0.01;

	if (req_gain < mn)
		req_gain = mn+0.001;

	mPylonCameraPtr->Gain.SetValue(req_gain);

	GetSettings();
}

void BaslerCamera::SetExposure(const float value)
{
	float req_exp = value;

	float mx = (float) mPylonCameraPtr->ExposureTime.GetMax();
	float mn = (float) mPylonCameraPtr->ExposureTime.GetMin();

	if (req_exp > mx)
		req_exp = mx - 0.01f;

	if (req_exp < mn)
		req_exp = mn + 0.001f;

	mPylonCameraPtr->ExposureTime.SetValue(req_exp);
	GetSettings();
}

void BaslerCamera::SetFrameRate(const int value)
{
	float mx = (float)mPylonCameraPtr->AcquisitionFrameRate.GetMax();
	float mn = (float)mPylonCameraPtr->AcquisitionFrameRate.GetMin();

	if (value < 0)
	{
		mPylonCameraPtr->AcquisitionFrameRateEnable.SetValue(false);
		mPylonCameraPtr->AcquisitionFrameRate.SetValue(mx);
	}
	else
	{	
		if (value > mx)
			mPylonCameraPtr->AcquisitionFrameRate.SetValue(mx);
		else
			if (value < mn)
				mPylonCameraPtr->AcquisitionFrameRate.SetValue(mn);
			else
				mPylonCameraPtr->AcquisitionFrameRate.SetValue(value);
	}

	cout << "Frame rate: " << this->GetRealFrameRate() << " fps " << endl;
}

void BaslerCamera::GetGainExpRate(float &Gain, uint16_t & ExpTime, uint16_t &FrameRate)
{
	Gain = mPylonCameraPtr->Gain();

	ExpTime = mPylonCameraPtr->ExposureTime();
	FrameRate = mPylonCameraPtr->AcquisitionFrameRate();
}

cv::Rect BaslerCamera::SetROI(cv::Rect &NewROI)
{
	// THIS CANNOT SET THE WIDTH or HEIGHT when running
	cv::Rect ActualROI;

	if (NewROI.width > mPylonCameraPtr->WidthMax())
		NewROI.width = (int)mPylonCameraPtr->WidthMax();

	if (NewROI.height > mPylonCameraPtr->HeightMax())
		NewROI.height = (int)mPylonCameraPtr->HeightMax();

	WidthInc = mPylonCameraPtr->Width.GetInc();
	HeightInc = mPylonCameraPtr->Height.GetInc();


	NewROI.width = ((int)(NewROI.width / WidthInc)) * WidthInc; // Make sure the width is acceptable
	NewROI.height = ((int)((NewROI.height / HeightInc) + 0.0)) * HeightInc;
	
	mPylonCameraPtr->Width.SetValue(NewROI.width);

	mPylonCameraPtr->Height.SetValue(NewROI.height);

	int64_t Width = mPylonCameraPtr->WidthMax();
	int64_t Height = mPylonCameraPtr->HeightMax();
	int64_t mWidth = mPylonCameraPtr->SensorWidth();
	int64_t mHeight = mPylonCameraPtr->SensorHeight();



	if (NewROI.width + NewROI.x > Width)
		NewROI.x = Width - NewROI.width;

	if (NewROI.height + NewROI.y > Height)
		NewROI.y = Height - NewROI.height;

	xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();

	NewROI.x = ((int)(NewROI.x / xInc)) * xInc;
	NewROI.y = ((int)(NewROI.y / yInc)) * yInc;

	mPylonCameraPtr->OffsetX.SetValue(NewROI.x);

	mPylonCameraPtr->OffsetY.SetValue(NewROI.y);// Crash

	GetSettings();// Repopulate internals

	ActualROI = this->GetROI();

	return(ActualROI);
}

/*
bool BaslerCamera::RoiUp(const int val)
{
	
	Point2i NewLocation;
	WidthInc = mPylonCameraPtr->Width.GetInc();
	HeightInc = mPylonCameraPtr->Height.GetInc();

	NewLocation.x = Xoffset;

	//xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();

	NewLocation.y = Yoffset + yInc+1;// Arb number

	int64_t MaxHeight = mPylonCameraPtr->HeightMax();

	if (CamHeight + NewLocation.y > MaxHeight)
		NewLocation.y = MaxHeight - CamHeight - yInc;

	NewLocation.y = ((int)(NewLocation.y / yInc)) * yInc;

	bool OK = SetROILocation(NewLocation);

	return(OK);
}
*/

bool BaslerCamera::RoiVert(const int val, const int Sign)
{

	Point2i NewLocation;
	WidthInc = mPylonCameraPtr->Width.GetInc();
	HeightInc = mPylonCameraPtr->Height.GetInc();

	NewLocation.x = Xoffset;

	//xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();

	NewLocation.y = Yoffset + ((Sign * (yInc + 1))); 

	int64_t MaxHeight = mPylonCameraPtr->HeightMax();

	if (CamHeight + NewLocation.y > MaxHeight)
		NewLocation.y = MaxHeight - CamHeight - yInc;
	
	if (NewLocation.y < 0)
		NewLocation.y = 0;

	NewLocation.y = ((int)(NewLocation.y / yInc)) * yInc;

	bool OK = SetROILocation(NewLocation);

	return(OK);
}

bool BaslerCamera::RoiHoriz(const int val, const int Sign)
{
	Point2i NewLocation;
	WidthInc = mPylonCameraPtr->Width.GetInc();
	HeightInc = mPylonCameraPtr->Height.GetInc();

	NewLocation.y = Yoffset;

	//xInc = mPylonCameraPtr->OffsetX.GetInc();
	xInc = mPylonCameraPtr->OffsetX.GetInc();

	NewLocation.x = (int) (Xoffset + ((Sign * (xInc + 1)) ));

	int64_t MaxWidth = mPylonCameraPtr->WidthMax();

	if (CamWidth + NewLocation.x > MaxWidth)
		NewLocation.x = MaxWidth - CamWidth - xInc;

	if (NewLocation.x < 0)
		NewLocation.x = 0;

	NewLocation.x = ((int)(NewLocation.x / xInc)) * xInc;

	bool OK = SetROILocation(NewLocation);

	return(OK);
}
/*
bool BaslerCamera::RoiDown(const int val)
{

	Point2i NewLocation;
	NewLocation.x = Xoffset;
	//xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();

	NewLocation.y = Yoffset - (yInc+1);// Arb number

	if (NewLocation.y < 0)
		NewLocation.y = 0;

	NewLocation.y = ((int)(NewLocation.y / yInc)) * yInc;

	if (NewLocation.y < 0)
		NewLocation.y = 0;

	bool OK = SetROILocation(NewLocation);

	return(OK);

}
*/

bool BaslerCamera::SetROILocation(cv::Point2i Location)
{
	xInc = mPylonCameraPtr->OffsetX.GetInc();
	yInc = mPylonCameraPtr->OffsetY.GetInc();

	if (Location.x < 0)
		Location.x = 0;
	if (Location.y < 0)
		Location.y = 0;
	
	Location.x = (int) (((int)(Location.x / xInc)) * xInc);
	Location.y = (int) (((int)(Location.y / yInc)) * yInc);

	mPylonCameraPtr->OffsetX.SetValue(Location.x);
	mPylonCameraPtr->OffsetY.SetValue(Location.y);
	GetSettings();// Repopulate internals

	return(true);
}

bool BaslerCamera::GetROI(cv::Rect &Roi)
{
	GetSettings();
	Roi.x = (int)this->Xoffset;
	Roi.y = (int)this->Yoffset;
	Roi.width = (int)this->CamWidth;
	Roi.height = (int)this->CamHeight;
	return(true);
}

cv::Rect  BaslerCamera::GetROI()
{
	GetSettings();
	Rect Roi;
	Roi.x = (int)this->Xoffset;
	Roi.y = (int)this->Yoffset;
	Roi.width = (int)this->CamWidth;
	Roi.height = (int)this->CamHeight;
	return(Roi);
}



