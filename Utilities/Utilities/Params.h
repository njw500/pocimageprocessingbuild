#pragma once
#ifndef PARAMS_C
#define PARAMS_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>




// Namespace for using cout.
using namespace std;


/* A class for storing integer parameters with automatic bound setting*/
class ParamInt
{
public:

	/* A class for storing integer parameters with automatic bound setting*/
	ParamInt();
	//ParamInt(int);
	~ParamInt();
	void ParamInt::setValue(int value);
	void ParamInt::setValue(int value, int mx, int mn);

	int ParamInt::getMax();
	int ParamInt::getMin();
	int val;

	static void ParamInt::onTrackbar(int newValue, void* object)
	{
		ParamInt* param = (ParamInt*)(object);
		param->setValue(newValue);
		//
	}
private:

	int mx;
	int mn;

};

/* A class for storing double parameters with automatic bound setting*/
class ParamDouble
{
public:
	ParamDouble();
	//ParamDouble(double);
	~ParamDouble();
	void ParamDouble::setValue(double value);
	void ParamDouble::setValue(double value, double mx, double mn);
	void ParamDouble::setValue(int value); // Set an integer value from 1 to 100 and convert accordingly. 
	double ParamDouble::getMax();
	double ParamDouble::getMin();
	double val;
	int intVal; // An equivalent integer value (mapped to the appropriate range);

	static void ParamDouble::onTrackbar(int newValue, void* object)
	{
		ParamDouble* param = (ParamDouble*)(object);
		param->setValue((double)newValue);
		//
	}
private:

	double mx;
	double mn;
};

#endif