#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "stdio.h"
#include <opencv2\opencv.hpp>

#include <algorithm>
#include <vector>
#include <numeric>
#include "WriteVector.h"


class cHistogram
{
private:
	cv::Mat Window;
	cv::String Name;

	float mn, mx;
	float mean, std;

	void cHistogram::DrawHistogram(const vector<int>& data, int Num, cv::String name);
	void cHistogram::CalculateStats(const vector<float>& data);
	int NumBins;
	vector<int> Hist;

public:

	bool DoPlot;
	bool UseFloat=false;// Bin calculation uses integer values ONLY (fine for generator, not for bootstrap)
	cHistogram();
	cHistogram(const bool DoPlot);
	cHistogram(cv::String name);
	~cHistogram();

	cv::Mat GetWindow(){ return(Window); };

	float GetHistogram(const vector<float>& data, int count, float &rmx, float &rmn, float &rstd, cv::String name);
	void   MakeEmptyWin();
	void  Annotate(cv::String str1, cv::String str2, cv::String str3);

	void Show();
};

#endif