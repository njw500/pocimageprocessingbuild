#ifndef WRITEVECTOR_H
#define WRITEVECTOR_H

#include "io.h"

#include <stdlib.h>

#include <algorithm>
#include <vector>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

using namespace std;

void WriteVector(const vector<float>& data, int sz, char * filename);
void WriteVector(const vector<int>& data, int sz, char * filename);
void WriteVector(const vector<__int64>& data, int sz, char * filename);

void WriteVector(cv::Mat res, char * filename);
void WriteVector(float *out, int sz, char * filename);



#endif