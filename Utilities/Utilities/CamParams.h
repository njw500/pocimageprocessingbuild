#pragma once

#ifndef CAMPARAMS_C
#define CAMPARAMS_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "Params.h"

// Namespace for using cout.
using namespace std;

class CamParams
{
public:
	CamParams();
	~CamParams();

	// Define all the parameters 

	void SetRect(cv::Rect In);

	bool StartCam = true;
	bool BlockMode = false;
	bool AutoStart = true;	// Start on Init

	// Define all the parameters 
	cv::Rect FrameROIr; // ROI to be requested
	cv::String CameraSerial; // Camera serial number

	int			exp; // Camera exposure
	ParamDouble gain; // Camera gain

private:
	int ok;


};

#endif