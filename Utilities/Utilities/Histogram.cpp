#include "stdafx.h"
#include "Histogram.h"

using namespace std;


using namespace cv;


cHistogram::cHistogram(String name)
{
	this->Name = name;
	this->DoPlot = true;
}

cHistogram::cHistogram(const bool doplot)
{
	this->DoPlot = doplot;
}

cHistogram::cHistogram()
{
	this->DoPlot = true;
}

// Do nothing
cHistogram::~cHistogram()
{
}

void cHistogram::MakeEmptyWin()
{
	const int c = 250;
	const int r = 300;
	Window = Mat3b(c, r, Vec3b(0, 0, 0));
}



void cHistogram::DrawHistogram(const vector<int>& data, int Num, String name)
{
	int max_value;
	Name = name;

	max_value = data[0];
	for (int i = 0; i < Num; ++i)
	{
		if (data[i] > max_value)
		{
			max_value = data[i];
		}
	}

	int x_sz = 5;
	int y_lim = 200;
	Point P1, P2;

	float y_scale = ((float)y_lim) / max_value;

	int r = x_sz*(Num + 2);
	int c = y_lim + 10;

	//Mat dst = Mat(r, c, CV_32F);

	//dst.setTo(0);
	Window = Mat3b(c, r, Vec3b(0, 0, 0));

	float val;
	for (int i = 0; i < Num; ++i)
	{
		P1.x = x_sz + (i*x_sz);
		P1.y = y_lim - 5;

		P2.x = x_sz + ((i + 1)*x_sz);
		val = (float)data[i] * y_scale;
		P2.y = y_lim - ((int)(val));

		rectangle(Window, P1, P2, (i % 2) ? Scalar(0, 100, 255) : Scalar(0, 0, 255), CV_FILLED);
	}

	//Show();

}

void cHistogram::Show()
{
	if (Window.cols < 49)
		return;
	namedWindow(Name, WINDOW_NORMAL);
	imshow(Name, Window);
	waitKey(2);

}

void cHistogram::Annotate(String str1, String str2, String str3)
{
	if (Window.cols < 49)
		return;
	
	putText(Window, str1, Point(10, 20), FONT_HERSHEY_SIMPLEX, .5, Scalar(0, 255, 255));
	putText(Window, str2, Point(10, 40), FONT_HERSHEY_SIMPLEX, .5, Scalar(0, 255, 127));
	putText(Window, str3, Point(10, 60), FONT_HERSHEY_SIMPLEX, .5, Scalar(200, 200, 200));
}
/*
void cHistogram::CalculateStats(const vector<float>& data, float &mx, float &mn, float &Average, float &Std)
{


}
*/

void cHistogram::CalculateStats(const vector<float>& data)
{
	// Calculate max,min,sum and sum^2
	mn = data[0];
	mx = mn;
	float sum = 0;
	float sum2 = 0;
	float val;
	float count = data.size();
	for (int i = 0; i < data.size(); ++i)
	{
		val = data[i];
		if (val > mx)
			mx = val;
		if (val < mn)
			mn = val;
		sum = sum + val;
		sum2 = sum2 + (val*val);
	}

	mean = sum / (float)count;
	std = (sum2 / (float)(count)) - (mean*mean);
	std = sqrt(std);

}

float cHistogram::GetHistogram(const vector<float>& data, int count, float &rmx, float &rmn, float &rstd, String name)
{
	//float mn, mx;
	NumBins = 50; // Fixed size

	//WriteVector(data, count, "Input.csv");

	CalculateStats(data);// Find max,min etc

	// Make histograme
	float range = (mx - mn);

	if (!UseFloat)
	{
		if ((int)(range + 0.5) < NumBins)
			NumBins = (int)(range + 0.5);
	}

	if (NumBins < 8)
		NumBins = 8;// Force to a non crash value

	Hist.resize(NumBins + 5);
	std::fill(Hist.begin(), Hist.end(),0);

	int ival;
	float val;
	//vector<int> BinVal(NumBins+1);

	for (int i = 0; i < count; ++i)
	{
		val = data[i];
		ival = (int) (((val - mn)/range) * NumBins) ;
		
		if (ival < 0)
			ival = 0;
		if (ival >= NumBins)
			ival = NumBins;
		Hist[ival] = Hist[ival] + 1;
	}

	// Find low 0.05 pecentile
	int limit = count / 20;
	int sum = 0;
	if (limit > 1)
	{
		int i=0;
		while ((i < Hist.size()) && (sum < limit))
		{
			sum = sum + Hist[i];
			i = i + 1;
		}
		// Now should have the percentile value
	}

	// Make stats
	rmx = mx;
	rmn = mn;
	rstd = std;

	//WriteVector(Hist, NumBins, "Hist.csv");
	if (DoPlot)
		DrawHistogram(Hist, NumBins, name);
	else
		Window = Mat(); 

	return(mean);
}





