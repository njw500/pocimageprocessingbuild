#include "stdafx.h"
#include "Params.h"



ParamInt::ParamInt()
{
	mn = 1;
	mx = 100;
	val = 1;
}

ParamInt::~ParamInt()
{

}

void ParamInt::setValue(int value)
{
	// Set value and force bounds
	val = value;
	if (val < mn)
		val = mn;
	if (val>mx)
		val = mx;

}

void ParamInt::setValue(int value, int mn_in, int mx_in)
{
	mx = mx_in;
	mn = mn_in;
	setValue(value);
}

ParamDouble::~ParamDouble()
{

}

int ParamInt::getMax()
{
	return mx;
}
int ParamInt::getMin()
{
	return mn;
}


ParamDouble::ParamDouble()
{
	mn = 1;
	mx = 100;
	val = 1;
}

void ParamDouble::setValue(double value)
{
	// Set value and force bounds
	val = value;
	if (val < mn)
		val = mn;
	if (val>mx)
		val = mx;

}

void ParamDouble::setValue(double value, double mn_in, double mx_in)
{

	mx = mx_in;
	mn = mn_in;
	setValue(value);
}

void ParamDouble::setValue(int value)
{
	setValue(((double)value/100)*(mx-mn)+mx);
}

double ParamDouble::getMax()
{
	return mx;
}
double ParamDouble::getMin()
{
	return mn;
}
