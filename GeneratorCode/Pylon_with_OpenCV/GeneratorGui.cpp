#include "stdafx.h"
#include "generatorSysParams.h"
#include "GeneratorGui.h"

// For Debug Macros
//#include "VideoOutput.h"

using namespace cv;

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC
GeneratorGui::GeneratorGui()
{
	mouseActive = false;
}

GeneratorGui::~GeneratorGui()
{
}

void GeneratorGui::generatorGuiInit(GetDropletSize *getDropletSize_in, GeneratorSysParams *generatorSysParams_in, BaslerCamera *camera_in)
{
	getDropletSize = getDropletSize_in;
	generatorSysParams = generatorSysParams_in;
	camera = camera_in;

	// Now draw the image
	namedWindow("Main Output", WINDOW_NORMAL);
	
	createTrackbars();

#ifdef ENABLECAMERA
	createMouseCallbacks();
#endif
	//onTrackbar(generatorSysParams->generatorRoiY1, 0);
}
void GeneratorGui::updateParams()
{
	generatorSysParams->checkBounds();

	// Reset all the parameters with some bounds checking
	generatorSysParams->roi_y1.setValue(generatorSysParams->roi_y1.val, 
		generatorSysParams->roi_y1.getMin(), generatorSysParams->roi_y2.val - 2);
	generatorSysParams->roi_y2.setValue(generatorSysParams->roi_y2.val,
		max(1, generatorSysParams->roi_y1.val + 2), generatorSysParams->roi_y2.getMax());
	generatorSysParams->padding_px.setValue(generatorSysParams->padding_px.val);
	generatorSysParams->thresholdLevel1.setValue(generatorSysParams->thresholdLevel1.val);
	generatorSysParams->thresholdLevel2.setValue(generatorSysParams->thresholdLevel2.val);
	generatorSysParams->structureEl1.setValue(generatorSysParams->structureEl1.val);
	generatorSysParams->structureEl2.setValue(generatorSysParams->structureEl2.val);
	generatorSysParams->min_pk_distance.setValue(generatorSysParams->min_pk_distance.val);
	generatorSysParams->widthHistory.setValue(generatorSysParams->widthHistory.val);
	generatorSysParams->checkDropRowBottom.setValue(generatorSysParams->checkDropRowBottom.val);
	generatorSysParams->checkDropRowTop.setValue(generatorSysParams->checkDropRowTop.val);
	// Supply the gain (double) from the int value
	generatorSysParams->cParams.gain.setValue(gain);

	/*generatorSysParams->generatorCheckDropRowTop.setValue(generatorSysParams->generatorCheckDropRowTop.val, generatorSysParams->generatorCheckDropRowTop.getMin(), generatorSysParams->generatorCheckDropRowBottom.val - 2);
	generatorSysParams->generatorCheckDropRowBottom.setValue(generatorSysParams->generatorCheckDropRowBottom.val, max(1, generatorSysParams->generatorCheckDropRowTop.val + 2), generatorSysParams->generatorCheckDropRowBottom.getMax());*/
	setTrackbarPos("RoiY1", "Main Output", generatorSysParams->roi_y1.val);
	setTrackbarPos("RoiY2", "Main Output", generatorSysParams->roi_y2.val);
	/*
	setTrackbarPos("DropRowTop", "Main Output", generatorSysParams->generatorCheckDropRowTop.val);
	setTrackbarPos("DropRowBot", "Main Output", generatorSysParams->generatorCheckDropRowBottom.val);*/
	//cellDetector->updateParams(generatorSysParams->generatorRoiY1.val, generatorSysParams->generatorRoiY2.val, generatorSysParams->generatorTopBottomBorder_px.val,
		//generatorSysParams->generatorThresholdLevelDrop.val, generatorSysParams->generatorMinPkDist.val, generatorSysParams->generatorCellPadding.val, generatorSysParams->generatorOpenSize.val,
		//generatorSysParams->generatorErodeSize.val, generatorSysParams->generatorThresholdLevelCell.val, generatorSysParams->generatorCheckDropRowTop.val, generatorSysParams->generatorCheckDropRowBottom.val);



}

void GeneratorGui::createTrackbars()
{
	cv::createTrackbar("StEl1", "Main Output", &(generatorSysParams->structureEl1.val), generatorSysParams->structureEl1.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("StEl2", "Main Output", &(generatorSysParams->structureEl2.val), generatorSysParams->structureEl2.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("Border", "Main Output", &(generatorSysParams->padding_px.val), generatorSysParams->padding_px.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("MinPkDist", "Main Output", &(generatorSysParams->min_pk_distance.val), generatorSysParams->min_pk_distance.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("WidthHist", "Main Output", &(generatorSysParams->widthHistory.val), generatorSysParams->widthHistory.getMax(), &GeneratorGui::onTrackbar, this);

	cv::createTrackbar("Gain", "Main Output", &gain, generatorSysParams->cParams.gain.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("RoiY1", "Main Output", &(generatorSysParams->roi_y1.val), generatorSysParams->roi_y1.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("RoiY2", "Main Output", &(generatorSysParams->roi_y2.val), generatorSysParams->roi_y2.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("Padding", "Main Output", &(generatorSysParams->padding_px.val), generatorSysParams->thresholdLevel1.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("Thresh1", "Main Output", &(generatorSysParams->thresholdLevel1.val), generatorSysParams->thresholdLevel1.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("Thresh2", "Main Output", &(generatorSysParams->thresholdLevel2.val), generatorSysParams->thresholdLevel2.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("DropRowTop", "Main Output", &(generatorSysParams->checkDropRowTop.val), generatorSysParams->checkDropRowTop.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("DropRowBot", "Main Output", &(generatorSysParams->checkDropRowBottom.val), generatorSysParams->checkDropRowBottom.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("StEl1", "Main Output", &(generatorSysParams->structureEl1.val), generatorSysParams->structureEl1.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("StEl2", "Main Output", &(generatorSysParams->structureEl2.val), generatorSysParams->structureEl2.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("Border", "Main Output", &(generatorSysParams->padding_px.val), generatorSysParams->padding_px.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("MinPkDist", "Main Output", &(generatorSysParams->min_pk_distance.val), generatorSysParams->min_pk_distance.getMax(), &GeneratorGui::onTrackbar, this);
	cv::createTrackbar("WidthHist", "Main Output", &(generatorSysParams->widthHistory.val), generatorSysParams->widthHistory.getMax(), &GeneratorGui::onTrackbar, this);
	//	cv::createTrackbar("Roi Y2", "Main Output", &(generatorSysParams->generatorRoiY2.val), generatorSysParams->generatorRoiY1.getMax(), &generatorGui::onTrackbar, this);*/

}

void GeneratorGui::createMouseCallbacks()
{
	setMouseCallback("Main Output", onGeneratorMouseClick, this);
}

void GeneratorGui::onMouseClickInternal(int ev, int x, int y)
{

	if (ev == EVENT_MOUSEMOVE)
	{
		if (mouseActive)
		{
			int moveX = 3*(lastX - x);
			int moveY = 3*(lastY - y);
			int sign;
			if (moveX > 0)
				sign = 1;
			else
				sign = -1;

			camera->RoiHoriz(abs(moveX), sign);
			if (moveY > 0)
				sign = 1;
			else
				sign = -1;
			camera->RoiVert(abs(moveY), sign);
		}
	}
	if (ev == EVENT_LBUTTONDOWN)
	{
		lastX = x;
		lastY = y;
		mouseActive = true;
	}

	if (ev == EVENT_LBUTTONUP)
	{
		mouseActive = false;
	}
}