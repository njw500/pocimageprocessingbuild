#ifndef GETDROPLETCONTOUR_H
#define GETDROPLETCONTOUR_H


#include "io.h"
#include <stdlib.h>
#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>
#include "GeneratorSysParams.h"

// Namespace for using OpenCV objects.
//using namespace cv;

// Namespace for using cout.
using namespace std;


//void get_droplet_contour(Mat background, Mat frame, float threshold1, float threshold2,
//					float detection_radius, Mat kernel);

int get_droplet_contour(cv::Mat background, cv::Mat frame, cv::Mat &OutputImage, vector<vector<cv::Point> > &output, float threshold1, float threshold2,
	float detection_radius, cv::Mat kernel, GeneratorSysParams Params);

//void complexhull(Mat greyframe, const float threshold1, const float threshold2);
bool complexhull(cv::Mat greyframe, vector<vector<cv::Point> > &Hull, const float threshold1, const float threshold2, GeneratorSysParams Params);

void complexhull_threshold(cv::Mat greyframe, vector<vector<cv::Point> > &Hull);

void contours_within_radius(vector<vector<cv::Point> >contours, vector<vector<cv::Point> > &output, const float detection_radius, cv::Mat frame);

//void displayIm(String str, Mat im);
//void displayIm(String str, Mat im, int flag);
//void ScaleImage(Mat in, Mat &out);
//void ScaleImageAbs(Mat in, Mat &out);

//float GetHistogram(const vector<float>& data, int count, float &mn, float &mx, float &std,String name);
//void drawHist(const vector<float>& data, Mat3b& dst, int binSize = 3, int height = 0);

#endif