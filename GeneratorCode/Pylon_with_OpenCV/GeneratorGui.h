#pragma once
#ifndef GENERATORGUI_C
#define GENERATORGUI_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "CamParams.h"
#include "GeneratorSysParams.h"
#include "GetDropletSize.h"
#include "BaslerCamera.h"


#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WCellDetection 7


// Namespace for using cout.
using namespace std;


/* This class adds simple controls using the openCV gui interface. It makes use
   of trackbars to establish callbacks that act on the system parameters as 
   well as a click and drag interface for changing the cameras ROI.*/
class GeneratorGui
{
public:
	GeneratorGui();
	~GeneratorGui();
	void GeneratorGui::generatorGuiInit(GetDropletSize *getDropletSize, GeneratorSysParams *GeneratorSysParams_in, BaslerCamera *camera_in);
	void onMouseClickInternal(int ev, int x, int y);

private:
	//CellDetector	*cellDetector;
	GeneratorSysParams *generatorSysParams;
	GetDropletSize *getDropletSize;
	BaslerCamera *camera;
	void updateParams();
	//static void GeneratorGui::onTrackbar(int, void*);
	static void GeneratorGui::onTrackbar(int newValue, void* object)
	{
		if (newValue < 1)
			newValue = 1;
		GeneratorGui* generatorGui = (GeneratorGui*)(object);
		generatorGui->updateParams();
		//
	}
	static void GeneratorGui::onGeneratorMouseClick(int ev, int x, int y, int, void* obj)
	{
		GeneratorGui* sg = static_cast<GeneratorGui*>(obj);
		if (sg)
			sg->onMouseClickInternal(ev, x, y);

	}

	//friend void onGeneratorMouseClick(int ev, int x, int y, void* obj);


	void createTrackbars();
	void createMouseCallbacks();
	bool mouseActive; // Whether the button is down
	int lastX; // Last x position when button is down
	int lastY; // Last y position when button is down
	int gain;
};



#endif