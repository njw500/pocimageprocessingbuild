#include "stdafx.h"
#include "Sorter.h"


Sorter::Sorter()
{
}


Sorter::~Sorter()
{
}


void CreateDropRoi(cv::Mat frame, cv::Mat &DropROI, int * DropLocation, const int Padding)
{
	// Use the location of a drop within a frame to create a region of interest.'''
	// Define the region of interest of the new frame, drop_roi
	// Extend region by 30 pixels along x.

	int drop_roi_x1 = DropLocation[0] - Padding;

	int drop_roi_x2 = DropLocation[1] + Padding;

	// Limit the X coords to the range of the frame.
	if (drop_roi_x1 < 0) drop_roi_x1 = 0;

	if (drop_roi_x2 > frame.cols - 1) drop_roi_x2 = frame.cols - 1;

	Rect R;
	//Create the drop ROI.
	if (drop_roi_x2 > drop_roi_x1)
	{
		R = Rect(drop_roi_x1, 0, drop_roi_x2 - drop_roi_x1, frame.rows);
	}
	else
	{
#ifdef FERR
		cout << "Backwards indices" << endl;
#endif
		R = Rect(drop_roi_x1, 0, drop_roi_x1 - drop_roi_x2, frame.rows);
	}

#ifdef FDBG
	imwrite("Frame.jpg", frame);
	imshow("Frame", frame);

	for (int i = 0; i < 10; ++i) waitKey(1);
#endif

	DropROI = frame(R);

#ifdef FDBG
	imwrite("Drop.jpg", DropROI);
	imshow("1D", DropROI);
	waitKey(1);

	for (int i = 0; i < 10; ++i) waitKey(1);
#endif
}


float CreateDropMask(cv::Mat DropROI, cv::Mat &DM, int *DropCentre, double thresh)
{
	//Detect the radius of the drop, and set values set to zero outside of a circular mask.

	// Create a mask of the droplet.
	//	Create a binary theshold to find the drop edge.
	//	http://docs.opencv.org/trunk/d7/d4d/tutorial_py_thresholding.html

	cv::Mat DropBinary;

	double radius_inset = 5.0;
	double rThresh;
	float Radius = -1;

	rThresh = threshold(DropROI, DropBinary, thresh, 255, THRESH_BINARY);

	// Find contours.
	// mode = cv2.RETR_EXTERNAL: retrieves only the extreme outer contours
	// method = cv2.CHAIN_APPROX_SIMPLE: compresses contours to save memory.

	cv::Mat  Hierachy;
	vector<vector<Point> >  Contours;
	findContours(DropBinary, Contours, Hierachy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Point ?

	if (Contours.size() > 0)
	{
		// Actuall have a contour

		//Only keep the largest contour.

		// Use list comprehension(a lambda expression) to find the length
		// of each contour.
		// contour_length = [len(c) for c in contours]
		int len;
		int MxLen = -1;
		int contour_index = -1;
		for (int i = 0; i < Contours.size(); ++i)
		{
			len = Contours[i].size();
			if (len > MxLen)
			{
				MxLen = len;
				contour_index = i;
			}
		}
		int contour_length = MxLen;


		// Only keep the contour at contour_index.
		vector<Point> contour = Contours[contour_index];

		cv::Mat Hull;
		convexHull(contour, Hull);

		//use the largest contour to mask the image
		// Find the bounding circle from the contour.
		Point2f Centre;

		minEnclosingCircle(contour, Centre, Radius);

		if (Radius < radius_inset + 0.01)
		{
#ifdef FERR
			cout << "Negative radius" << endl;
#endif
			return(-1.0);
		}

		Radius = Radius - radius_inset;

		// Create empty circle
		cv::Mat DropMask(DropROI.rows, DropROI.cols, CV_8UC3);
		Point iCentre(Centre);

		DropMask = Mat::zeros(DropROI.rows, DropROI.cols, CV_8U);
		circle(DropMask, iCentre, (int)Radius, Scalar(255), FILLED);


		DM = DropMask;
		DropCentre[0] = Centre.x;
		DropCentre[1] = Centre.y;
	}
	return(Radius);
}

bool DetectCells(cv::Mat DropROI, cv::Mat DropMask, int *CellLocation, double Threshold, int dbg)
{
	//threshold = 800, debug_stats = False) :
	// use a fast approximation of the derivative of the frame to detect flaws within the drop. '''

	// Use an integral filter to detect gradients within drop.
	// This is different to normal convolution.
	// Define a filter to detect flaws in the horizontal direction.

	/*const int filter_h[2][5] = { { 1, 1, 3, 4, -1 },
	{ 4, 1, 6, 4, +1 } };*/

	/*
	filter_h = np.array([[1, 1, 3, 4, -1],
	[4, 1, 6, 4, +1]])
	*/

	void IntegralFilter2(cv::Mat im, cv::Mat &Filtered);// New fast 
	void IntegralFilter(cv::Mat im, cv::Mat &Filtered); // old slow

#ifdef FDBG
	imwrite("FilteredInput.jpg", DropROI);
#endif
	cv::Mat FilteredFrame;
	IntegralFilter2(DropROI, FilteredFrame);

#ifdef FDBG
	imwrite("IntegralFiltereOutput.jpg", FilteredFrame);
	imwrite("DropMask.jpg", DropMask);
#endif


	convertScaleAbs(FilteredFrame, FilteredFrame);



	int type2 = DropMask.type();

	if (type2 != CV_8U)
		DropMask.convertTo(DropMask, CV_8U);


	const int border_x = 3, border_y = 5;

	// Make array the same size as the original by addind a hard coded sized border
	copyMakeBorder(FilteredFrame, FilteredFrame, 0, border_y, 0, border_x, BORDER_CONSTANT, 0);



	// Apply the drop mask to the frame.
	// Logical AND the frame with itself and mask with drop_mask.

	cv::Mat OutputFrame;
	FilteredFrame.copyTo(OutputFrame, DropMask);

#ifdef FDBG
	imwrite("FilteredData.jpg", OutputFrame);
#endif
	//bitwise_and(FilteredFrame, FilteredFrame, DropMask);


	//Now filtered_frame is zero outside the drop radius.

	cv::Mat sum_v, sum_h;


	reduce(OutputFrame, sum_v, 0, REDUCE_SUM, CV_32F);
	reduce(OutputFrame, sum_h, 1, REDUCE_SUM, CV_32F);

	// Find maximum and maxima location
	double minVal, maxValV, maxValH;
	cv::Point minLoc, maxLocV, maxLocH;

	bool cell_detected = false;
	CellLocation[0] = 0;
	CellLocation[1] = 0;

	minMaxLoc(sum_v, &minVal, &maxValV, &minLoc, &maxLocV);
	if (maxValV > Threshold)
	{
		minMaxLoc(sum_h, &minVal, &maxValH, &minLoc, &maxLocH);
		if (maxValH > Threshold)
		{
			cell_detected = true;
			CellLocation[0] = maxLocV.x;
			CellLocation[1] = maxLocH.y;
		}
	}

	return(cell_detected);
}

