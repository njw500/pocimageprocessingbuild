
#include "stdafx.h"

#define _CRT_SECURE_NO_WARNINGS

#include "GetDropletSize.h"

#include "get_droplet_contour.h"

#include "HoughDroplets.h"
#include "DropDetection.h"


#include "..\..\Utilities\Utilities\DisplayIm.h"



//#include "Histogram.h"

// Namespace for using cout
using namespace std;

using namespace cv;

GetDropletSize::GetDropletSize()
{
	Width.reserve(InitialAllocSize * 15);
	Width2.reserve(InitialAllocSize * 15);
	Area.reserve(InitialAllocSize * 15);

	OutputMean[0] =0.0;
	OutputMean[1] =0.0;
	OutputMean[2] =0.0;

	OutputStd[0]=0.0;
	OutputStd[1] = 0.0;
	OutputStd[2] = 0.0;

}


void GetDropletSize::WriteVectors()
{
	WriteVector(Area, Area.size(), "Area1.csv");
	WriteVector(Width, Width.size(), "Width1.csv");
	//WriteVector(Area2, Area2.size(), "Area2.csv");
	WriteVector(Width2, Width2.size(), "Widths2.csv");
}

void GetDropletSize::ResetVectors()
{
	Area.erase(Area.begin(), Area.end());
	//Width.erase(Width.begin(), Width.end());
	Width2.erase(Width2.begin(), Width2.end());
}

void GetDropletSize::InitializeAverage(const cv::Mat cframe, cv::Mat &avg)
{
	int Fr = cframe.rows;
	int Fc = cframe.cols;
	cv::Mat frame;

	int Ty = cframe.type();// For the video this is 16
	int Ch = cframe.channels(); // For the video this is 3

	avg = Mat(Fr, Fc, CV_32F);
	
	//avg.setTo(0.0);
	if (Ch > 1)
		cvtColor(cframe, frame, COLOR_BGR2GRAY);
	else
		frame = cframe;
	
	int Ty2 = frame.type();// This is 0  always
	int Ch2 = frame.channels();// This is 1 always

	frame.convertTo(avg, CV_32F);

	ResetVectors();

	// Empty the rolling vectors
	//AreaCount = 0;
	//AreaCount2 = 0;
	//AreaCount3 = 0;
}

// Do nothing
GetDropletSize::~GetDropletSize()
{
}

// Main call to measurment functions
bool GetDropletSize::Execute(cv::Mat &avg, const cv::Mat cframe, const GeneratorSysParams Params)
{
	
	// Add the current into the average and caculate the background
	Mat ffcrop;
	Mat frame;

	Hist2.DoPlot = false;
	Hist3.DoPlot = true;// WAS FALSE

	OutputMean[0] = 0.0f;
	OutputMean[1] = 0.0f; 
	OutputMean[2] = 0.0f;
	OutputStd[0] = 0.0f;
	OutputStd[1] = 0.0f;
	OutputStd[2] = 0.0f;

	// Convert BGR frame to GRAY frame
	int Ch = cframe.channels(); // For the video this is 3
	if (Ch > 1)
		cvtColor(cframe, frame, COLOR_BGR2GRAY);
	else
		frame = cframe;

	//cvtColor(cframe, frame, COLOR_BGR2GRAY);

	frame.convertTo(ffcrop, CV_32F);// Promote current crop to float

	accumulateWeighted(ffcrop, avg, Params.AverageDecay); // Add float crop into the average

	cv::Mat background;
	convertScaleAbs(avg, background);// Take ABS value and scale to 8 bit

#ifdef DEBUGOUTPUT
	if (Params.DisplayMode[WBack])
		displayIm("Background", background);
#endif
	// Call Each of the three droplet size code blocks

	// Call the Area finder 
	// this is the 1st version of the finder {Duncan}

	Mat kernel;
	kernel = kernel.ones(Size(5, 5), CV_8U);

	Mat OutputImage;
	vector<vector<Point> >Final;
	float mn, mx;

	Hist1.MakeEmptyWin();// Still need a window to write results into


	// Call the 2nd method (ED)
	// Next add and call Ed's code into here  21/11/2016
	// Test droplet finding code using morphological operations

	int NumDroplets2;
	Mat frameDisp;

	FindDropWidths(frame, Width2, &NumDroplets2, &frameDisp, Params);
	
	// Get histogram and stats
	float mn2, mx2;

	if (NumDroplets2 >= Params.MinDropsPerFrame)
	{
		OutputMean[1] = Hist1.GetHistogram(Width2, Width2.size(), mn2, mx2, this->OutputStd[1], "UDP Histogram");

		//if (Params.DisplayMode[WDisplayEd])
		//{
			// Add ext
			char Cstr[200];
			String str;
			sprintf(Cstr, "Width: %0.2f  std: %0.2f", OutputMean[1], OutputStd[1]);
			str = Cstr;
			frameDisp.convertTo(frameDisp, CV_32F);
			putText(frameDisp, str, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0));
			line(frameDisp, Point(1, Params.checkDropRowTop.val), Point(frameDisp.cols, Params.checkDropRowTop.val), Scalar(255, 255, 0), 1, 8);
			line(frameDisp, Point(1, Params.checkDropRowBottom.val), Point(frameDisp.cols, Params.checkDropRowBottom.val), Scalar(255, 255, 0), 1, 8);
			line(frameDisp, Point(1, Params.roi_y1.val), Point(frameDisp.cols, Params.roi_y1.val), Scalar(0, 255, 0), 2, 8);
			line(frameDisp, Point(1, Params.roi_y2.val), Point(frameDisp.cols, Params.roi_y2.val), Scalar(0, 255, 0), 2, 8);
#ifdef SCREENOUTPUT
			imshow("Main Output", frameDisp);
			waitKey(1);
			//resizeWindow("Main Output", 1400, 400);
#endif
		//}
	}
	else
	{
		// Display debug parameters
		line(frameDisp, Point(1, Params.checkDropRowTop.val), Point(frameDisp.cols, Params.checkDropRowTop.val), Scalar(255, 255, 0), 1, 8);
		line(frameDisp, Point(1, Params.checkDropRowBottom.val), Point(frameDisp.cols, Params.checkDropRowBottom.val), Scalar(255, 255, 0), 1, 8);
		line(frameDisp, Point(1, Params.roi_y1.val), Point(frameDisp.cols, Params.roi_y1.val), Scalar(0, 255, 0), 2, 8);
		line(frameDisp, Point(1, Params.roi_y2.val), Point(frameDisp.cols, Params.roi_y2.val), Scalar(0, 255, 0), 2, 8);
#ifdef SCREENOUTPUT
		imshow("Main Output", frameDisp);
		waitKey(1);
		//resizeWindow("Main Output", 1400, 400);
#endif
	}



	// Call the 3rd method (NJW)
	// Hough and box based

	cv::Mat fgmask;
	// Calculate this again
	subtract(background, frame, fgmask);

	Mat tmp;
	ScaleImage(fgmask, tmp);

	if (Params.DisplayMode[WDisplaySubN])
		displayIm("SubtractNorm", tmp);

	vector<float> CurWidths;
	CurWidths.reserve(20);

	//int NumValid;
	int NumDroplets3=HoughDroplets(tmp, CurWidths, Params);

	if (NumDroplets3 >= Params.MinDropsPerFrame)
	{
		if (Width.size()< RingBufferLength)
			Width.insert(Width.end(), CurWidths.begin(), CurWidths.end());
		else
		{
			// Pull elements off the front and put them on the back
			for (int k = 0; k < NumDroplets3; ++k)
				Width.erase(Width.begin());
			Width.insert(Width.end(), CurWidths.begin(), CurWidths.end());
		}

		OutputMean[2] = Hist3.GetHistogram(Width, Width.size(), mn, mx, this->OutputStd[2], "Width");

		if (Params.DisplayMode[WOutput])
		{
			char Cstr[200];
			String str;

			sprintf(Cstr, "Area: %0.0f  std:  %0.2f  2nd W: %0.2f  std: %0.2f", OutputMean[0], OutputStd[0], OutputMean[2], OutputStd[2]);
			str = Cstr;

			putText(cframe, str, Point(50, 50), FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0));

			displayIm("Output", cframe);
		}
	}

	if ((NumDroplets2 < Params.MinDropsPerFrame) &&
		(NumDroplets3 < Params.MinDropsPerFrame))
		cout << " Insufficient droplets seen" << endl;
	else
	{
		char Cstr1[200], Cstr2[200];// Cstr0[200];
		String str1,str2,str0;


		if (NumDroplets2>0)
			sprintf(Cstr1, "Width: %0.2f std: %0.2f", OutputMean[1], OutputStd[1]);
		else
			sprintf(Cstr1, "---");
		str1 = Cstr1;

		if (NumDroplets3>0)
			sprintf(Cstr2, "Width: %0.2f std: %0.2f", OutputMean[2], OutputStd[2]);
		else
			sprintf(Cstr2, "---");
		str2 = Cstr2;

		//Hist1.Annotate("",str1, str2);
		Hist1.Show();
		Hist3.Show();
	


	}
	


	return(true);
}


bool GetDropletSize::PlotResults(GeneratorSysParams &Params)
{

	return(true);
}