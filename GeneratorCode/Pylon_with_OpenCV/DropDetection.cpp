#include "stdafx.h"
#include "DropDetection.h"
#include <stdio.h>
#include <iostream>
#include <vector>
#include "DebugFlags.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\..\Utilities\Utilities\DisplayIm.h"
#include "..\..\Utilities\Utilities\WriteVector.h"


using namespace std;
using namespace cv;

bool DetectDrop_Ed(cv::Mat Frame,
	int roi_y1,
	int roi_y2,
	int regionPadding,
	int thresholdLevel,
	int checkDropRowTop,
	int checkDropRowBottom,
	double min_pk_dist,
	int *DropLocationX,
	int *numDrops)
{
	/*   Detect if a drop is present on the input frame by measuring the peaks
	along the X axis of the frame.
	Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2).
	Note: this was modified from the original function to follow the process of:
	1. Take inverse threshold to find 'halo' around drops
	2. Take a small ROI in the centre of the images
	3. Sum the columns (reduce to a 1D array)
	4. Find where the peaks are
	5. Select the leftmost peak that is separated by the diameter
	6. Check for the top and bottom of the drop (to make sure that
	the peaks are the edges of the same drop and not two different drops)


	Possible improvements include a more sensible management of the intermediate images (i.e., passing
	them in by argument to avoid lots of memory allocs/deallocs that must be going on under the hood) and
	also using binary images (or at least int) after the image has been thresholded.

	Input
	-----
	Frame: The input image in which a drop should be found
	roi_y1: The start column that should be selected
	roi_y2: The end column that should be selected
	min_pk_dist: The distance which corresponds to the minimum diameter of a drop.
	topbottomborder_px: The number of pixels to ignore at the top and bottom of the image
	counter: A flag for debug logging

	Output
	------
	DropLocation: Vector specifying the x positions that bound the drop diameter
	*/

	// Declaration of parameters
	stringstream ss;

	int leftIdx = -1;
	double peakArrayBuffer[200];
	bool DropDetected = false;
	cv::Mat peak_array2; // Copy a second array to subtract. Could be more memory efficient

	// Determine ROI
	int NumPeaks = 0;
	int width = Frame.cols;
	int height = Frame.rows;
	cv::Mat invertedImage = Frame.clone();
	cv::Mat invertedImage2 = Frame.clone();
	width = width - 2;

	// Find the inverse binary threshold to capitalise on the well defined black halo around the circles 
	threshold(Frame, invertedImage, thresholdLevel, 255, THRESH_BINARY_INV);
	invertedImage2 = invertedImage;

#ifdef DEBUGOUTPUT 
	ss.str("");
	ss << "Frame" << 1 << ".jpg";
	imwrite((string)ss.str(), Frame);
	ss.str("");
	ss << "invertedImage" << 1 << ".jpg";
	imwrite((string)ss.str(), invertedImage);

#endif

	imshow("Inverted", invertedImage);
	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = invertedImage(ROI);

#ifdef DEBUGOUTPUT 
	// Some temporary logging 
	ss.str("");
	ss << "PeakArray" << 1 << ".jpg";
	imwrite((string)ss.str(), peak_array);
#endif
	// Sum along columns to find a line profile
	peak_array.convertTo(invertedImage, CV_32F); // Note, probably not necessary if using REDUCE_MAX
	reduce(invertedImage, peak_array, 0, REDUCE_SUM, -1);
	threshold(peak_array, peak_array, (roi_y2 - roi_y1)*0.5 * 255, 255, THRESH_BINARY);

	peak_array2 = peak_array.clone();
	// Some temporary logging 
#ifdef DEBUGOUTPUT 
	WriteVector(peak_array2, "PreBlur.csv");  // Write the pre blur vector
	imwrite("peak_array2.jpg", peak_array2);  // Write the pre blur vector
#endif
	// Quickly find connected points
	// Pad to the right and left of the vectors
	copyMakeBorder(peak_array, peak_array, 0, 0, 1, 0, 0, 0);
	copyMakeBorder(peak_array2, peak_array2, 0, 0, 0, 1, 0, 0);
	// Now subtract the offset vectors
	peak_array = peak_array - peak_array2;

#ifdef DEBUGOUTPUT 
	WriteVector(peak_array, "Subtract.csv");
	imwrite("Subtract1.jpg", peak_array);  // Write the pre blur vector
#endif
	// Set default, NULL locations for the Droplet
	DropLocationX[0] = 0;


	// Loop over the whole array and find anywhere that there
	// are two peaks separated by a given threshold. Note, 
	// the first drop from the left is selected and the loop is exited as soon as it is found. 
	// When a peak across x direction is found, 2 peaks must be found in the y direction
	// To confirm that it is a drop. 
	for (int i = 0; i < peak_array.cols; i++)
	{
		// Check if its the first peak or if its there is sufficient
		// spacing between the peaks. 
		if (peak_array.at<float>(i) > 0)
			if (leftIdx < 0)
			{
				leftIdx = i;

			}
			else if (i - leftIdx > min_pk_dist)
			{

				// Check to see if this is valid by looking for 2 peaks along the columns at the centre
				int temp = (int)((i - leftIdx) / 2 + leftIdx);
				// Check to see if the side parts of the drops are visible, or check to see if we are somehow
				// between drops. Do this by checking the bottom and top of the images over a 5 row region, and
				// summing accordingly. 
				cv::Rect ROIcol(leftIdx - regionPadding, checkDropRowTop, i - leftIdx + 2 * regionPadding, 5);
				cv::Rect ROIcol2(leftIdx - regionPadding, checkDropRowBottom, i - leftIdx + 2 * regionPadding, 5);
#ifdef DEBUGOUTPUT
				cv::Rect ROIVis(leftIdx - regionPadding, 1, i - leftIdx + 2 * regionPadding, height - 1);
#endif
				if (ROIcol.x < 0)
				{
					ROIcol.x = 0;
					ROIcol2.x = 0;
#ifdef DEBUGOUTPUT
					ROIVis.x = 0;
#endif

				}
				if (ROIcol.width + ROIcol.x  >= invertedImage2.cols)
				{
					ROIcol.width = invertedImage2.cols  - ROIcol.x - 1;
					ROIcol2.width = invertedImage2.cols - ROIcol2.x - 1;
#ifdef DEBUGOUTPUT

					ROIVis.width = invertedImage2.cols  - ROIVis.x - 1;
#endif
				}

#ifdef DEBUGOUTPUT
				cv::Mat roiVis = invertedImage2(ROIVis);
#endif
				//cv::Rect ROIcol(temp, topbottomborder_px, 1, height - topbottomborder_px - topbottomborder_px);
				cv::Mat roi2 = invertedImage2(ROIcol);
				roi2.convertTo(roi2, CV_32F); // Note, probably not necessary
				
				reduce(roi2, roi2, 0, REDUCE_SUM, -1);
				threshold(roi2, roi2, 255 * 4, 255, THRESH_BINARY);				
				cv::Mat roi1 = invertedImage2(ROIcol2);
				roi1.convertTo(roi1, CV_32F); // Note, probably not necessary
				reduce(roi1, roi1, 0, REDUCE_SUM, -1);
				threshold(roi1, roi1, 255 * 4, 255, THRESH_BINARY);
				cv::Mat roi3;
				copyMakeBorder(roi2, roi3, 0, 0, 0, 1, BORDER_CONSTANT, Scalar(1));
				copyMakeBorder(roi2, roi2, 0, 0, 1, 0, BORDER_CONSTANT, Scalar(1));

#ifdef DEBUGOUTPUT
				ss.str("");
				ss << "roi1_" << (double)i << ".jpg";
				imwrite((string)ss.str(), roi2);

				ss.str("");
				ss << "roiVis_" << (double)i << ".jpg";
				imwrite((string)ss.str(), roiVis);
#endif
				roi2 = roi3 - roi2;
				copyMakeBorder(roi1, roi3, 0, 0, 0, 1, BORDER_CONSTANT, Scalar(1));
				copyMakeBorder(roi1, roi1, 0, 0, 1, 0, BORDER_CONSTANT, Scalar(1));
				roi1 = roi3 - roi1;
				//#ifdef DEBUGOUTPUT 
				// Some temporary logging 
#ifdef DEBUGOUTPUT
				ss.str("");
				ss << "roiSub" << (double)i << ".jpg";
				imwrite((string)ss.str(), roi2);
#endif
				//#endif
				

				int vertCount = 0;
				int vertCount2 = 0;

				for (int j = 0; j < roi2.cols; j++)
				{
					if (roi2.at<float>(j) > 0)
					{
						vertCount = vertCount + 1;
					}
					if (roi1.at<float>(j) > 0)
					{
						vertCount2 = vertCount2 + 1;
					}
				}
				// If 2 peaks have been found in the top and bottom region,
				// save accordingly. 
				if ((vertCount>1) && (vertCount2 > 1))
				{
					peakArrayBuffer[NumPeaks] = leftIdx;
					DropDetected = true;

					DropLocationX[NumPeaks] = leftIdx - 10;
					DropLocationX[NumPeaks + 1] = i + 10;
					NumPeaks = NumPeaks + 2;

				}
				else
				{
					// Peak missed, don't do anything!
				}
				leftIdx = i;
			}
			else
			{
				//leftIdx = i;
			}
	}
	*numDrops = NumPeaks / 2;
	return(DropDetected);
}


bool DetectDrop(cv::Mat Frame, int roi_y1, int roi_y2, double min_pk_height, double min_pk_dist, int *DropLocation, int counter)
{
	/* PY:  Detect if a drop is present on the input frame by measuring the peaks
	along the X axis of the frame.
	Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2). :PY */

	//Find droplets by looking for peaks across line in image.


	// Apply ROI
	int width = Frame.cols;
	int height = Frame.rows;

	width = width - 10;

	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = Frame(ROI);
	stringstream ss;

	//ss << "PeakArray" << counter << ".jpg";
	//imwrite((string)ss.str(), peak_array);

	// integrate this to a single line profile
	reduce(peak_array, peak_array, 0, REDUCE_AVG, -1);


	int GaussianBlurSize = 11;
	peak_array.convertTo(peak_array, CV_32F);

	WriteVector(peak_array, "PreBlur.csv");
	// Gaussian Blur, which needs the data to be float32
	GaussianBlur(peak_array, peak_array, Size(GaussianBlurSize, GaussianBlurSize), 0, 0);
	WriteVector(peak_array, "PostBlur.csv");

	cv::Mat Peak_loc;
	cv::Mat Peak_height;

	// Set default, NULL locations for the Droplet
	DropLocation[0] = 0;
	DropLocation[1] = 0;

	int NumPeaks = opencv_peaks(peak_array, Peak_loc, Peak_height, min_pk_height, min_pk_dist);

	int i = 0;
	bool DropDetected = false;

	//WriteVector(Peak_height, "PeakHeight.csv");

	if (NumPeaks > 0)
	{
		// use the iffy code to check peak intensities (this works on the the demo video file)
		while ((!DropDetected) && (i < NumPeaks - 1))
		{
			if (Peak_height.at<float>(i) >Peak_height.at<float>(i + 1))
			{
				DropDetected = true;
				DropLocation[0] = Peak_loc.at<unsigned short int>(i);
				DropLocation[1] = Peak_loc.at<unsigned short int>(i + 1);
			}
			++i;
		}
	}

	return(DropDetected);
}


void CreateDropRoi(cv::Mat frame, cv::Mat &DropROI, int * DropLocation, const int Padding, int & roiX, int & roiY)
{
	// Use the location of a drop within a frame to create a region of interest.'''
	// Define the region of interest of the new frame, drop_roi
	// Extend region by 30 pixels along x.
	int drop_roi_x1 = DropLocation[0] - Padding;
	int drop_roi_x2 = DropLocation[1] + Padding;
	Rect R;

	// Limit the X coords to the range of the frame.
	if (drop_roi_x1 < 0) drop_roi_x1 = 0;

	if (drop_roi_x2 > frame.cols - 1) drop_roi_x2 = frame.cols - 1;

	// Some quick checking in case x2 and x1 are flipped...
	if (drop_roi_x2 < drop_roi_x1)
	{
		//Create the drop ROI.

		R.x = drop_roi_x2;
		R.y = 0;
		R.width = drop_roi_x1 - drop_roi_x2;
		R.height = frame.rows;

	}
	else
	{
		//Create the drop ROI.
		R.x = drop_roi_x1;
		R.y = 0;
		R.width = drop_roi_x2 - drop_roi_x1;
		R.height = frame.rows;
	}


#ifdef DEBUGOUTPUT 
	imwrite("Frame.jpg", frame);
#endif
#ifdef DEBUGSCREENOUTPUT
	namedWindow("Frame", WINDOW_AUTOSIZE);
	imshow("Frame", frame);
	//for (int i = 0; i < 10;++i) //waitKey(1);
#endif
	//DropROI = frame(R);
	//DropROI = frame(R);
	DropROI.release();
	DropROI = frame(R).clone();
#ifdef DEBUGOUTPUT 
	imwrite("Drop.jpg", DropROI);
#endif
#ifdef DEBUGSCREENOUTPUT
	namedWindow("DROP", WINDOW_AUTOSIZE);
	imshow("DROP", DropROI);
#endif
	//waitKey(1);
	//DropROI.release();
	//for (int i = 0; i < 10; ++i) //waitKey(1);
	roiX = R.x;
	roiY = R.y;
}

float CreateDropMask(cv::Mat DropROI, cv::Mat &DM, int *DropCentre, double thresh)
{
	//Detect the radius of the drop, and set values set to zero outside of a circular mask.

	// Create a mask of the droplet.
	//	Create a binary theshold to find the drop edge.
	//	http://docs.opencv.org/trunk/d7/d4d/tutorial_py_thresholding.html

	cv::Mat DropBinary;
	double radius_inset = 15.0;

	double rThresh;
	rThresh = threshold(DropROI, DropBinary, thresh, 255, THRESH_BINARY);

	// Find contours.
	// mode = cv2.RETR_EXTERNAL: retrieves only the extreme outer contours
	// method = cv2.CHAIN_APPROX_SIMPLE: compresses contours to save memory.

	cv::Mat  Hierachy;
	vector<vector<Point> >  Contours;
	findContours(DropBinary, Contours, Hierachy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Point ?

	//Only keep the largest contour.
	// Use list comprehension(a lambda expression) to find the length
	// of each contour.
	// contour_length = [len(c) for c in contours]
	int len;
	int MxLen = -1;
	int contour_index = -1;
	for (int i = 0; i < Contours.size(); ++i)
	{
		len = Contours[i].size();
		if (len > MxLen)
		{
			MxLen = len;
			contour_index = i;
		}
	}


	int contour_length = MxLen;

	// Only keep the contour at contour_index.
	vector<Point> contour = Contours[contour_index];

	cv::Mat Hull;
	convexHull(contour, Hull);

	//use the largest contour to mask the image
	// Find the bounding circle from the contour.
	Point2f Centre;
	float Radius;
	minEnclosingCircle(contour, Centre, Radius);

	Radius = Radius - radius_inset;

	// Create empty circle
	cv::Mat DropMask(DropROI.rows, DropROI.cols, CV_8UC3);
	Point iCentre(Centre);
	//iCentre.x = Centre.y;
	//iCentre.y = Centre.x;

	DropMask = Mat::zeros(DropROI.rows, DropROI.cols, CV_8U);
	circle(DropMask, iCentre, (int)Radius, Scalar(255), FILLED);



	DM = DropMask;
	DropCentre[0] = Centre.x;
	DropCentre[1] = Centre.y;

	return(Radius);
}





bool FindDropWidths(cv::Mat frame, vector<float> &widths2, int *numDrops, cv::Mat *debugOutput, const GeneratorSysParams Params)
{
	/*   Detect if a drop is present using the process listed below.

	In summary: this uses some thresholding morphological operations to locate the centre of the drops,
	followed be thresholding across the a single 1D column vector at the centre to accurately determine
	the 'width' of the drops

	1. Quick coarse find of the drop boundaries
	2. Inverse threshold image
	3. Erode image
	4. Large close on the image
	5. Operating just on ROI defined by the coarse drop boundaries, invert the image
	6. Find the largest contour (which corresponds to a single drop) and its centre
	7. Take a radial slice (a single row) of the original thresholded image at the drop
	ROI. Set the centre according to 6.
	8. Find the left and the right which defines the diameter (width) by looking
	for the left most thresholded and right most thresholded pixel
	*/

	/*
	Input
	-----
	frame:				the input frame with all the drops in

	Parameters
	----------
	roi_y1:				Start row for the coarse drop find (note that
	the drop should be larger than this reason).
	roi_y2:				End row for the coarse drop find.
	top:				The top/bottom border for the coarse detect.
	thresholdLevel1:	thresholdLevel
	thresholdLevel2:
	structureEl1:		The erosion structral element size
	structureEl2:		The close structural element size
	padding_px:			padding from the coarse droplet results

	Output
	------
	widths2:  A vector containing the widths of the drops
	numDrops: A vector containing the number of detected drops in the image
	*/

	//Just copy this lot for now  (tidying started)

	int roi_y1 = Params.roi_y1.val;
	int roi_y2 = Params.roi_y2.val;

	//int thresholdLevel1 = Params.thresholdLevel1;
	double min_pk_distance = Params.min_pk_distance.val;
	int thresholdLevel2 = Params.thresholdLevel2.val;// Image intensity based value (adjusatable)  (could be the same as 1)

	//int structureEl1 = Params.structureEl1;
	int structureEl2 = Params.structureEl2.val;
	int padding_px = Params.padding_px.val;

	//* Declaration and Initialisation 
	int DropLocationX[200];
	int counter = 1;

	int NumDropsFound = 0;

	bool res;
	vector<vector<Point> >newContours;
	cv::Rect Roi;
	cv::Mat dropArray;
	Mat element;




	Mat invertedImage = frame.clone();

	//	1. Quick coarse find of the drop boundaries 
	res = DetectDrop_Ed(frame, (int)roi_y1, (int)roi_y2, (int)Params.regionPadding.val,
		(int)Params.thresholdLevel1.val, /// Image intensity based value (adjusatable)
		(double)min_pk_distance,
		Params.checkDropRowBottom.val,
		Params.checkDropRowTop.val,
		DropLocationX,
		&NumDropsFound);

	*debugOutput = frame.clone(); // For visualisation only. 

	// test the number of drops calculated here against Params.MinDropsPerFrame 0
	if (NumDropsFound < Params.MinDropsPerFrame)
	{
		*numDrops = 0;
		return(0);
	}

	//	2. Inverse threshold image 

	threshold(frame, invertedImage, thresholdLevel2, 255, THRESH_BINARY_INV);
	Mat thresholdedImage = invertedImage.clone();

	//	3. Erode image
	element = getStructuringElement(MORPH_ELLIPSE, cv::Size(Params.structureEl1.val, Params.structureEl1.val), cv::Point(-1, -1));
	morphologyEx(invertedImage, invertedImage, MORPH_ERODE, element, cv::Point(-1, -1), 1);
	
#ifdef DEBUGOUTPUT
	imwrite("Erode.jpg", invertedImage);
#endif
	//	4. Large close on the image
	element = getStructuringElement(MORPH_ELLIPSE, cv::Size(structureEl2, structureEl2), cv::Point(-1, -1));
	morphologyEx(invertedImage, invertedImage, MORPH_CLOSE, element, cv::Point(-1, -1), 1);

#ifdef DEBUGOUTPUT
	imwrite("OpenClose.jpg", invertedImage);
#endif

	// Loop over each detected drop
	//widths2.reserve(NumDropsFound);
	vector<float> CurWidths;

	CurWidths.reserve(NumDropsFound);

	roi_y1 = 1;
	roi_y2 = invertedImage.rows - 1;

	Rect lastBounding_rect(-1,-1,1,1);

	for (int dropIdx = 0; dropIdx < NumDropsFound * 2; dropIdx = dropIdx + 2)
	{
		// Check to make sure that the padding won't push the roi out of the image boundaries
		if (((DropLocationX[dropIdx] - padding_px) + (DropLocationX[dropIdx + 1] + padding_px - DropLocationX[dropIdx] + padding_px)
			< invertedImage.cols) &&
			((DropLocationX[dropIdx] - padding_px) > 0))
		{
			//	5. Operating just on ROI defined by the coarse drop boundaries, invert the image
			Roi = Rect(DropLocationX[dropIdx] - padding_px,
				roi_y1,
				DropLocationX[dropIdx + 1] + padding_px - DropLocationX[dropIdx] + padding_px,
				roi_y2 - roi_y1);

			bitwise_not(invertedImage(Roi), dropArray);
#ifdef DEBUGOUTPUT
			imwrite("DropArray.jpg", dropArray);
#endif
			vector<Vec4i> hierarchy; // Not used...
			// 6. Find the largest contour(which corresponds to a single drop) and its centre
			findContours(dropArray, newContours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

			if (newContours.size() > 0)
			{
				int largest_contour_index = 0;
				int largest_area = 0;
				Rect bounding_rect;
				Moments DropMoment;

				// Loop over each contour found to find the largest
				for (int i = 0; i < newContours.size(); i++)
				{
					//  Find the area of contour
					double a = contourArea(newContours[i], false);
					if (a > largest_area)
					{
						// REMOVE THIS DEBUG OUTPUT LINE NJW
						largest_area = a;
						//cout << i << " area  " << a << endl;
						// Store the index of largest contour
						largest_contour_index = i;
						// Find the bounding rectangle for biggest contour
						bounding_rect = boundingRect(newContours[i]);
					}
				}

				Mat newArray = dropArray.clone();
				//Mat newArray2;
				//newArray = Scalar(0, 0, 0);
				drawContours(newArray, newContours, largest_contour_index,
					Scalar(255, 255, 255), -1, 8, hierarchy, 0, Point());
#ifdef DEBUGOUTPUT
				imwrite("NewArray.jpg", newArray);
#endif
				// Find the centre of the contour 

				DropMoment = moments(newContours[largest_contour_index]);
				int cx = (DropMoment.m10 / DropMoment.m00);
				int cy = (DropMoment.m01 / DropMoment.m00);

				// 7. Take a radial slice(a single row) of the original thresholded image at the drop
				//	ROI.Set the centre according to step (6).
				Mat radialSlice = thresholdedImage(Roi);
				if (cy < radialSlice.rows && cy > 0)
				{
					radialSlice = radialSlice(Rect(0, cy, radialSlice.cols, 1));
					radialSlice.convertTo(radialSlice, CV_32F);
				}
				else
				{
					return false;
				}
#ifdef DEBUGOUTPUT
				imwrite("radialSlice.jpg", radialSlice);
#endif

				// 8. Find the left and the right which defines the diameter(width) by looking
				//	for the left most thresholded and right most thresholded pixel
				int leftIdx = -1;
				int rightIdx = 0;
				// Loop over each column in the 1D vector
				for (int j = 0; j < radialSlice.cols; j++)
				{
					// If this is the first 'white' pixel, set the left index
					if (radialSlice.at<float>(j) == 255 && leftIdx < 0)
					{
						leftIdx = j;
					}
					// If this is another white pixel, update the right index
					if (radialSlice.at<float>(j) == 255)
					{
						rightIdx = j;
					}
				}
				// Add an additional pixel to the right index
				rightIdx = rightIdx + 1;
				// Determine a new rectangular ROI
				bounding_rect = Rect(leftIdx, cy - (rightIdx - leftIdx) / 2, rightIdx - leftIdx, (rightIdx - leftIdx));
				// Add some circles for debugging
				bounding_rect = bounding_rect + Point(DropLocationX[dropIdx] - padding_px, roi_y1);

				// Check to see if this overlaps with the previous. 
				if ((lastBounding_rect & bounding_rect).width>0)
				{
					//cout << "Intersect Detected!" << endl;
					if (CurWidths.size() > 0)
					{
						CurWidths.pop_back();
					}
				}
				else
				{
					// Add the new width (i.e., diameter) to the list
					circle(dropArray, Point(cx, cy), 2, Scalar(255, 255, 255), FILLED);
					circle(*debugOutput, Point(cx + DropLocationX[dropIdx] - padding_px, cy + roi_y1), 2, Scalar(255, 255, 255), FILLED);
					rectangle(*debugOutput, bounding_rect, Scalar(255, 255, 255), 1, 8, 0);
					CurWidths.push_back((float)(rightIdx - leftIdx));
				}
				lastBounding_rect = bounding_rect;


			}
		}
	}

	//= NumDropsFound;
	*numDrops = CurWidths.size();

	// Append the CurWidths onto the Widths list
	widths2.insert(widths2.end(), CurWidths.begin(), CurWidths.end());
	
	while (widths2.size() > Params.widthHistory.val)
	{
		widths2.erase(widths2.begin());
	}

	return true;
}

bool DetectCells_Ed(cv::Mat DropROI,
	cv::Mat DropMask,
	int *CellLocation,
	int openSize,
	int erodeSize,
	int threshold,
	int dbg)
{

	/*   Detect if a cell is present within a drop using the following process:
	1. Use a greyscale morphological 'open' on the droplet ROI
	2. Subtract the open from the original droplet ROI
	3. Erode the mask image
	4. AND the results of (2) and (3)
	5. Find the maximum of the result of (4).

	Input
	-----
	DropROI: the input drop
	DropMask: Input drop mask

	Parameters
	----------
	openSize:  The size to open (see step 1)
	erodeSize: The size to erode (note, must be bigger than the size of the 'halo')
	threshold: A threshold after the morphological operations

	Output
	------
	CellLocation: Vector specifying the x,y position of any detected cell
	return: Whether or not a cell was detected
	*/

	bool detected;
	Mat DropROIDilate;
	Mat element;

	detected = false;

#ifdef DEBUGOUTPUT 
	imwrite("DropROI_Cells.jpg", DropROI);
#endif
#ifdef DEBUGSCREENOUTPUT
	namedWindow("DropROI_cells", WINDOW_AUTOSIZE);
	imshow("DropROI_cells", DropROI);
#endif

	Mat DropROIOpen;
	// 1. Open the image of the droplet ROI
	element = getStructuringElement(MORPH_ELLIPSE, cv::Size(openSize, openSize), cv::Point(-1, -1));
	morphologyEx(DropROI, DropROIOpen, MORPH_OPEN, element, cv::Point(-1, -1), 1);
#ifdef DEBUGSCREENOUTPUT
	namedWindow("DropROI_cellsOpen", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpen", DropROIOpen);
#endif
	// 2. Subtract the opened image from the original image
	DropROI = DropROI - DropROIOpen;
	// 3. Make the droplet mask smaller (to ignore the 'halo' at the edges) using erode
	//element = getStructuringElement(MORPH_ELLIPSE, cv::Size(erodeSize, erodeSize), cv::Point(-1, -1));
	//morphologyEx(DropMask, DropMask, MORPH_ERODE, element, cv::Point(-1, -1), 1);

#ifdef DEBUGSCREENOUTPUT
	namedWindow("DropROI_erode", WINDOW_AUTOSIZE);
	imshow("DropROI_erode", DropMask);
	namedWindow("DropROI_cellsOpenAnd", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd", DropROI);
#endif
	// 4. AND the result of (3) and (2). 
	DropROI = DropROI & DropMask;

#ifdef DEBUGSCREENOUTPUT
	namedWindow("DropROI_cellsOpenAnd2", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd2", DropROI);
#endif
	// 5. Find the location of any values > threshold
	double maxVal = 0;
	double minVal = 0;
	Point minLoc; Point maxLoc;
	minMaxLoc(DropROI, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	//cout << "Max Val:" << maxVal << endl;
	if (maxVal > threshold)
	{
		//DropROI = DropROI & DropMask;
		Scalar  Colour = Scalar(255, 255, 255);
		circle(DropROI, maxLoc, (int)5, Colour);
		detected = true;
	}

#ifdef DEBUGSCREENOUTPUT
	namedWindow("DropROI_cellsOpenAnd2", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd2", DropROI);
#endif
	CellLocation[0] = maxLoc.x;
	CellLocation[1] = maxLoc.y;
	return detected;
}


bool DetectCells(cv::Mat DropROI, cv::Mat DropMask, int *CellLocation, double Threshold, int dbg)
{
	//threshold = 800, debug_stats = False) :
	// use a fast approximation of the derivative of the frame to detect flaws within the drop. '''

	// Use an integral filter to detect gradients within drop.
	// This is different to normal convolution.
	// Define a filter to detect flaws in the horizontal direction.
	const int filter_h[2][5] = { { 1, 1, 3, 4, -1 },
	{ 4, 1, 6, 4, +1 } };

	/*
	filter_h = np.array([[1, 1, 3, 4, -1],
	[4, 1, 6, 4, +1]])
	*/
	void IntegralFilter(cv::Mat im, cv::Mat &Filtered);

	cv::Mat FilteredFrame;
	IntegralFilter(DropROI, FilteredFrame);

	convertScaleAbs(FilteredFrame, FilteredFrame);
#ifdef DEBUGSCREENOUTPUT
	namedWindow("Filtered Frame", WINDOW_AUTOSIZE);
	imshow("Filtered Frame", FilteredFrame);
#endif
	int type2 = DropMask.type();

	if (type2 != CV_8U)
		DropMask.convertTo(DropMask, CV_8U);


	const int border_x = 3, border_y = 5;

	// Make array the same size as the original by addind a hard coded sized border
	copyMakeBorder(FilteredFrame, FilteredFrame, 0, border_y, 0, border_x, BORDER_CONSTANT, 0);



	// Apply the drop mask to the frame.
	// Logical AND the frame with itself and mask with drop_mask.
	cv::Mat OutputFrame;
	FilteredFrame.copyTo(OutputFrame, DropMask);

#ifdef DEBUGOUTPUT 
	imwrite("FilteredData.jpg", OutputFrame);
#endif

	//Now filtered_frame is zero outside the drop radius.
	cv::Mat sum_v, sum_h;


	reduce(OutputFrame, sum_v, 0, REDUCE_SUM, CV_32F);
	reduce(OutputFrame, sum_h, 1, REDUCE_SUM, CV_32F);

	// Find maximum and maxima location
	double minVal, maxValV, maxValH;
	cv::Point minLoc, maxLocV, maxLocH;

	bool cell_detected = false;
	CellLocation[0] = 0;
	CellLocation[1] = 0;

	minMaxLoc(sum_v, &minVal, &maxValV, &minLoc, &maxLocV);
	if (maxValV > Threshold)
	{
		minMaxLoc(sum_h, &minVal, &maxValH, &minLoc, &maxLocH);
		if (maxValH > Threshold)
		{
			cell_detected = true;
			CellLocation[0] = maxLocV.x;
			CellLocation[1] = maxLocH.y;
		}
	}

	return(cell_detected);
}

