#include "stdafx.h"
#include "io.h"
#include <algorithm>
// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>


/* C coded IntegralFilter from Python code from Matlab
    This function exploits an integral image to perform filtering operations
    (using rectangular filters) on an image in time that only depends on the 
    image size irrespective of the filter size.
 
    Usage: fim = integralfilter(intim, f)
 
    Arguments:  intim - An integral image computed using INTEGRALIMAGE.
                f     - An n x 5 array defining the filter (see below).
 
    Returns:    fim   - The filtered image.
 
 
    Defining Filters: f is a n x 5 array in the following form
                      [ r1 c1 r2 c2 v
                        r1 c1 r2 c2 v
                        r1 c1 r2 c2 v
                          .......     ]
 
    Where (r1 c1) and (r2 c2) are the row and column coordinates defining the
    top-left and bottom-right corners of a rectangular region of the filter
    (inclusive) and v is the value to be associated with that region of the
    filter.  The row and column coordinates of the rectangular region are defined
    with respect to the reference point of the filter, typically its centre.
 
    Examples:
      f = [-3 -3  3  3  1/49]  # Defines a 7x7 averaging filter
 
      f = [-3 -3  3 -1  -1
           -3  1  3  3   1];   # Defines a differnce of boxes filter over a 7x7
                                 region where the left 7x3 region has a value
                                 of -1, the right 7x3 region has a value of +1,
                                 and the vertical line of pixels through
    rows, cols = np.shape(intim)
    fim = np.zeros((rows, cols))
    nfilters, fcols = np.shape(f)
*/
// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;


void IntegralFilter(cv::Mat im, cv::Mat &Filtered)
{
	// Implement the "Slow Integral filter" to start with
	// The fast one uses python slice and needs coding to C++ Vector

	// Filter is HARDCODED TO GET IT WORKING
	int f[2][5] = {		  { 1, 1, 3, 4, -1 },
						  { 4, 1, 6, 4, +1 } };

	// This is the matlab filter, so all the indices are 1 based.
	// Offset back to C++ 0 based
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 4; ++j)
		{
			f[i][j] = f[i][j] - 1;
		}
	
	// Convert to 8bit
	int type1, type2;
	cv::Mat intim;
	type1 = intim.type();
	im.convertTo(intim,CV_32F);
	type2 = intim.type();
	int rows = intim.rows;
	int cols = intim.cols;
	const int nfilters = 2; //Filter is hard coded so this is too
	const int FiltCols = 5; //Filter is hard coded

	
	const int rmin = 0;  // hard code
	const int cmin = 0;  // hard code
	int rmax = rows - 5;
	int cmax = cols - 3;
	
	Filtered = Mat::zeros(rmax, cmax, CV_32F); // Slighlty reduced size image
	float A, B, C, D, F;
	

	// Write as nested loops
	for (int r = rmin; r < rmax; ++r)
	{
		for (int c = cmin; c < cmax; ++c)
		{
			for (int n = 0; n < nfilters; ++n)
			{
				// In loop
				A = intim.at<float>(r+f[n][0], c+f[n][1]);
				B = intim.at<float>(r+f[n][2], c+f[n][1]);
				C = intim.at<float>(r+f[n][0], c+f[n][3]);
				D = intim.at<float>(r+f[n][2], c+f[n][3]);
				F = f[n][4]*(D - C - B + A);
				Filtered.at<float>(r, c) += F;
			}
		}
	}

	
	return;

}



void IntegralFilter2(cv::Mat im, cv::Mat &Filtered)
{
	// Implement the "Slow Integral filter" to start with
	// The fast one uses python slice and needs coding to C++ Vector

	// Filter is HARDCODED TO GET IT WORKING
	int f[2][5] = { { 1, 1, 3, 4, -1 },
	{ 4, 1, 6, 4, +1 } };

	// This is the matlab filter, so all the indices are 1 based.
	// Offset back to C++ 0 based
	for (int i = 0; i < 2; ++i)
		for (int j = 0; j < 4; ++j)
		{
			f[i][j] = f[i][j] - 1;
		}

	// Convert to 8bit
	int type1, type2;
	cv::Mat intim;
	type1 = intim.type();
	im.convertTo(intim, CV_32F);
	type2 = intim.type();
	int rows = intim.rows;
	int cols = intim.cols;
	const int nfilters = 2; //Filter is hard coded so this is too
	const int FiltCols = 5; //Filter is hard coded


	const int rmin = 0;  // hard code
	const int cmin = 0;  // hard code
	int rmax = rows - 5;
	int cmax = cols - 3;
	int Rsz = rmax - rmin;
	int Csz = cmax - cmin;

	Filtered = Mat::zeros(rmax, cmax, CV_32F); // Slighlty reduced size image
	

	cv::Mat S1,S2;
	cv:: Mat A, B, C, D;
	for (int n = 0; n < nfilters; ++n)
	{

		A = intim(Rect(cmin + f[n][1], rmin + f[n][0], Csz, Rsz));
		B = intim(Rect(cmin + f[n][1], rmin + f[n][2], Csz, Rsz));
		C = intim(Rect(cmin + f[n][3], rmin + f[n][1], Csz, Rsz));
		D = intim(Rect(cmin + f[n][3], rmin + f[n][2], Csz, Rsz));

		if (n == 0)
		{
			S1 = D - C - B - A;
		}
		else
		{
			S2 = D - C - B - A;
		}
	}
	Filtered = S2 - S1;







	return;

}