**# README #**

#  1 OVERVIEW 
In the root directory is a README pdf document that provides a guide to the process for tuning the standalone executables sorter, generator and bootstrap. 

It should be noted that this is development code, and as such, has not been rigorously tested.

Tuning of the parameters will be required due to changes in lighting conditions as well as changing positions of fluid channels as new cartridges are added. 

This document is not a comprehensive description of the working of the prototype algorithms it is a primarily a README document for the current code.

# 2	EXECUTABLES #
There are different build configurations corresponding to different usages of the codes. These are given as follows:

## 1.	DebugGUI:  
Provides a simple GUI that can be used to tune the algorithm parameters, along with a number of debug outputs to help tune the parameters (see the corresponding discussion on parameter setting for the relevant executable). 

## 2.	ReleaseGUI : 
As above, but with a reduced number of debug outputs.

## 3.	Release ##
This code will not provide an option for manipulating the parameters, but will provide the top level visualisation of the output. It should run more quickly than the other executables.

The corresponding binaries are Sorter.exe, Generator.exe, and Bootstrap.exe. Details on each of these binaries and on the process for tuning the parameters are given in sections 5.1, 6.1 and


# 8	BUILD ENVIRONMENT #
The executable code described in this document is currently held under GIT in Bitbucket.

The code has been source controlled at TTP under SVN and is SVN Version 185.

The code is build and configured for Visual Studio 2013 Pro (however it will compiled under the Express/Community version with the usual constraints).

The code has multiple configurations depending on:
 the OpenCV build
optimizer setting
Debug flags for build.

The primary options are described earlier in this document.

The code has many warnings at compilation stage, these have been noted but are not currently critical to code operation.

# 9	KNOWN BUGS #
It should be noted that this is development code, and as such, has not been rigorously tested.

There are many known bugs in the code: These include:
•	No automatic control of exposure.
•	Potential to crash code with the GUI sliders or keyboard key parameter adjustments.
•	No universal error code
•	Un optimised memory management
•	Bootstrap generation of velocities for liquid flow, no bounds checking for bootstrap output


 