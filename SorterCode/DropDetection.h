#ifndef DROPDETECTION_H
#define DROPDETECTION_H

//#include "DropDetection.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include <stdlib.h>
#include <vector>
#include <algorithm>

//#include "io.h"
using namespace std;


void Writevector(cv::Mat res, 
	char * filename);

int opencv_peaks(cv::Mat peak_array, 
	cv::Mat &PeakLoc, 
	cv::Mat &PeakHeight, 
	double minimum_peak_height, 
	double minimum_peak_distance);

bool DetectDrop(cv::Mat Frame,
	int roi_y1, 
	int roi_y2, 
	double min_pk_height,
	double min_pk_dist, 
	cv::Vec2i &DropLocation,
	int counter);


bool DetectDrop_Ed2(cv::Mat Frame,
	int roi_y1,
	int roi_y2,
	int regionPadding,
	int thresholdLevel,
	int checkDropRowTop,
	int checkDropRowBottom,
	double min_pk_dist,
	int *DropLocationX,
	int *numDrops);

bool DetectDrop_Ed(cv::Mat Frame,
	int roi_y1,
	int roi_y2,
	int topbottomborder_px,
	int thresholdLevel,
	double min_pk_dist,
	cv::Vec2i &DropLocation,
	int counter);

bool CreateDropRoi(cv::Mat frame, cv::Mat &DropROI, cv::Vec2i  &DropLocation, 
	const int Padding,int &roiX,int &roiY);

bool CreateDropMask(cv::Mat DropROI, cv::Mat &DM, cv::Vec2i &DropCentre, float &DropRadius, double thresh, vector<cv::Point> &dropContour);

bool DetectCells(cv::Mat DropROI, cv::Mat DropMask, cv::Vec2i &CellLocation, double Threshold, int dbg);

bool DetectCells_Ed(cv::Mat DropROI,cv::Mat DropMask,cv::Vec2i &CellLocation,
	int openSize,
	int erodeSize,
	int threshold,
	int dbg);

bool checkRoi(cv::Mat *frame, cv::Rect roi);

#endif