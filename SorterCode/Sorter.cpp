/*
New Sorter code.

After Ed Bean's debug

*/

#include "stdafx.h"

#define _WINSOCKAPI_

#include <conio.h>

#include "CellDetector.h"

#include <ctime>
#include <iostream>
#include <fstream>

#include "DebugFlags.h"

#include "..\Utilities\Utilities\DisplayIm.h"
#include "..\Utilities\Utilities\UDPSend.h"

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>

#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using OpenCV objects.
using namespace cv;

// Namespace for using cout.
using namespace std;

#include "..\Utilities\Utilities\BaslerCamera.h"
#include "SorterSysParams.h"

#include "SorterGui.h"

int main(int argc, char* argv[])
{
   
	int startFrom = 0; // Use this to start processing from a particular location
	int FrameCount =0;
	bool CamOK = false;// Status flag for camera
	bool UseFile = false; // Enable/disable toggle for file processing
	bool FileLoaded = false; // Status flag to know of file has been read in
	bool WaitKeySingleStep = false;

	cUDP UDP;
	UDP.Init(27129,false);// Pick a different port for Bootstrap

#ifdef EMULATEFROMFILE
	UseFile = true;
	//WaitKeySingleStep = true;
#endif

	SorterSysParams Params; 

	// Parameters defining the square box from the top (?)
	const int Left = 250, Top = 10, Width = 280, Height = 250;

	// Declare + Initialise
	bool Detected = false;	// Flag to indicate whether a cell is detected
	//int DropLocation[2];	// The location of the drop (if detected)
	Vec2i DropCentre;		// The centre of the drop with respect to the 
	//float DropRadius;

	double CellThreshold = 600;

	Vec2i CellLocation;
	Vec2i CellLocation2;

	// Initialise the required openCV images
	Mat croppedImage;
	Mat croppedImageBackup;

	Mat DropROI;
	Mat DropROI2;
	Mat DropMask;
	Mat disp;
	Mat invertedImage;
	
	Scalar Colour;

    // is initialized during the lifetime of this object.
    Pylon::PylonAutoInitTerm autoInitTerm;

	Mat avgImg; 

    try
    {
		LARGE_INTEGER Frequency, start, end;

		double freq,diff;
		char string[80];
		char filename[100];

		QueryPerformanceFrequency(&Frequency);
		cout << "QueryPerformanceFrequency -> " << _i64toa(Frequency.QuadPart, &string[0], 10) << endl;

		BaslerCamera  camera;
#ifdef ENABLECAMERA
		CamOK = camera.Init(Params.cParams.CameraSerial, Params.cParams); // Get first camera available

		if (!UseFile &&  !CamOK)
		{
			cerr << "NO CAMERA OR DATA FILE" << endl << "giving up " << endl;

			cerr << endl << "Press Enter to exit." << endl;
			while (cin.get() != '\n');
				return(-1);
		}
		else
		{
			camera.SetExposure(10);// Use lowest
			camera.SetFrameRate(-1);// Max
		}
#else
		CamOK = false;
#endif	
		// Create a PylonImage that will be used to create OpenCV images later.
		CPylonImage pylonImage;

		// Open stored video file
		sprintf(filename, "%s", Params.VideoFileName.c_str());

#ifdef DEBUGOUTPUT
		ofstream myfile;
		myfile.open("results4.csv");
#endif
		
		// Create an OpenCV image.
		Mat openCvImage;
		int NumFrames=200;

		int NumDrops=0;
		int NumCells=0;

#ifdef EMULATEFROMFILE
		VideoCapture vidCap;

		vidCap.open(filename);
	
		int NumFramesInFile = vidCap.get(CAP_PROP_FRAME_COUNT);

		if (NumFramesInFile < 10)
		{
			cout << " NO FRAMES IN DATA FILE !" << endl;
			UseFile = false;
		}

		// Allocate and read in the frames
		// pre-load to avoid this read giving misleading load times

		Mat Frames[Params.c_countOfImagesToGrab];
		NumFrames = Params.c_countOfImagesToGrab;

		if (NumFramesInFile < NumFrames)
		{
			cout << "Too few frames" << endl;
			NumFrames = NumFramesInFile;
		}

		cout << "Reading frames" << endl;

		for (int i = 0; i < NumFrames; ++i)
		{
			vidCap.read(Frames[i]);  // Fake a frame from the stored file
			if (Frames[i].empty())
			{
				cerr << "ERROR! blank frame grabbed\n";
				break;
			}
		}
		cout << NumFrames << " Frames read into memory " << endl;

#endif // Read in from file

		// Other openVCImages
		Mat filtered_frame, mask,frame;

#ifdef recordVideo
		// Create an OpenCV video creator.
		VideoWriter cvVideoCreator;

		// Define the video file name.
		std::string videoFileName = "c:\\Temp\\openCvVideo.avi";

		// Define the video frame size.
		cv::Size frameSize = Size((int)camera.GetWidth(), (int)camera.GetHeight());

		// Set the codec type and the frame rate. You have 3 codec options here.
		// The frame rate should match or be lower than the camera acquisition frame rate.
		cvVideoCreator.open(videoFileName, CV_FOURCC('D','I','V','X'), 20, frameSize, true);
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','P','4','2'), 20, frameSize, true); 
		//cvVideoCreator.open(videoFileName, CV_FOURCC('M','J','P','G'), 20, frameSize, true);
#endif


		// Start main grab and process loop
		bool StopLoop = false;

		//CamOK = false;
		// Just to be sure
#ifndef ENABLECAMERA
		CamOK = false;
#endif
		bool GotValidFrame = false;
		
		// Define and initialise the cell detector class
		CellDetector cellDetector(Params.sorterRoiY1.val, Params.sorterRoiY2.val, Params.sorterTopBottomBorder_px.val,
			Params.sorterThresholdLevelDrop.val, Params.sorterMinPkDist.val, Params.sorterCellPadding.val, Params.sorterOpenSize.val,
			Params.sorterErodeSize.val, Params.sorterThresholdLevelCell.val, Params.sorterCheckDropRowBottom.val, Params.sorterCheckDropRowTop.val);
		
#ifdef WITHGUI
		SorterGui sorterGui;
		sorterGui.SorterGuiInit(&cellDetector, &Params, &camera);
#endif

		// Starting processing loop
		QueryPerformanceCounter(&start);
		while (!StopLoop)
        {
			GotValidFrame = false;

			if (CamOK)
				camera.GrabNextImg(); // Just a grab

			cout << fixed << FrameCount << endl;

			// Image grabbed successfully?
			if (CamOK && camera.GrabSuccesful())
			{
				Mat camframe;
				int Chf = frame.channels();

				bool ok = camera.GetMatImage(camframe);

				frame = camframe.clone();// Make a safe COPY out of volatile camera memory

				if (ok)
				{

						GotValidFrame = true;
						if (Params.DisplayMode[WGrab])
						{
							Mat out;
							Size osize;
							try
							{
								resize(frame, out, osize, 0.5, 0.5, INTER_LINEAR);
								displayIm("Cam", out);
							}
							catch (InvalidArgumentException e)
							{
								// Get an exception sometimes, not sure why
								cout << "Frame exception: " << e.what() << endl;
							}
						}
					
				}
				else
				{
					cout << "Invalid frame from camera ?! " << endl;
					ok = false;
					GotValidFrame = false;
				}
			}
			else
			{
				if (CamOK)
					cout << "Failed to grab" << endl;
				// Otherwise the camera is just disconnected and we are in video mode
			}


#ifdef EMULATEFROMFILE
			Mat getframe;
			if (UseFile)
			{
				getframe = Frames[FrameCount];
				getframe.copyTo(frame);
				if ((frame.rows > 10) && (frame.cols > 10))
					GotValidFrame = true;
				else
					GotValidFrame = false;
			}
#endif
			//vidCap.read(frame);  // Fake a frame from the stored file
				// Process loop
				if(GotValidFrame)
				{ 
					if (UseFile)
					{
						// Extract square box from the top
						if (Params.VideoIsPreCropped)
						{
							croppedImage = frame;
							cvtColor(croppedImage, croppedImage, COLOR_BGR2GRAY);
						}
						else
						{
							croppedImage = frame(Rect(Left, Top, Width, Height));
							// Write the cropped image to file
							cvtColor(croppedImage, croppedImage, COLOR_BGR2GRAY);
						}
					}
					else
					{
						croppedImage = frame;
					}

#ifdef SCREENOUTPUT		
					// Hunt for cells within a drop
					imshow("Input Image",croppedImage);
#endif

					if (cellDetector.updateFrame(&croppedImage))
					{
						char Cstr[200];

						if (cellDetector.IsCelldetected())
							sprintf(Cstr, "1\n");// Mean Time
						else
							sprintf(Cstr, "0\n");// Mean Time

						UDP.SendString(Cstr);

					}
					if (Params.DisplayMode[WCellDetection])
					{
						// Plot the result
						cellDetector.plotResult("Main Output", croppedImage, Left, Top);
					}

#ifdef recordVideo
						
							cvVideoCreator.write(openCvImage);
							grabbedImages++;
							//cv::accumulate(openCvImage, avgImg)		
#endif					

							FrameCount = FrameCount + 1;
							if (FrameCount > NumFrames - 2)
							{
								FrameCount = 2;// avoid the special conditions
								cout << "Rewind" << endl;
								cellDetector.Init();
								//getDropletSize.WriteVectors();
								//getDropletSize.ResetVectors();

								//StopLoop = true;// ONE PASS FOR SPEED TEST
							}


							// Step through if this is being read from a file and we want single step mode
							if (WaitKeySingleStep)
								waitKey(0);

						
#ifdef PYLON_WIN_BUILD
						// Display the grabbed image in pylon.
						//Pylon::DisplayImage(1, ptrGrabResult);
#endif
					
				}
				// Produce a nice image with the cell location outlined
				/*
				if (Detected)
				{
					Colour = Scalar(255, 0, 0);
					circle(frame, Point(DropCentre[0] + Left + roiX, DropCentre[1] + Top + roiY), DropRadius, Colour);
				}
				else
				{
					Colour = Scalar(0, 255, 0);
				}*/
				
				


#ifdef DEBUGOUTPUT
				//myfile << FrameCount << "," << CellDetected2 << "," << CellDetected << "," << Detected << "," << CellLocation2[0] << "," << CellLocation2[1] << "," << DropLocation[0] << "," << DropLocation[1] << endl;
				//  displayIm("Full Frame", frame);				
#endif
            //}
            //else
            //{
              //  cout << "Error: " << ptrGrabResult->GetErrorCode() << " " << ptrGrabResult->GetErrorDescription() << endl;
            //}

				/*
				if (_kbhit())
				{
					cout << "ok" << endl;
					int key = _getch();
					if (key != 224)
					{
						if (key == 'Q')
						{
							StopLoop = true;
						}

						if (key == '[')
						{
							float gain;
							uint16_t Exp, Rate;
							camera.GetGainExpRate(gain, Exp, Rate);
							cout << "Current gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
							Params.cParams.gain = Params.cParams.gain + (float)1;
							camera.SetGain(Params.cParams.gain);
						}

						if (key == ']')
						{
							float gain;
							uint16_t Exp, Rate;
							camera.GetGainExpRate(gain, Exp, Rate);
							cout << "Current Gain: " << gain << " Exp: " << Exp << " Rate " << Rate << endl;
							Params.cParams.gain = Params.cParams.gain - (float)0.5;

							if (Params.cParams.gain  < 0.0)
								Params.cParams.gain = 0.0;
							camera.SetGain(Params.cParams.gain);
						}
						if (key == 'g')
						{
							// Adjust Ed's threshold (together)
							Params.sorterThresholdLevelDrop = Params.sorterThresholdLevelDrop - 1;
							// Params.sorterThresholdLevelDrop = Params.sorterThresholdLevelDrop;
							cout << endl << "Changed sorterThresholdLevelDrop: " << fixed << Params.sorterThresholdLevelDrop << endl;

						}
						if (key == 't')
						{
							// Adjust Ed's threshold
							Params.sorterThresholdLevelDrop = Params.sorterThresholdLevelDrop + 1;
							// Params.thresholdLevel2 = Params.thresholdLevel1;
							cout << endl << "Changed sorterThresholdLevelDrop & 2: " << fixed << Params.sorterThresholdLevelDrop << endl;

						}
						
						if (key == '#')
							WaitKeySingleStep = !WaitKeySingleStep;

						if (key == 'a')
						{
							// Adjust roi_y1
							Params.sorterRoiY1 = Params.sorterRoiY1 - 1;
							cout << endl << "Changed sorterRoiY1: " << fixed << Params.sorterRoiY1 << endl;

						}
						if (key == 'q')
						{
							// Adjust Ed's threshold
							Params.sorterRoiY1 = Params.sorterRoiY1 + 1;
							cout << endl << "Changed sorterRoiY1 " << fixed << Params.sorterRoiY1 << endl;
						}


						if (key == 's')
						{
							// Adjust roi_y1
							Params.sorterRoiY2 = Params.sorterRoiY2 - 1;
							cout << endl << "Changed roi_y1: " << fixed << Params.sorterRoiY2 << endl;

						}
						if (key == 'w')
						{
							// Adjust Ed's threshold
							Params.sorterRoiY2 = Params.sorterRoiY2 + 1;

							cout << endl << "Changed roi_y2 " << fixed << Params.sorterRoiY2 << endl;
						}

						if (key == 'f')
						{
							// Adjust roi_y1
							Params.sorterCellPadding = Params.sorterCellPadding - 1;
							cout << endl << "Changed sorterCellPadding: " << fixed << Params.sorterCellPadding << endl;

						}
						if (key == 'r')
						{
							Params.sorterCellPadding = Params.sorterCellPadding + 1;
							cout << endl << "Changed regionPadding " << fixed << Params.sorterCellPadding << endl;
						}

						if (key == '9')
						{
							// Adjust roi_y1
							Params.Padding = Params.Padding - 1;
							cout << endl << "Changed Padding: " << fixed << Params.Padding << endl;

						}
						if (key == '0')
						{
							Params.Padding = Params.Padding + 1;
							cout << endl << "Changed Padding " << fixed << Params.Padding << endl;
						}

						if (key == 'd')
						{
							Params.sorterMinPkDist = Params.sorterMinPkDist + 1;
							cout << endl << "Changed sorterMinPkDist: " << fixed << Params.sorterMinPkDist << endl;

						}
						if (key == 'c')
						{
							Params.sorterMinPkDist = Params.sorterMinPkDist - 1;
							cout << endl << "Changed sorterMinPkDist " << fixed << Params.sorterMinPkDist << endl;
						}
						if (key == 'l')
						{
							Params.sorterErodeSize = Params.sorterErodeSize - 1;
							cout << endl << "Changed sorterErodeSize: " << fixed << Params.sorterErodeSize << endl;
						}
						if (key == 'o')
						{
							Params.sorterErodeSize = Params.sorterErodeSize + 1;
							cout << endl << "Changed sorterErodeSize " << fixed << Params.sorterErodeSize << endl;
						}

						if (key == 'k')
						{
							Params.sorterOpenSize = Params.sorterOpenSize - 1;
							cout << endl << "Changed sorterOpenSize: " << fixed << Params.sorterOpenSize << endl;
						}
						if (key == 'i')
						{
							Params.sorterOpenSize = Params.sorterOpenSize + 1;
							cout << endl << "Changed sorterOpenSize " << fixed << Params.sorterOpenSize << endl;
						}
						if (key == '.')
						{
							Params.sorterTopBottomBorder_px = Params.sorterTopBottomBorder_px - 1;
							cout << endl << "Changed sorterTopBottomBorder_px: " << fixed << Params.sorterTopBottomBorder_px << endl;
						}
						if (key == 'l')
						{
							Params.sorterTopBottomBorder_px = Params.sorterTopBottomBorder_px + 1;
							cout << endl << "Changed sorterTopBottomBorder_px " << fixed << Params.sorterTopBottomBorder_px << endl;
						}
					
						if (key == '1')
						{
							Params.sorterThresholdLevelCell = Params.sorterThresholdLevelCell - 1;
							cout << endl << "Changed sorterThresholdLevelCell: " << fixed << Params.sorterThresholdLevelCell << endl;
						}
						if (key == '2')
						{
							Params.sorterThresholdLevelCell = Params.sorterThresholdLevelCell + 1;
							cout << endl << "Changed sorterThresholdLevelCell " << fixed << Params.sorterThresholdLevelCell << endl;
						}
						if (key == 'j')
						{
							Params.sorterCheckDropRowTop = Params.sorterCheckDropRowTop - 1;
							cout << endl << "Changed sorterCheckDropRowTop: " << fixed << Params.sorterCheckDropRowTop << endl;
						}
						if (key == 'm')
						{
							Params.sorterCheckDropRowTop = Params.sorterCheckDropRowTop + 1;
							cout << endl << "Changed sorterCheckDropRowTop " << fixed << Params.sorterCheckDropRowTop << endl;
						}
						if (key == 'h')
						{
							Params.sorterCheckDropRowBottom = Params.sorterCheckDropRowBottom - 1;
							cout << endl << "Changed sorterCheckDropRowBottom: " << fixed << Params.sorterCheckDropRowBottom << endl;
						}
						if (key == 'n')
						{
							Params.sorterCheckDropRowBottom = Params.sorterCheckDropRowBottom + 1;
							cout << endl << "Changed sorterCheckDropRowBottom " << fixed << Params.sorterCheckDropRowBottom << endl;
						}
						/*
						if (key == 'n')
						{
							Params.checkDropRowBottom = Params.checkDropRowBottom - 1;
							cout << endl << "Changed checkDropRowBottom: " << fixed << Params.checkDropRowBottom << endl;
						}
						if (key == 'h')
						{
							Params.checkDropRowBottom = Params.checkDropRowBottom + 1;
							cout << endl << "Changed checkDropRowBottom " << fixed << Params.checkDropRowBottom << endl;
						}

						if (key == '7')
						{
							Params.checkDropRowBottom = Params.checkDropRowBottom - 1;
							cout << endl << "Changed checkDropRowBottom: " << fixed << Params.checkDropRowBottom << endl;
						}
						if (key == '8')
						{
							Params.checkDropRowBottom = Params.checkDropRowBottom + 1;
							cout << endl << "Changed checkDropRowBottom " << fixed << Params.checkDropRowBottom << endl;
						}
						*/
						/*
						if (key == 'e')
						{
						if (Params.DisplayMode[WDisplayEd])
						Params.DisplayMode[WDisplayEd] = false;
						else
						Params.DisplayMode[WDisplayEd] = true;
						}

						if (key == 'g')// Grab window
						{
						bool flag = true;
						if (Params.DisplayMode[WGrab])
						flag = false;
						Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
						}

						if (key == 'a')
						{
						bool flag=true;
						if (Params.DisplayMode[WGrab])
						flag = false;
						Params.DisplayMode[WGrab] = flag; // Display this window   WOutput
						Params.DisplayMode[WHough] = flag;
						Params.DisplayMode[WBack] = flag;
						Params.DisplayMode[WCropped] = flag;
						Params.DisplayMode[WEdges] = flag;
						Params.DisplayMode[WHist] = true;
						Params.DisplayMode[WOutput] = flag;
						Params.DisplayMode[WDisplayEd] = flag;
						}

						*/ // Commented for now, will sort this out later. 

/*
					}
					else
					{
						int key2 = _getch();

						if (key2 == 72) // Up
						{
						*/
#ifdef ENABLECAMERA

						/*	camera.RoiUp(1);
							Rect Cur = camera.GetROI();
							cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
							// Update params accordingly. 
							Params.cParams.FrameROIr.width = Cur.width;
							Params.cParams.FrameROIr.height = Cur.height;*/
#endif

#ifdef EMULATEFROMFILE

						//	Params.FrameROIr.x = Params.FrameROIr.x - 1;
						//	cout << "ROI Up: X " << Params.FrameROIr.x << endl;
							// Update params accordingly. 
#endif
/*
						}
						if (key2 == 80) // Down
						{
#ifdef ENABLECAMERA

						/*	camera.RoiDown(1);

							Rect Cur = camera.GetROI();
							cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
							// Update params accordingly. 
							Params.cParams.FrameROIr.width = Cur.width;
							Params.cParams.FrameROIr.height = Cur.height;*/

//#endif
#ifdef EMULATEFROMFILE

						//	Params.FrameROIr.x = Params.FrameROIr.x + 1;
						//	bool check = Params.checkVarBounds(&(Params.FrameROIr.x), 1, frame.cols);
						//	cout << "ROI: Down " << Params.FrameROIr.x << endl;
#endif


						}
						/*	if (key2 == 'v')
						{
						if (UseFile)
						UseFile = false;
						else
						UseFile = true;
						FrameCount = 0;
						getDropletSize.ResetVectors();
						}
						if (key2 == '?')
						{
						double val = camera.GetRealFrameRate();
						cout << " Frame rate: " << fixed << val << endl << endl;
						}
						if (key2 == 'p')
						{
						Rect Cur = camera.GetROI();
						cout << "ROI: X " << Cur.x << " Y " << Cur.y << " W " << Cur.width << " H " << Cur.height << endl;
						cerr << endl << "Press Enter to continue." << endl;
						while (cin.get() != '\n');*/
						/*

					}
					bool changeOk = Params.checkBounds();
					if (!changeOk)
					{
						cout << "Value was out of bounds." << endl;
					}
					cellDetector.updateParams(Params.sorterRoiY1, Params.sorterRoiY2, Params.sorterTopBottomBorder_px,
						Params.sorterThresholdLevelDrop, Params.sorterMinPkDist, Params.sorterCellPadding, Params.sorterOpenSize,
						Params.sorterErodeSize, Params.sorterThresholdLevelCell,Params.sorterCheckDropRowBottom,Params.sorterCheckDropRowTop);
				}
				

		}
		*/

		QueryPerformanceCounter(&end);

#ifdef DEBUGOUTPUT
		myfile.close();
#endif

		diff = (double)(end.QuadPart - start.QuadPart);
		freq = (double)Frequency.QuadPart;
		diff = diff / freq;

		cout << "end - start -> " << _i64toa(end.QuadPart - start.QuadPart, &string[0], 10) << endl;


		printf("%d frames in %g sec, rate %g\n", NumFrames, diff, ((double)NumFrames )/ diff);

		//cout << "Num frames" << _i64toa_s(grabbedImages,&string[0],30,10) << endl;

		// Release the video file on leaving.
#ifdef recordVideo
			cvVideoCreator.release();
#endif
    }
    catch (GenICam::GenericException &e)
    {
        // Error handling.
        cerr << "An exception occurred." << endl
        << e.GetDescription() << endl;

		cerr << endl << "Press Enter to exit." << endl;
		while (cin.get() != '\n');
    }

    // Comment the following two lines to disable waiting on exit.
    cerr << endl << "Press Enter to exit." << endl;
    while( cin.get() != '\n');

    return 0;
}
