#include "stdafx.h"


#pragma comment(lib, "vfw32.lib")
#pragma comment( lib, "comctl32.lib" )


#include <stdio.h>
#include <iostream>

//#define DEBUGSCREENOUTPUT

//#include "SorterSysParams.h"
#include "..\Utilities\Utilities\DisplayIm.h"
#include "..\Utilities\Utilities\WriteVector.h"

#include "DropDetection.h"
#include "DebugFlags.h"

// Namespace for using cout.
using namespace std;

using namespace cv;


void Writevector(cv::Mat res, char * filename)
{
	FILE *file;
	cv::Mat out;
	res.convertTo(out, CV_32F);

	double minVal, maxVal;
	minMaxLoc(out, &minVal, &maxVal);
	cout << "max/min " << std::fixed << maxVal << " " << std::fixed << minVal << endl;

	float *my_data = (float *)out.data;
	file = fopen(filename, "wt");
	int i;
	for (i = 0; i < res.cols; ++i)
	{
		fprintf(file, "%d,%f\n", i, my_data[i]);
	}
	fclose(file);
}


int opencv_peaks(cv::Mat peak_array, cv::Mat &PeakLoc, cv::Mat &PeakHeight, double minimum_peak_height, double minimum_peak_distance)
{
	// Detect peaks on a 1D array using OpenCV calls to calculate derivatives.
	// Return the index of the peaks on the input array.
	// calculate 1st and 2nd deriviatives
	// zeroth derivative, grad0
	// Laplacian grad2
	int skipPoints = 4; // Note, ELB changed from 15 to 4
#ifdef DEBUGOUTPUT 
	WriteVector(peak_array, "PeakArray.csv");
#endif
	// 2nd derivative, grad2
	cv::Mat grad0, grad2;

	// threshold zeroth derivative above minimum peak height
	double thresh;
	thresh = threshold(peak_array, grad0, minimum_peak_height, 1, THRESH_BINARY);
#ifdef DEBUGOUTPUT 
	WriteVector(grad0, "Thres1.csv");
#endif
	// 2nd derivative
	Laplacian(peak_array, grad2, CV_32F, 1);
	cv::InputArray NullArray = cv::noArray();
#ifdef DEBUGOUTPUT 
	WriteVector(grad2, "Laplacian.csv");
#endif
	double second_derivative_threshold = -0.14;

	// grad2 is the threshold of the computed 2nd derivative for the values BELOW the threshold
	thresh = threshold(grad2, grad2, second_derivative_threshold, 1, THRESH_BINARY_INV);

#ifdef DEBUGOUTPUT 
	WriteVector(grad2, "Thres2.csv");
#endif

	Mat res;

	//# Find the locations where all the conditions specified are met.
	int Type0 = grad0.type();
	int Type1 = grad2.type();
	grad2.convertTo(grad2, Type0);
	bitwise_and(grad0, grad2, res, NullArray);

	//Get the index locations of the peaks.
	//peak_locations = np.where(peaks == True)
	//# Convert to a 1D vector.
	//peak_locations = peak_locations[0]
	//Writevector(res, "BitWiseAnd.csv");

	Mat list(1, res.cols, CV_16U);
	int count = 0;
	// Find where the data is non-Zero
	// Skip 15 points at the start and end

	for (int i = skipPoints; i < res.cols - skipPoints; ++i)
	{
		if (res.at<float>(i) > 0)
		{
			list.at<unsigned short int>(count) = i;
			count = count + 1;
		}
	}

	// Reduce size of list to just the filled values
	cv::Mat RedList = list(Rect(0, 0, count, 1));
	//Writevector(RedList, "RPointList.csv");

	const int BufLen = 100;
	//int CurrentIndex[BufLen];
	//int CurrentValue[BufLen];

	int TruePeakLocations[BufLen];
	float TruePeakHeight[BufLen];
	int TrueCount = 0;
	int mx_pt;
	float mx_val = -999.0;
	float val;
	int pt, last_pt;

	//memset(&CurrentValue, 0, sizeof(int) * BufLen);// Zero the array

	int CurC = 0;
	for (int loc = 1; loc < count; ++loc)
	{
		pt = RedList.at<unsigned short int>(loc);
		last_pt = RedList.at<unsigned short int>(loc - 1);
		if (pt - 1 == last_pt)
		{
			// Consecutive points
			//CurrentIndex[CurC] = pt;
			val = peak_array.at<float>(pt);
			//CurrentValue[CurC] = val;
			if (val > mx_val)
			{
				mx_val = val;
				mx_pt = pt;
			}
			//++CurC;
		}
		else
		{
			if (loc == count - 1)
			{
				//Process end block, or actually just dont bother

			}
			else
			{
				// We are about to skip to the new block, so process this one
				TruePeakLocations[TrueCount] = mx_pt;
				TruePeakHeight[TrueCount] = mx_val;
				mx_val = -999.0;
				mx_pt = 0;
				++TrueCount;
			}
		}
	}

	Mat PL(1, TrueCount, CV_16U);
	Mat PH(1, TrueCount, CV_32F);
	for (int i = 0; i < TrueCount; ++i)
	{
		PL.at<unsigned short int>(i) = TruePeakLocations[i];
		PH.at<float>(i) = TruePeakHeight[i];
	}

	PeakLoc = PL;
	PeakHeight = PH;
	return(PL.cols);
}


bool DetectDrop_Ed(cv::Mat Frame, int roi_y1, int roi_y2, 
	int topbottomborder_px,
	int thresholdLevel,
	double min_pk_dist, 
	Vec2i &DropLocation, 
	int counter)
{
/*   Detect if a drop is present on the input frame by measuring the peaks
along the X axis of the frame.
Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2).  
Note: this was modified from the original function to follow the process of:
	1. Take inverse threshold to find 'halo' around drops
	2. Take a small ROI in the centre of the images
	3. Sum the columns (reduce to a 1D array)
	4. Find where the peaks are
	5. Select the leftmost peak that is separated by the diameter
	6. Check for the top and bottom of the drop (to make sure that
	   the peaks are the edges of the same drop and not two different drops)


Possible improvements include a more sensible management of the intermediate images (i.e., passing
them in by argument to avoid lots of memory allocs/deallocs that must be going on under the hood) and
also using binary images (or at least int) after the image has been thresholded. 

Input
-----
	Frame: The input image in which a drop should be found
	roi_y1: The start column that should be selected
	roi_y2: The end column that should be selected
	min_pk_dist: The distance which corresponds to the minimum diameter of a drop. 
	topbottomborder_px: The number of pixels to ignore at the top and bottom of the image
	counter: A flag for debug logging

Output
------
	DropLocation: Vector specifying the x positions that bound the drop diameter
*/

	// Declaration of parameters
	stringstream ss;

	int leftIdx = -1;
	double peakArrayBuffer[50]; 
	bool DropDetected = false;
	cv::Mat peak_array2; // Copy a second array to subtract. Could be more memory efficient


	// Determine ROI
	int NumPeaks = 0;
	int width = Frame.cols;
	int height = Frame.rows;

	Mat invertedImage;// = Frame.clone();	// FIX
	Mat invertedImage2;// = Frame.clone();

	width = width - 2;

	// Find the inverse binary threshold to capitalise on the well defined black halo around the circles 
	threshold(Frame, invertedImage, thresholdLevel, 255, THRESH_BINARY_INV);
	invertedImage2 = invertedImage;

	displayIm("Inverse", invertedImage);

#ifdef DEBUGOUTPUT 
	ss.str("");
	ss << "Inverted" << counter << ".jpg";
	imwrite((string)ss.str(), invertedImage);
#endif

	

	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = invertedImage(ROI);


#ifdef DEBUGOUTPUT 
	// Some temporary logging 
	ss.str("");
	ss << "PeakArray" << counter << ".jpg";
	imwrite((string)ss.str(), peak_array);
#endif

	

	// Sum along columns to find a line profile
	peak_array.convertTo(invertedImage, CV_32F); // Note, probably not necessary if using REDUCE_MAX
	reduce(invertedImage, peak_array, 0, REDUCE_MAX, -1);
	peak_array2 = peak_array.clone();
	// Some temporary logging 
#ifdef DEBUGOUTPUT 
	Writevector(peak_array, "PreBlur.csv");  // Write the pre blur vector
#endif
	// Quickly find connected points
	// Pad to the right and left of the vectors
	copyMakeBorder(peak_array, peak_array, 0, 0, 1, 0, 0, 0);
	copyMakeBorder(peak_array2, peak_array2, 0, 0, 0, 1, 0, 0);
	// Now subtract the offset vectors
	peak_array = peak_array - peak_array2;
#ifdef DEBUGOUTPUT 
	Writevector(peak_array, "Subtract.csv");
#endif
	// Set default, NULL locations for the Droplet
	DropLocation[0] = 0;
	DropLocation[1] = 0;

	// Loop over the whole array and find anywhere that there
	// are two peaks separated by a given threshold. Note, 
	// the first drop from the left is selected and the loop is exited as soon as it is found. 
	// When a peak across x direction is found, 2 peaks must be found in the y direction
	// To confirm that it is a drop. 
	for (int i = 0; i < peak_array.cols; i++)
	{
		// Check if its the first peak or if its there is sufficient
		// spacing between the peaks. 
		if (peak_array.at<float>(i) > 0)
			if (leftIdx < 0)
			{
				leftIdx = i;
			}
			else if (i - leftIdx > min_pk_dist)
			{

				// Check to see if this is valid by looking for 2 peaks along the columns at the centre
				int temp = (int)((i - leftIdx) / 2 + leftIdx);
				cv::Rect ROIcol(temp, topbottomborder_px, 1, 
					height - topbottomborder_px - topbottomborder_px);
				if (!checkRoi(&invertedImage2, ROIcol))
				{
					return false;
				}

 				cv::Mat roi2 = invertedImage2(ROIcol);
				cv::Mat roi3;
				copyMakeBorder(roi2, roi3, 1, 0, 0, 0, 0, 0);
				copyMakeBorder(roi2, roi2, 0, 1, 0, 0, 0, 0);
				roi2 = roi2 - roi3;
				roi2.convertTo(roi2, CV_32F); // Note, probably not necessary if using REDUCE_MAXs
				int vertCount = 0;
				for (int j = 0; j < roi2.rows; j++)
				{
					if (roi2.at<float>(j) > 0)
					{
						vertCount = vertCount + 1;
					}
				}
				// If 1 peaks have been found, this is a drop and the location
				// will be added to DropLocation and the loop exited accordingly. ideally, 
				// it would be 2 peaks: but 1 peak covers the situation where the drop wanders
				// towards the top/bottom of the image.
				if (vertCount>0)
				{ 
					peakArrayBuffer[NumPeaks] = leftIdx;
					DropDetected = true;
					NumPeaks = NumPeaks + 1;
					DropLocation[0] = leftIdx;
					DropLocation[1] = i;
					leftIdx = i;
					// Exit as we only want the left most cell.
					break;
					// Check to make sure that the peak array buffer is not full
					if (NumPeaks > 50)
					{
						cout << "Error: more peaks than available buffer detected." << endl;
						return false;
					}
				}
			}
			else
			{
				leftIdx = i;
			}
	}
	
	return(DropDetected);
}




bool DetectDrop_Ed2(cv::Mat Frame,
	int roi_y1,
	int roi_y2,
	int regionPadding,
	int thresholdLevel,
	int checkDropRowTop,
	int checkDropRowBottom,
	double min_pk_dist,
	int *DropLocationX,
	int *numDrops)
{
	/*   Detect if a drop is present on the input frame by measuring the peaks
	along the X axis of the frame.
	Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2).
	Note: this was modified from the original function to follow the process of:
	1. Take inverse threshold to find 'halo' around drops
	2. Take a small ROI in the centre of the images
	3. Sum the columns (reduce to a 1D array)
	4. Find where the peaks are
	5. Select the leftmost peak that is separated by the diameter
	6. Check for the top and bottom of the drop (to make sure that
	the peaks are the edges of the same drop and not two different drops)


	Possible improvements include a more sensible management of the intermediate images (i.e., passing
	them in by argument to avoid lots of memory allocs/deallocs that must be going on under the hood) and
	also using binary images (or at least int) after the image has been thresholded.

	Input
	-----
	Frame: The input image in which a drop should be found
	roi_y1: The start column that should be selected
	roi_y2: The end column that should be selected
	min_pk_dist: The distance which corresponds to the minimum diameter of a drop.
	topbottomborder_px: The number of pixels to ignore at the top and bottom of the image
	counter: A flag for debug logging

	Output
	------
	DropLocation: Vector specifying the x positions that bound the drop diameter
	*/

	// Declaration of parameters
	stringstream ss;
	bool tryLeft = false;
	int leftIdx = -1;
	double peakArrayBuffer[200];
	bool DropDetected = false;
	cv::Mat peak_array2; // Copy a second array to subtract. Could be more memory efficient

	// Determine ROI
	int NumPeaks = 0;
	int width = Frame.cols;
	int height = Frame.rows;
	cv::Mat invertedImage = Frame.clone();
	cv::Mat invertedImage2 = Frame.clone();
	width = width - 2;
	displayIm("Frame1", Frame);

	// Find the inverse binary threshold to capitalise on the well defined black halo around the circles 
	threshold(Frame, invertedImage, thresholdLevel, 255, THRESH_BINARY_INV);
	invertedImage2 = invertedImage;

#ifdef DEBUGOUTPUT 
	ss.str("");
	ss << "Frame" << 1 << ".jpg";
	imwrite((string)ss.str(), Frame);
	ss.str("");
	ss << "invertedImage" << 1 << ".jpg";
	imwrite((string)ss.str(), invertedImage);

#endif

	displayIm("Inverted", invertedImage);
	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = invertedImage(ROI);

#ifdef DEBUGOUTPUT 
	// Some temporary logging 
	ss.str("");
	ss << "PeakArray" << 1 << ".jpg";
	imwrite((string)ss.str(), peak_array);
#endif
	// Sum along columns to find a line profile
	peak_array.convertTo(invertedImage, CV_32F); // Note, probably not necessary if using REDUCE_MAX
	reduce(invertedImage, peak_array, 0, REDUCE_SUM, -1);
	threshold(peak_array, peak_array, (roi_y2 - roi_y1)*0.5 * 255, 255, THRESH_BINARY);

	peak_array2 = peak_array.clone();
	// Some temporary logging 
#ifdef DEBUGOUTPUT 
	Writevector(peak_array2, "PreBlur.csv");  // Write the pre blur vector
	imshow("peak_array2.jpg", peak_array2);  // Write the pre blur vector
#endif
	// Quickly find connected points
	// Pad to the right and left of the vectors
	copyMakeBorder(peak_array, peak_array, 0, 0, 1, 0, 0, 0);
	copyMakeBorder(peak_array2, peak_array2, 0, 0, 0, 1, 0, 0);
	// Now subtract the offset vectors
	peak_array = peak_array - peak_array2;

#ifdef DEBUGOUTPUT 
	Writevector(peak_array, "Subtract.csv");
	imwrite("Subtract1.jpg", peak_array);  // Write the pre blur vector
#endif
	// Set default, NULL locations for the Droplet
	DropLocationX[0] = 0;


	// Loop over the whole array and find anywhere that there
	// are two peaks separated by a given threshold. Note, 
	// the first drop from the left is selected and the loop is exited as soon as it is found. 
	// When a peak across x direction is found, 2 peaks must be found in the y direction
	// To confirm that it is a drop. 
	for (int i = 0; i < peak_array.cols; i++)
	{
		// Check if its the first peak or if its there is sufficient
		// spacing between the peaks. 
		if (peak_array.at<float>(i) > 0)
			if (leftIdx < 0)
			{
				leftIdx = i;

			}
			else if (i - leftIdx > min_pk_dist)
			{

				// Check to see if this is valid by looking for 2 peaks along the columns at the centre
				int temp = (int)((i - leftIdx) / 2 + leftIdx);
				// Check to see if the side parts of the drops are visible, or check to see if we are somehow
				// between drops. Do this by checking the bottom and top of the images over a 5 row region, and
				// summing accordingly. 
				cv::Rect ROIcol(leftIdx - regionPadding, checkDropRowTop, i - leftIdx + 2 * regionPadding, 5);
				cv::Rect ROIcol2(leftIdx - regionPadding, checkDropRowBottom, i - leftIdx + 2 * regionPadding, 5);
#ifdef DEBUGOUTPUT
				cv::Rect ROIVis(leftIdx - regionPadding, 1, i - leftIdx + 2 * regionPadding, height - 1);
#endif
				if (ROIcol.x < 0)
				{
					ROIcol.x = 0;
					ROIcol2.x = 0;
#ifdef DEBUGOUTPUT
					ROIVis.x = 0;
#endif

				}
				if (ROIcol.width + ROIcol.x >= invertedImage2.cols)
				{
					ROIcol.width = invertedImage2.cols - ROIcol.x - 1;
					ROIcol2.width = invertedImage2.cols - ROIcol2.x - 1;
#ifdef DEBUGOUTPUT

					ROIVis.width = invertedImage2.cols - ROIVis.x - 1;
#endif
				}

#ifdef DEBUGOUTPUT
				cv::Mat roiVis = invertedImage2(ROIVis);
#endif
				//cv::Rect ROIcol(temp, topbottomborder_px, 1, height - topbottomborder_px - topbottomborder_px);
				cv::Mat roi2 = invertedImage2(ROIcol);
				roi2.convertTo(roi2, CV_32F); // Note, probably not necessary

				reduce(roi2, roi2, 0, REDUCE_SUM, -1);
				threshold(roi2, roi2, 255 * 4, 255, THRESH_BINARY);
				cv::Mat roi1 = invertedImage2(ROIcol2);
				roi1.convertTo(roi1, CV_32F); // Note, probably not necessary
				reduce(roi1, roi1, 0, REDUCE_SUM, -1);
				threshold(roi1, roi1, 255 * 4, 255, THRESH_BINARY);
				cv::Mat roi3;
				copyMakeBorder(roi2, roi3, 0, 0, 0, 1, BORDER_CONSTANT, Scalar(1));
				copyMakeBorder(roi2, roi2, 0, 0, 1, 0, BORDER_CONSTANT, Scalar(1));

#ifdef DEBUGOUTPUT
				ss.str("");
				ss << "roi1_" << (double)i << ".jpg";
				imwrite((string)ss.str(), roi2);

				ss.str("");
				ss << "roiVis_" << (double)i << ".jpg";
				imwrite((string)ss.str(), roiVis);
#endif
				roi2 = roi3 - roi2;
				copyMakeBorder(roi1, roi3, 0, 0, 0, 1, BORDER_CONSTANT, Scalar(1));
				copyMakeBorder(roi1, roi1, 0, 0, 1, 0, BORDER_CONSTANT, Scalar(1));
				roi1 = roi3 - roi1;
				// Some temporary logging 
#ifdef DEBUGOUTPUT
				ss.str("");
				ss << "roiSub" << (double)i << ".jpg";
				imwrite((string)ss.str(), roi2);
#endif
				int vertCount = 0;
				int vertCount2 = 0;

				for (int j = 0; j < roi2.cols; j++)
				{
					if (roi2.at<float>(j) > 0)
					{
						vertCount = vertCount + 1;
					}
					if (roi1.at<float>(j) > 0)
					{
						vertCount2 = vertCount2 + 1;
					}
				}
				// If 3 peaks have been found in the top and bottom region,
				// save accordingly. 
				if ((vertCount>2) && (vertCount2 > 2))
				{
					peakArrayBuffer[NumPeaks] = leftIdx;
					DropDetected = true;

					DropLocationX[NumPeaks] = leftIdx - 10;
					DropLocationX[NumPeaks + 1] = i + 10;
					NumPeaks = NumPeaks + 2;
					leftIdx = i;
					tryLeft = false;
				}
				else
				{
					/* Check every other in case a cell has chopped a drop in half!*/
					// NOT: NOTUSED AT PRESENT!
					/**/
					tryLeft = true;
					if (tryLeft == true)
					{
						leftIdx = i;
						tryLeft = false;
					}
					
						

				}
				
			}
			else
			{
				//leftIdx = i;
			}
	}
	*numDrops = NumPeaks / 2;
	return(DropDetected);
}



// Quick check of the ROI to make sure it is within a frame. True if ROI is valid.
bool checkRoi(cv::Mat *frame, cv::Rect roi)
{
	if (roi.width + roi.x > frame->cols )
	{
		return false;
	}
	if (roi.height + roi.y > frame->rows)
	{
		return false;
	}

	if (roi.x < 0)
	{
		return false;
	}
	if (roi.y < 0)
	{
		return false;
	}
	
	return true;
}

bool DetectDrop(cv::Mat Frame, int roi_y1, int roi_y2, double min_pk_height, double min_pk_dist, int *DropLocation, int counter)
{
	/* PY:  Detect if a drop is present on the input frame by measuring the peaks
	along the X axis of the frame.
	Returns True if drop detected and the X coordinate positions of the drop edges as(X1, x2). :PY */

	//Find droplets by looking for peaks across line in image.


	// Apply ROI
	int width = Frame.cols;
	int height = Frame.rows;

	width = width - 10;

	//Define the region of interest of the channel by cropping the frame.
	cv::Rect ROI(1, roi_y1, width, roi_y2 - roi_y1);
	cv::Mat peak_array = Frame(ROI);
	stringstream ss;

	ss << "PeakArray" << counter << ".jpg";
	imwrite((string)ss.str(), peak_array);
	// integrate this to a single line profile
	reduce(peak_array, peak_array, 0, REDUCE_AVG, -1);

	
	int GaussianBlurSize = 11;
	peak_array.convertTo(peak_array, CV_32F);
	Writevector(peak_array, "PreBlur.csv");
	// Gaussian Blur, which needs the data to be float32
	GaussianBlur(peak_array, peak_array, Size(GaussianBlurSize, GaussianBlurSize), 0, 0);
	Writevector(peak_array, "PostBlur.csv");

	cv::Mat Peak_loc;
	cv::Mat Peak_height;

	// Set default, NULL locations for the Droplet
	DropLocation[0] = 0;
	DropLocation[1] = 0;

	int NumPeaks = opencv_peaks(peak_array, Peak_loc, Peak_height, min_pk_height, min_pk_dist);

	int i = 0;
	bool DropDetected = false;

	Writevector(Peak_height, "PeakHeight.csv");

	if (NumPeaks > 1)
	{
		// use the iffy code to check peak intensities (this works on the the demo video file)
		while ((!DropDetected) && (i < NumPeaks - 1))
		{
			if (Peak_height.at<float>(i) >Peak_height.at<float>(i + 1))
			{
				DropDetected = true;
				DropLocation[0] = Peak_loc.at<unsigned short int>(i);
				DropLocation[1] = Peak_loc.at<unsigned short int>(i + 1);
			}
			++i;
		}
	}

	return(DropDetected);
}


bool CreateDropRoi(cv::Mat frame, cv::Mat &DropROI, Vec2i &DropLocation, const int Padding,int & roiX, int & roiY)
{
	// Use the location of a drop within a frame to create a region of interest.'''
	// Define the region of interest of the new frame, drop_roi
	// Extend region by 30 pixels along x.
	int drop_roi_x1 = DropLocation[0] - Padding;
	int drop_roi_x2 = DropLocation[1] + Padding;
	Rect R;

	// Limit the X coords to the range of the frame.
	if (drop_roi_x1 < 0) drop_roi_x1 = 0;

	if (drop_roi_x2 > frame.cols - 1) drop_roi_x2 = frame.cols - 1;

	// Some quick checking in case x2 and x1 are flipped...
	if (drop_roi_x2 < drop_roi_x1)
	{
		//Create the drop ROI.
		R.x = drop_roi_x2;
		R.y = 0;
		R.width = drop_roi_x1 - drop_roi_x2;
		R.height = frame.rows;
	}
	else
	{
		//Create the drop ROI.
		R.x = drop_roi_x1;
		R.y = 0;
		R.width = drop_roi_x2 - drop_roi_x1;
		R.height = frame.rows;
	}


#ifdef DEBUGOUTPUT 
	imwrite("Frame.jpg", frame);
#endif

	try
	{
		if (!checkRoi(&frame, R))
		{
			cout << "Roi is larger than the frame." << endl;
			return false;
		}
		DropROI = frame(R);
	}
	catch(GenICam::GenericException &e)
	{
		cout << "Crash in CreateDropROI !" << endl;
	}
#ifdef DEBUGOUTPUT 
	imwrite("Drop.jpg", DropROI);
#endif


	roiX = R.x;
	roiY = R.y;
	return true;
}

bool CreateDropMask(cv::Mat DropROI, cv::Mat &DM, Vec2i &DropCentre, float &DropRadius, double thresh, vector<Point> &dropContour)
{
	//Detect the radius of the drop, and set values set to zero outside of a circular mask.

	// Create a mask of the droplet.
	//	Create a binary theshold to find the drop edge.
	//	http://docs.opencv.org/trunk/d7/d4d/tutorial_py_thresholding.html

	

	DropRadius = 0.0f;
	
	cv::Mat DropBinary;
	double radius_inset = 15.0;

	double rThresh;

	rThresh = threshold(DropROI, DropBinary, thresh, 255, THRESH_BINARY);
	

	// Find contours.
	// mode = cv2.RETR_EXTERNAL: retrieves only the extreme outer contours
	// method = cv2.CHAIN_APPROX_SIMPLE: compresses contours to save memory.

	cv::Mat  Hierachy;
	vector<vector<Point> >  Contours;

	displayIm("DropBinary", DropBinary);

	int Ch = DropBinary.channels();
	int Ty = DropBinary.type();

	// https://stackoverflow.com/questions/36138982/opencv-3-0-findcontours-crashes
	findContours(DropBinary, Contours, Hierachy, CV_RETR_LIST, CHAIN_APPROX_SIMPLE, Point(0, 0));

	if (Contours.size() < 1)
		return(false);

	//Only keep the largest contour.
	// Use list comprehension(a lambda expression) to find the length
	// of each contour.
	// contour_length = [len(c) for c in contours]

	Mat tempDisp = DropROI;
	cvtColor(tempDisp, tempDisp, COLOR_GRAY2BGR);


	int len;
	int MxLen = -1;
	int contour_index = -1;
	int contourIdxSecond = - 1;
	int mxLenSecond = -1;
	
	for (int i = 0; i < Contours.size(); i++)
	{
		len = (int)contourArea(Contours[i]);
		if (len > MxLen)
		{
			MxLen = len;
			contour_index = i;
		}
	}
	
/*	for (int i = 0; i < Contours.size(); i++)
	{
		len = contourArea(Contours[i]);
		if (len > mxLenSecond && i != contour_index)
		{
			mxLenSecond = len;
			contourIdxSecond = i;
		}
	}*/

#ifdef DEBUGOUTPUT
	drawContours(tempDisp, Contours, -1, Scalar(255, 255, 0), 1, 1);
	drawContours(tempDisp, Contours, contourIdxSecond, Scalar(0, 255, 0), 1, 1);
	drawContours(tempDisp, Contours, contour_index, Scalar(255, 0, 0), 1, 1);
	displayIm("Contours", tempDisp);
#endif

	int contour_length = MxLen;
	//contour_index = contourIdxSecond;

	// Only keep the contour at contour_index.
	dropContour = Contours[contour_index];

	cv::Mat Hull;
	convexHull(dropContour, Hull);

	//use the largest contour to mask the image
	// Find the bounding circle from the contour.
	Point2f Centre;
	float Radius;
	minEnclosingCircle(dropContour, Centre, Radius);

	Radius = Radius - radius_inset;
	if (Radius < 3)
	{
		return(false);// FAIL
	}

	// Create empty circle
	cv::Mat DropMask(DropROI.rows, DropROI.cols, CV_8UC3);
	Point iCentre(Centre);
	//iCentre.x = Centre.y;
	//iCentre.y = Centre.x;

	DropMask = Mat::zeros(DropROI.rows, DropROI.cols, CV_8U);
	//circle(DropMask, iCentre, (int)Radius, Scalar(255), FILLED);
	drawContours(DropMask, Contours, contour_index, Scalar(255), -1);

	DM = DropMask;
	DropCentre[0] = Centre.x;
	DropCentre[1] = Centre.y;
	DropRadius = Radius;

	return(true);
}



bool DetectCells_Ed(cv::Mat DropROI, cv::Mat DropMask, Vec2i &CellLocation, int openSize, 
					int erodeSize, int threshold,int dbg)
{

	/*   Detect if a cell is present within a drop using the following process:
	1. Use a greyscale morphological 'open' on the droplet ROI
	2. Subtract the open from the original droplet ROI
	3. Erode the mask image
	4. AND the results of (2) and (3)
	5. Find the maximum of the result of (4). 

	Input
	-----
	DropROI: the input drop
	DropMask: Input drop mask

	Parameters
	----------
	openSize:  The size to open (see step 1)
	erodeSize: The size to erode (note, must be bigger than the size of the 'halo')
	threshold: A threshold after the morphological operations

	Output
	------
	CellLocation: Vector specifying the x,y position of any detected cell
	return: Whether or not a cell was detected
	*/

	bool detected;
	Mat DropROIDilate;
	Mat element;
	
	detected = false;

#ifdef DEBUGOUTPUT 
	imwrite("DropROI_Cells.jpg", DropROI);
#endif
#ifdef DEBUGOUTPUT
	//namedWindow("DropROI_cells", WINDOW_AUTOSIZE);
	displayIm("DropROI_cells", DropROI);
#endif
	
	Mat DropROIOpen;
	// 1. Open the image of the droplet ROI
	element = getStructuringElement(MORPH_ELLIPSE, cv::Size(openSize, openSize), cv::Point(-1, -1));
	morphologyEx(DropROI, DropROIOpen, MORPH_OPEN, element, cv::Point(-1, -1), 1);
#ifdef DEBUGOUTPUT
	namedWindow("DropROI_cellsOpen", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpen", DropROIOpen);
#endif
	// 2. Subtract the opened image from the original image
	DropROIOpen = DropROI - DropROIOpen;
	// 3. Make the droplet mask smaller (to ignore the 'halo' at the edges) using erode
	element = getStructuringElement(MORPH_ELLIPSE, cv::Size(erodeSize, erodeSize), cv::Point(-1, -1));
	morphologyEx(DropMask, DropMask, MORPH_ERODE, element, cv::Point(-1, -1), 1);
	
#ifdef DEBUGOUTPUT
	namedWindow("DropROI_erode", WINDOW_AUTOSIZE);
	imshow("DropROI_erode", DropMask);
	namedWindow("DropROI_cellsOpenAnd", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd", DropROI);
#endif
	// 4. AND the result of (3) and (2). 
	DropROIOpen = DropROIOpen & DropMask;

#ifdef DEBUGOUTPUT
	namedWindow("DropROI_cellsOpenAnd2", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd2", DropROI);
#endif
	// 5. Find the location of any values > threshold
	double maxVal = 0;
	double minVal = 0;
	Point minLoc; Point maxLoc;
	minMaxLoc(DropROIOpen, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	//cout << "Max Val:" << maxVal << endl;
	if (maxVal > threshold)
	{ 
		//DropROI = DropROI & DropMask;
		Scalar  Colour = Scalar(255, 255, 255);
		circle(DropROIOpen, maxLoc, (int)5, Colour);
		detected = true;
	}

#ifdef DEBUGOUTPUT
	namedWindow("DropROI_cellsOpenAnd2", WINDOW_AUTOSIZE);
	imshow("DropROI_cellsOpenAnd2", DropROI);
#endif
	CellLocation[0] = maxLoc.x;
	CellLocation[1] = maxLoc.y;
	return detected;
}


bool DetectCells(cv::Mat DropROI, cv::Mat DropMask, Vec2i &CellLocation, double Threshold, int dbg)
{
	//threshold = 800, debug_stats = False) :
	// use a fast approximation of the derivative of the frame to detect flaws within the drop. '''

	// Use an integral filter to detect gradients within drop.
	// This is different to normal convolution.
	// Define a filter to detect flaws in the horizontal direction.
	const int filter_h[2][5] = { { 1, 1, 3, 4, -1 },
	{ 4, 1, 6, 4, +1 } };

	/*
	filter_h = np.array([[1, 1, 3, 4, -1],
	[4, 1, 6, 4, +1]])
	*/
	void IntegralFilter(cv::Mat im, cv::Mat &Filtered);

	cv::Mat FilteredFrame;
	IntegralFilter(DropROI, FilteredFrame);

	convertScaleAbs(FilteredFrame, FilteredFrame);
#ifdef DEBUGSOUTPUT
	namedWindow("Filtered Frame", WINDOW_AUTOSIZE);
	imshow("Filtered Frame", FilteredFrame);
#endif
	int type2 = DropMask.type();

	if (type2 != CV_8U)
		DropMask.convertTo(DropMask, CV_8U);


	const int border_x = 3, border_y = 5;

	// Make array the same size as the original by addind a hard coded sized border
	copyMakeBorder(FilteredFrame, FilteredFrame, 0, border_y, 0, border_x, BORDER_CONSTANT, 0);



	// Apply the drop mask to the frame.
	// Logical AND the frame with itself and mask with drop_mask.

	cv::Mat OutputFrame;
	FilteredFrame.copyTo(OutputFrame, DropMask);

#ifdef DEBUGOUTPUT 
	imwrite("FilteredData.jpg", OutputFrame);
#endif
	//bitwise_and(FilteredFrame, FilteredFrame, DropMask);
	//waitKey(0);

	//Now filtered_frame is zero outside the drop radius.

	cv::Mat sum_v, sum_h;


	reduce(OutputFrame, sum_v, 0, REDUCE_SUM, CV_32F);
	reduce(OutputFrame, sum_h, 1, REDUCE_SUM, CV_32F);

	// Find maximum and maxima location
	double minVal, maxValV, maxValH;
	cv::Point minLoc, maxLocV, maxLocH;

	bool cell_detected = false;
	CellLocation[0] = 0;
	CellLocation[1] = 0;

	minMaxLoc(sum_v, &minVal, &maxValV, &minLoc, &maxLocV);
	if (maxValV > Threshold)
	{
		minMaxLoc(sum_h, &minVal, &maxValH, &minLoc, &maxLocH);
		if (maxValH > Threshold)
		{
			cell_detected = true;
			CellLocation[0] = maxLocV.x;
			CellLocation[1] = maxLocH.y;
		}
	}

	return(cell_detected);
}

