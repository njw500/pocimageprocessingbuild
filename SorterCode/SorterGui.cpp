#include "stdafx.h"
#include "sorterSysParams.h"
#include "SorterGui.h"

// For Debug Macros
//#include "VideoOutput.h"

using namespace cv;

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC
SorterGui::SorterGui()
{
	mouseActive = false;
}

SorterGui::~SorterGui()
{
}

void SorterGui::SorterGuiInit(CellDetector *cellDetector_in, SorterSysParams *sorterSysParams_in, BaslerCamera *camera_in)
{
	cellDetector = cellDetector_in;
	sorterSysParams = sorterSysParams_in;
	camera = camera_in;

	// Now draw the image
	namedWindow("Main Output", WINDOW_AUTOSIZE);

	createTrackbars();
#ifdef ENABLECAMERA
	createMouseCallbacks();
#endif
	//onTrackbar(sorterSysParams->sorterRoiY1, 0);
}
void SorterGui::updateParams()
{
	sorterSysParams->checkBounds();
	
	cout << sorterSysParams->sorterThresholdLevelDrop.val << endl;
	// Reset all the parameters with some bounds checking
	sorterSysParams->sorterRoiY1.setValue(sorterSysParams->sorterRoiY1.val, sorterSysParams->sorterRoiY1.getMin(), sorterSysParams->sorterRoiY2.val - 2);
	sorterSysParams->sorterRoiY2.setValue(sorterSysParams->sorterRoiY2.val, max(1,sorterSysParams->sorterRoiY1.val + 2), sorterSysParams->sorterRoiY2.getMax());
	sorterSysParams->sorterTopBottomBorder_px.setValue(sorterSysParams->sorterTopBottomBorder_px.val);
	sorterSysParams->sorterThresholdLevelDrop.setValue(sorterSysParams->sorterThresholdLevelDrop.val);
	sorterSysParams->sorterMinPkDist.setValue(sorterSysParams->sorterMinPkDist.val);
	sorterSysParams->sorterCellPadding.setValue(sorterSysParams->sorterCellPadding.val);
	sorterSysParams->sorterOpenSize.setValue(sorterSysParams->sorterOpenSize.val);
	sorterSysParams->sorterErodeSize.setValue(sorterSysParams->sorterErodeSize.val);
	sorterSysParams->sorterThresholdLevelCell.setValue(sorterSysParams->sorterThresholdLevelCell.val);
	sorterSysParams->sorterCheckDropRowTop.setValue(sorterSysParams->sorterCheckDropRowTop.val);
	sorterSysParams->sorterCheckDropRowBottom.setValue(sorterSysParams->sorterCheckDropRowBottom.val);
	sorterSysParams->cParams.gain.val = (double)gain;
	sorterSysParams->cParams.gain.setValue(gain);


	/*sorterSysParams->sorterCheckDropRowTop.setValue(sorterSysParams->sorterCheckDropRowTop.val, sorterSysParams->sorterCheckDropRowTop.getMin(), sorterSysParams->sorterCheckDropRowBottom.val - 2);
	sorterSysParams->sorterCheckDropRowBottom.setValue(sorterSysParams->sorterCheckDropRowBottom.val, max(1, sorterSysParams->sorterCheckDropRowTop.val + 2), sorterSysParams->sorterCheckDropRowBottom.getMax());*/
	setTrackbarPos("RoiY1", "Main Output", sorterSysParams->sorterRoiY1.val);
	setTrackbarPos("RoiY2", "Main Output", sorterSysParams->sorterRoiY2.val);
	/*
	setTrackbarPos("DropRowTop", "Main Output", sorterSysParams->sorterCheckDropRowTop.val);
	setTrackbarPos("DropRowBot", "Main Output", sorterSysParams->sorterCheckDropRowBottom.val);*/
	cellDetector->updateParams(sorterSysParams->sorterRoiY1.val, sorterSysParams->sorterRoiY2.val, sorterSysParams->sorterTopBottomBorder_px.val,
		sorterSysParams->sorterThresholdLevelDrop.val, sorterSysParams->sorterMinPkDist.val, sorterSysParams->sorterCellPadding.val, sorterSysParams->sorterOpenSize.val,
		sorterSysParams->sorterErodeSize.val, sorterSysParams->sorterThresholdLevelCell.val, sorterSysParams->sorterCheckDropRowTop.val, sorterSysParams->sorterCheckDropRowBottom.val);


	
}

void SorterGui::createTrackbars()
{
	cv::createTrackbar("RoiY1", "Main Output", &(sorterSysParams->sorterRoiY1.val), sorterSysParams->sorterRoiY1.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("RoiY2", "Main Output", &(sorterSysParams->sorterRoiY2.val), sorterSysParams->sorterRoiY2.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("ThreshDrop", "Main Output", &(sorterSysParams->sorterThresholdLevelDrop.val), sorterSysParams->sorterThresholdLevelDrop.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("ThreshCell", "Main Output", &(sorterSysParams->sorterThresholdLevelCell.val), sorterSysParams->sorterThresholdLevelCell.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("DropRowTop", "Main Output", &(sorterSysParams->sorterCheckDropRowTop.val), sorterSysParams->sorterCheckDropRowTop.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("DropRowBot", "Main Output", &(sorterSysParams->sorterCheckDropRowBottom.val), sorterSysParams->sorterCheckDropRowBottom.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("ErodeSz", "Main Output", &(sorterSysParams->sorterErodeSize.val), sorterSysParams->sorterErodeSize.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("OpenSz", "Main Output", &(sorterSysParams->sorterOpenSize.val), sorterSysParams->sorterOpenSize.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("Border", "Main Output", &(sorterSysParams->sorterTopBottomBorder_px.val), sorterSysParams->sorterTopBottomBorder_px.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("Gain", "Main Output", &(gain), sorterSysParams->cParams.gain.getMax(), &SorterGui::onTrackbar, this);
	cv::createTrackbar("MinPkDist", "Main Output", &(sorterSysParams->sorterMinPkDist.val), sorterSysParams->sorterMinPkDist.getMax(), &SorterGui::onTrackbar, this);

//	cv::createTrackbar("Roi Y2", "Main Output", &(sorterSysParams->sorterRoiY2.val), sorterSysParams->sorterRoiY1.getMax(), &SorterGui::onTrackbar, this);*/


}

void SorterGui::createMouseCallbacks()
{
	setMouseCallback("Main Output", onSorterMouseClick, this);
}

void SorterGui::onMouseClickInternal(int ev, int x, int y)
{
	
	if (ev == EVENT_MOUSEMOVE)
	{
		if (mouseActive)
		{
			int moveX = lastX - x;
			int moveY = lastY - y;
			int sign;
			if (moveX > 0)
				sign = 1;
			else
				sign = -1;

			camera->RoiHoriz(abs(moveX), sign);
			if (moveY > 0)
				sign = 1;
			else
				sign = -1;
			camera->RoiVert(abs(moveY), sign);
		}
	}
	if (ev == EVENT_LBUTTONDOWN)
	{
		lastX = x;
		lastY = y;
		mouseActive = true;
	}

	if (ev == EVENT_LBUTTONUP)
	{
		mouseActive = false;
	}
}