#pragma once
#ifndef SORTERGUI_C
#define SORTERGUI_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\Utilities\Utilities\CamParams.h"
#include "SorterSysParams.h"
#include "CellDetector.h"
#include "BaslerCamera.h"

#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WCellDetection 7


// Namespace for using cout.
using namespace std;

class SorterGui
{
public:
	SorterGui();
	~SorterGui();



	void SorterGui::setGain(int gain);
	void SorterGui::setSorterRoiY1(int sorterRoiY1);
	void SorterGui::SorterGuiInit(CellDetector *cellDetector_in, SorterSysParams *sorterSysParams_in, BaslerCamera *camera_in);
	void onMouseClickInternal(int ev, int x, int y);

private:
	CellDetector	*cellDetector;
	SorterSysParams *sorterSysParams;
	BaslerCamera *camera;
	void updateParams();
	//static void SorterGui::onTrackbar(int, void*);
	static void SorterGui::onTrackbar(int newValue, void* object)
	{
		if (newValue < 1)
			newValue = 1;
		SorterGui* sorterGui = (SorterGui*)(object);
		sorterGui->updateParams();
		//
	}
	static void SorterGui::onSorterMouseClick(int ev, int x, int y, int, void* obj)
	{
		SorterGui* sg = static_cast<SorterGui*>(obj);
		if (sg)
			sg->onMouseClickInternal(ev, x, y);

	}

	//friend void onSorterMouseClick(int ev, int x, int y, void* obj);
	

	void createTrackbars();
	void createMouseCallbacks();
	bool mouseActive; // Whether the button is down
	int lastX; // Last x position when button is down
	int lastY; // Last y position when button is down
	int gain;
};



#endif