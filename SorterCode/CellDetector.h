#ifndef CELLDETECTOR_H
#define CELLDETECTOR_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include <stdlib.h>
#include <vector>

class CellDetector
{

private:

	void CellDetector::BuildBackground();

// PARAMETERS

	//roi_y1 : The start column that should be selected, roi_y2 : The end column that should be selected									 	
	int roiY1, roiY2;

	//topbottomborder_px : The number of pixels to ignore at the top and bottom of the image
	int topBottomBorder_px;

	// Grayscale threshold level for the halo surrounding drops
	int thresholdLevelDrop;

	// min_pk_dist: The distance which corresponds to the minimum diameter of a drop.//
	double minPkDist;

	//counter: Frame counter
	int counter;

	// Cell padding parameters
	int cellPadding;
	// The open size for cell detection
	int openSize; 
	// The erode size for cell detection
	int erodeSize;
	// The threshold level for the cell
	int thresholdLevelCell;

	// THe number of background subtractions before cells will be detected
	const int numberBackgroundSubtractions=30;
	// The position whether to check if this is a gap or a drop (should be at around 1/4 from the top to the bottom of the drop)
	int checkDropRowTop;
	// The bottom position whether to check if this is a gap or a drop (should be at around 3/4 from the top to the bottom of the drop)
	int checkDropRowBottom;


	

// INPUTS/OUTPUTS
	std::vector<cv::Point> dropContour;
	// The next frame for processing
	cv::Mat *frame;
	// The resulting location of a calculated cell
	cv::Vec2i cellLocation;
	// The resulting location of a calculated drop
	cv::Vec2i dropLocation;
	// The resulting location of a calculated drop
	int dropLocation2[20];

	// The drop mask
	cv::Mat dropMask;

	// The drop centre
	cv::Vec2i dropCentre;

	// The drop radius
	float dropRadius;

	// The ROI that will contain the drop
	cv::Mat dropRoi;

	// The drop ROI offset
	int roiX, roiY;

	// The average image
	cv::Mat avg;
	bool ValidAverage = false;
	// The drop contour
	
	// Flag set to true when a cell was detected within the last succesfully detected drop.
	bool cellDetected;

public:

	CellDetector(int roiY1_in, int roiY2_in, int topBottomBorder_px_in, int thresholdLevelDrop_in,
		int minPkDist_in, int cellPadding_in, int openSize_in, int erodeSize_in, int thresholdLevelCell_in,
		int checkDropRowTop_in, int checkDropRowBottom_in);
void	updateParams(int roiY1_in, int roiY2_in, int topBottomBorder_px_in, int thresholdLevelDrop_in,
		int minPkDist_in, int cellPadding_in, int openSize_in, int erodeSize_in, int thresholdLevelCell_in,
		int checkDropRowTop_in,	int checkDropRowBottom_in);

	~CellDetector();

	void Init();// Reset counters and average

	// Adds a new frame and calculates the cell position
	bool updateFrame(cv::Mat *newFrame); 

	// Plots the result
	void plotResult(cv::String plotName, cv::Mat frame, int left, int top);

	// Flag set to true when a drop was detected
	bool dropDetected;

	
	bool IsCelldetected(){ return(cellDetected); };
};
#endif 
