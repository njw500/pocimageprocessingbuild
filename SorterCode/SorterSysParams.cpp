#include "stdafx.h"
#include "SorterSysParams.h"

// For Debug Macros
//#include "VideoOutput.h"

using namespace cv;

// Namespace for using cout
using namespace std;

#define USEPOC 1
//#undef USEPOC




// Do nothing in Constructor
SorterSysParams::SorterSysParams()
{

	//CamROI.x = 0;
	//CamROI.y = 0;
	//CamROI.height = 100;
	//CamROI.width  = 400;

	// Set ROI for extraction of useful data from grabbed (or video frame)

	CannyThres1 = 22.0;// 22.0;// was 70 in python
	CannyThres2 = 15.0;// 15.0;// was 40
	CannyThres1 = 70.0;// 22.0;// was 70 in python
	CannyThres2 = 40.0;// 15.0;// was 40
	// This works for POC video 

	/*  Generator numbers !
	VideoFileName="V:/Projects/Pascal/Generator C Code/GeneratorVid.avi";
	*/
	cParams.FrameROIr.x		 = 1;
	cParams.FrameROIr.y		 = 863;
	cParams.FrameROIr.width  = 500;
	cParams.FrameROIr.height = 146;

	cParams.CameraSerial = "21997816";
	//cParams.CameraSerial = "21997816"; //21997816;
	cParams.gain.setValue(8.9,1,12);
	cParams.exp = 59;
	

	VideoFileName = "c:/Temp/Basler acA1300-200um CS (21997816)_20161213_121445700.avi"; // ED PC
	//VideoFileName = "v:/projects/Pascal/Sorter/Basler acA1300-200um CS (21997816)_20161213_121445700.avi"; // NJW PC

	VideoIsPreCropped = true;

	//CameraSerial = "21799755";// Local office
	//CameraSerial = "21799755"; //  ED's PC   21997816;


	//"22025921";// Generator
	// 21997816; // SORTER
	// 22025914; // DISP
	// 21982883  // BOOT

	// SORTER



	AverageDecay = 0.1f;
	Zone_Detection_Radius= 0.4f;


	DisplayMode[WGrab] = false;
	DisplayMode[WHough] = false;
	DisplayMode[WBack] = false;
	DisplayMode[WCropped] = false;
	DisplayMode[WEdges] = false;
	DisplayMode[WHist] = false;
	DisplayMode[WOutput] = false;
	DisplayMode[WCellDetection] = true;// True for final image display



	Padding.setValue(3,1,20); // Padding of the drop region
	sorterTopBottomBorder_px.setValue(5, 1, cParams.FrameROIr.height - 1);
	sorterRoiY1.setValue(77, 1, cParams.FrameROIr.height - sorterTopBottomBorder_px.val); // The start column that should be selected, 
	sorterRoiY2.setValue(81, 1, cParams.FrameROIr.height - sorterTopBottomBorder_px.val); //  The end column that should be selected	
	//topbottomborder_px : The number of pixels to ignore at the top and bottom of the image
	
	// Grayscale threshold level for the halo surrounding drops
	sorterThresholdLevelDrop.setValue(100,1,255);
	// min_pk_dist: The distance which corresponds to the minimum diameter of a drop.//
	sorterMinPkDist.setValue(100,1,cParams.FrameROIr.width/2);
	// Cell padding parameters
	sorterCellPadding.setValue(10,1,50);
	// The open size for cell detection
	sorterOpenSize.setValue(8,1,10);
	// The erode size for cell detection
	sorterErodeSize.setValue(20,1,50);
	// The threshold level for the cell
	sorterThresholdLevelCell.setValue(30,1,255);
	// The top region for checking whether this is a gap or a drop
	sorterCheckDropRowTop.setValue(30, 1, cParams.FrameROIr.height - 1 - sorterTopBottomBorder_px.val);
	// The bottom region for checking whether this is a gap or a drop
	sorterCheckDropRowBottom.setValue(105, 1, cParams.FrameROIr.height - 1 - sorterTopBottomBorder_px.val);

}


// Do nothing in Destructor
SorterSysParams::~SorterSysParams()
{
}

// A helper function to check (and correct) the bounds of key function parameters
// Note: this has been deprecated by the Params class which has built in bounds checking
bool SorterSysParams::checkBounds()
{
	bool retVal = true;
	/*retVal = retVal && checkVarBounds(&sorterRoiY1.val, 2, cParams.FrameROIr.height);
	retVal = retVal && checkVarBounds(&sorterRoiY2.val, 2, cParams.FrameROIr.height - 2);
	retVal = retVal && checkVarBounds(&sorterRoiY1.val, 2, sorterRoiY2.val);
	retVal = retVal && checkVarBounds(&sorterCellPadding.val, 1, 50);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelCell.val, 1, 255);
	retVal = retVal && checkVarBounds(&sorterThresholdLevelDrop.val, 1, 255);
	retVal = retVal && checkVarBounds(&sorterCellPadding.val, 1, 50);
	retVal = retVal && checkVarBounds(&sorterOpenSize.val, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&sorterErodeSize.val, 2, cParams.FrameROIr.height / 2);
	retVal = retVal && checkVarBounds(&(cParams.gain), (int)0, (int)12);*/

	

	// Need to add some bounds checking for exposure and gain. 
	return retVal;
}



bool SorterSysParams::checkVarBounds(int *var, int mn, int mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}

bool SorterSysParams::checkVarBounds(double *var, double mn, double mx)
{
	bool retVal = true;
	if (*var < mn)
	{
		*var = mn;
		retVal = false;

	}
	if (*var > mx)
	{
		*var = mx;
		retVal = false;
	}
	return retVal;
}
