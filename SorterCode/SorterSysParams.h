#pragma once
#ifndef SORTERSYSPARAMS_C
#define SORTERSYSPARAMS_C

#include "io.h"

#include <stdlib.h>

#include <algorithm>

// Include files to use OpenCV API.
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/opencv.hpp>

#include "..\Utilities\Utilities\CamParams.h"
#include "..\Utilities\Utilities\Params.h"


#define WGrab  0
#define WHough 1
#define WBack 2
#define WCropped 3
#define WEdges 4
#define WHist 5
#define WOutput 6
#define WCellDetection 7


// Namespace for using cout.
using namespace std;


class SorterSysParams
{
public:
	SorterSysParams();
	~SorterSysParams();

	// Define all the parameters that are tuned and tuneable in the algorithm
	static const uint c_countOfImagesToGrab =500;
	
	CamParams cParams;
	const bool DoFrameRotate = false;
	const float FrameRotate = 0.0;

	float CannyThres1;// 70.0;// 22.0;// was 70
	float CannyThres2;// 40.0;// 15.0;// was 40

	//cv::Rect CamROI;
	//cv::Rect FrameROI;

	//int Frame_ROI_Left , Frame_ROI_Top , Frame_ROI_Width , Frame_ROI_Height ;
	//cv::Rect FrameROIr;

	float Zone_Detection_Radius;// = 0.4;
	float AverageDecay;

	cv::String VideoFileName;

	bool DisplayMode[10];
	bool VideoIsPreCropped = true;

	// Sorter parameters
	ParamDouble Padding; // Padding of the drop region
	ParamInt sorterRoiY1; // The start column that should be selected, 
	ParamInt sorterRoiY2; //  The end column that should be selected	
	//topbottomborder_px : The number of pixels to ignore at the top and bottom of the image
	ParamInt sorterTopBottomBorder_px;
	// Grayscale threshold level for the halo surrounding drops
	ParamInt sorterThresholdLevelDrop;
	// min_pk_dist: The distance which corresponds to the minimum diameter of a drop.//
	ParamInt sorterMinPkDist;
	// Cell padding parameters
	ParamInt sorterCellPadding;
	// The open size for cell detection
	ParamInt sorterOpenSize;
	// The erode size for cell detection
	ParamInt sorterErodeSize;
	// The threshold level for the cell
	ParamInt sorterThresholdLevelCell;
	ParamInt sorterCheckDropRowTop;
	ParamInt sorterCheckDropRowBottom;
	// Parameters defining the square box from the top (?)
	//const int SortLeft = 250, SortTop = 200, SortWidth = 280, SortHeight = 250;

	bool SorterSysParams::checkVarBounds(double *var, double mn, double mx);
	bool SorterSysParams::checkVarBounds(int *var, int mn, int mx);
	bool SorterSysParams::checkBounds();
private:
	int ok;


};




#endif