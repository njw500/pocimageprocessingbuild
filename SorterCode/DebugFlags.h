#define DBG 1
#define FDBG 1
#define FERR 1

#undef DBG
#undef FDBG 
#undef SERR

// Define if images are to be saved.
// '0'- no; '1'- yes.
#define saveImages 0

// Define if video is to be recorded.
// '0'- no; '1'- yes.
#define recordVideo 0
#undef recordVideo

/// NOTE, the following are handled as preprocessor definitions within the build configuration. 
//#define ENABLECAMERA		// Use the camera
//#define EMULATEFROMFILE	// Use file rather than camera

//#undef DEBUGOUTPUT
//#undef SCREENOUTPUT
//#undef ENABLECAMERA
//#define EMULATEFROMFILE